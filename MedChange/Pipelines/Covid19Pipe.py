'''
Created on Apr 1, 2020

@author: dweissen
'''
import sys
import os
import logging as lg
from Handlers.Covid19TweetsHandler import Covid19TweetsHandler
from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
# from theano.tensor import inplace
log = lg.getLogger('MainPipeline')
import Properties
from Properties.DavyMacProperties import DavyMacProperties
from Properties.DavyLPCProperties import DavyLPCProperties
from Data.Pool import Pool
from ActiveLearners.Oracles.GoldStandardOracle import GoldStandardOracle
import pickle as pkl
#maybe to be changed
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from ActiveLearners.RandomSampler import RandomSampler
from ActiveLearners.UncertaintySampler import UncertaintySampler, USQueryingAlgorithm, UncertaintyMeasure
from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
from ActiveLearners.Classifiers.CNNMultinomial import CNNMultinomial
from ActiveLearners.Classifiers.CNNClassifier import CNNClassifier
from Performance.Performance import Scores, Performance
import pandas as pd

#For BERT from Haitao code in box or by email, last summer

class covid19Pipe(object):
    """
    This pipeline uses active learning to detect tweets mentioning a possible/probable/other mention of a Twitter user to be infected by Coronavirus
    Comment: should definitly not be in this project but it is a very short request due to the pandemy of the virus, so I keep everything in this project as I know it runs here
    """

    def __setCovid19Pool(self, properties:Properties, classification="multiclass"):
        """
        Prepare the corpus for the training and test of the system
        :param: properties, accessing the paths
        :param: classification, will prepare the data as binary: classes 1 and 2 are merged as 1; as multiclass: 3 classes to predict possible/probable/other
        """
        #for the moment I have only one file of training ex. I'm splitting
        cvhdl = Covid19TweetsHandler(properties)
        train = cvhdl.getTrainExamples()
        val = cvhdl.getValExamples()
        test = cvhdl.getTestExamples()
        unlab = cvhdl.getUnlabeledExamples()
        
        #If we need to save the inputs
        Alltrain = train.append(unlab, ignore_index=False, verify_integrity=True)
        Alltrain.to_csv("/tmp/trainCovid19.tsv", sep='\t')
        assert len(Alltrain)!=len(train), "problem here..."
        val.to_csv("/tmp/valCovid19.tsv", sep='\t')
        test.to_csv("/tmp/testCovid19.tsv", sep='\t')
        
        if classification=="binary":
            def toBin(label):
                if label>0:
                    return 1
                else:
                    return 0
            train['label'] = train['label'].apply(lambda label: toBin(label))
            val['label'] = val['label'].apply(lambda label: toBin(label))
            test['label'] = test['label'].apply(lambda label: toBin(label))
            unlab['label'] = unlab['label'].apply(lambda label: toBin(label))
            log.debug("Prepare the pool as a binary classification problem")
            assert train['label'].nunique()==2, f"I found {train['label'].nunique()} classes when I was expecting 2, possible/probable vs. other"
        elif classification=="multiclass":
            log.debug("Prepare the pool as a multi-class classification problem")
            assert train['label'].nunique()==3, f"I found {train['label'].nunique()} classes when I was expecting 3, possible, probable, other"
        else:
            raise Exception(f"Unknown type of classification: {classification}")  
        
        covidPool = Pool(unlabeledExamples = unlab, trainingExamples = train, validationExamples = val, testExamples = test)
        return covidPool
    
    def trainBinary(self, properties:Properties):
        """
        :param: properties for the application
        :param: iteration the number of iteration in the active learning process
        Run the training in multi-class classification
        """
        log.info("Start training the model on the corpus using active learning...")
        pool = self.__setCovid19Pool(properties, classification="binary")
        log.info(f"covid19 Pool created -> {str(pool)}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy())
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(pool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        
        
        #start the active learning process using a gold standard as oracle
        iteration = 0
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNMultinomial(name='CNNMulti1', preprocessWebMD=process_WebMD, numberClasses=3))
        #--- To Run with UncertaintySampler:
        actLearner: UncertaintySampler = UncertaintySampler(CNNClassifier('CNN1', process_WebMD))
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        #actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD), CNNTF2Classifier('CNN2',process_WebMD), CNNTF2Classifier('CNN3',process_WebMD)])
        #actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))
        
        actLearner.train(pool)
        actLearner.clear()
        
        #we compute the score on eval
        perfEval = Performance("ActiveLearner_Validation_Set")
        predictionsEval = actLearner.classify(pool.valEx)
        scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
        perfEval.addPerformance(0, scoresEval)
        #we compute the score on the test set and add to the scores in performance
        perfTest = Performance("ActiveLearner_Test_Set")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        perfTest.addPerformance(0, scores)
        
        stillImproving = True
        log.error("TODO: set the budget correctly.")
        budget = len(pool.unlabEx)
        
        while stillImproving:
            if budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={budget}.")
                miExamples = actLearner.getMostInformativeExamples(pool, properties.ExQueriedBatchSize)
                gsOracle.getQueryLabels(miExamples)
                pool.addQueryResult(miExamples, iteration)
                budget = budget - len(miExamples)
                
                #training on the batch of examples most recently annotated            
                actLearner.train(pool)
                actLearner.clear()
                
                #we compute the score on eval
                predictionsEval = actLearner.classify(pool.valEx)
                scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
                perfEval.addPerformance(iteration, scoresEval)
                #we compute the improvement of the classifier on the test set and add to the performance
                predictions = actLearner.classify(pool.testEx)
                scores = actLearner.evaluate(predictions, pool.testEx)
                perfTest.addPerformance(iteration, scores)

        actLearner.drawPerformance(perfEval, f'/tmp/ALEvaluation_PerfOnEval.pdf')
        actLearner.drawPerformance(perfTest, f'/tmp/ALEvaluation_PerfOnTest.pdf')
        log.info(f"Performances on Evaluation set:{perfEval}")
        log.info(f"Performances on Test set:{perfTest}")
        # I recompute the predictions for the last iteration to be displayed
        predictions = actLearner.classify(pool.testEx)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = pool.testEx['label']
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['User_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','notes'])
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")
    
    
    def trainMultiClass(self, properties:Properties):
        """
        :param: properties for the application
        :param: iteration the number of iteration in the active learning process
        Run the training in multi-class classification
        """
        log.info("Start training the model on the corpus using active learning...")
        pool = self.__setCovid19Pool(properties)
        log.info(f"covid19 Pool created -> {str(pool)}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy())
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(pool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        
        
        #start the active learning process using a gold standard as oracle
        iteration = 0
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNMultinomial(name='CNNMulti1', preprocessWebMD=process_WebMD, numberClasses=3))
        #--- To Run with UncertaintySampler:
        actLearner: UncertaintySampler = UncertaintySampler(CNNMultinomial(name='CNNMulti1', preprocessWebMD=process_WebMD, numberClasses=3))
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        #actLearner: ExpertCommittee = ExpertCommittee([CNNMultinomial(name='CNNMulti1', preprocessWebMD=process_WebMD, numberClasses=3),CNNMultinomial(name='CNNMulti2', preprocessWebMD=process_WebMD, numberClasses=3), CNNMultinomial(name='CNNMulti3', preprocessWebMD=process_WebMD, numberClasses=3)])
        #actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))
        
        actLearner.train(pool)
        actLearner.clear()
        
        #we compute the score on eval
        perfEval = Performance("ActiveLearner_Validation_Set")
        predictionsEval = actLearner.classify(pool.valEx)
        scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
        perfEval.addPerformance(0, scoresEval)
        #we compute the score on the test set and add to the scores in performance
        perfTest = Performance("ActiveLearner_Test_Set")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        perfTest.addPerformance(0, scores)
        
        stillImproving = True
        log.error("TODO: set the budget correctly.")
        budget = len(pool.unlabEx)
        
        while stillImproving:
            if budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={budget}.")
                miExamples = actLearner.getMostInformativeExamples(pool, properties.ExQueriedBatchSize)
                gsOracle.getQueryLabels(miExamples)
                pool.addQueryResult(miExamples, iteration)
                budget = budget - len(miExamples)
                
                #training on the batch of examples most recently annotated            
                actLearner.train(pool)
                actLearner.clear()
                
                #we compute the score on eval
                predictionsEval = actLearner.classify(pool.valEx)
                scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
                perfEval.addPerformance(iteration, scoresEval)
                #we compute the improvement of the classifier on the test set and add to the performance
                predictions = actLearner.classify(pool.testEx)
                scores = actLearner.evaluate(predictions, pool.testEx)
                perfTest.addPerformance(iteration, scores)

        actLearner.drawPerformance(perfEval, f'/tmp/ALEvaluation_PerfOnEval.pdf')
        actLearner.drawPerformance(perfTest, f'/tmp/ALEvaluation_PerfOnTest.pdf')
        log.info(f"Performances on Evaluation set:{perfEval}")
        log.info(f"Performances on Test set:{perfTest}")
        # I recompute the predictions for the last iteration to be displayed
        predictions = actLearner.classify(pool.testEx)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = pool.testEx['label']
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','certainty_label_2','notes'])
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")
        
        
    def classifyBinary(self, properties):
        """
        Just run the trained classifier to predict on 1 set of unlabeled examples
        """
        log.info(f"read the unlabeled tweets to classify @{properties.covidTweetsUnlabeledPath}")
        ex = pd.read_csv(properties.covidTweetsUnlabeledPath, sep='\t')
        #ex.rename(columns = {"Tweet_ID":"docID", "User_ID":"user_id", "Tweet":"text", "Date":"created_at", "0 = Other Mention,    1 = Probable,  2 = Possible":"label", "Notes":"notes"}, inplace=True)
        ex.rename(columns = {"tweet_id":"docID", "User_ID":"user_id", "Tweet":"text", "Date":"created_at", "0 = Other Mention,    1 = Probable,  2 = Possible":"label", "Notes":"notes"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            raise Exception("I was expecting the preprocessWebMD to be available for classification...")
        
        clsr: ClassifierInt = CNNClassifier('CNN1', process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/Coronavirus/Model/binary/CNN1_bestModel.h5')
        ex = clsr.classify(ex)
        ex.to_csv('/tmp/covid19_unlabeled_predicted.tsv', sep='\t')
        
        
    def classifyMultiClass(self, properties):
        """
        Just run the trained classifier to predict on 1 set of unlabeled examples
        """
        log.info(f"read the unlabeled tweets to classify @{properties.covidTweetsUnlabeledPath}")
        ex = pd.read_csv(properties.covidTweetsUnlabeledPath, sep='\t')
        #ex.rename(columns = {"Tweet_ID":"docID", "User_ID":"user_id", "Tweet":"text", "Date":"created_at", "0 = Other Mention,    1 = Probable,  2 = Possible":"label", "Notes":"notes"}, inplace=True)
        ex.rename(columns = {"tweet_id":"docID", "User_ID":"user_id", "Tweet":"text", "Date":"created_at", "0 = Other Mention,    1 = Probable,  2 = Possible":"label", "Notes":"notes"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            raise Exception("I was expecting the preprocessWebMD to be available for classification...")
        
        clsr: ClassifierInt = CNNMultinomial('CNNM1', process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/Coronavirus/Model/Multiclass/CNNMulti1_bestModel.h5')
        ex = clsr.classify(ex)
        ex.to_csv('/tmp/covid19_unlabeled_predicted.tsv', sep='\t')
if __name__ == '__main__':
    assert len(sys.argv)==4, "This pipeline is waiting for 3 arguments: the running location [local/lpc], the mode [train/classify] and the number of iteration"
    # Remove all handlers associated with the root logger object. (avoid import library take priority over my settings)
    
    for handler in lg.root.handlers[:]:
        lg.root.removeHandler(handler)
    
    #define the running environment local/lpc
    if sys.argv[1]=='local':
        properties = DavyMacProperties()
        lg.basicConfig(level=lg.DEBUG,
                   format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                   handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler(properties.logFilePath)])
        lg.info("Running locally.")
    elif sys.argv[1] == 'lpc':
        properties = DavyLPCProperties()
        lg.basicConfig(level=lg.DEBUG,
                       format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                       handlers=[lg.StreamHandler(sys.stdout)])
        lg.info("Running on the lpc.")
    else:
        raise Exception(f"Unknown location for the running location at argv [1], waiting for [local/lpc]: {sys.argv[1]}")
    
    #create the instance of the pipe
    log = lg.getLogger(__name__)
    cvPipe = covid19Pipe()

    #run the pipe in train or classify mode in binary
    if sys.argv[2]=='train':
        cvPipe.trainBinary(properties)
    elif sys.argv[2]=='classify':
        cvPipe.classifyBinary(properties)
    else:
        raise Exception(f"Unknown training/corpus argument at argv [2], waiting for [train/classify]: {sys.argv[2]}")

    
#     #run the pipe in train or classify mode in multi-class 
#     if sys.argv[2]=='train':
#         cvPipe.trainMultiClass(properties)
#     elif sys.argv[2]=='classify':
#         cvPipe.classifyMultiClass(properties)
#     else:
#         raise Exception(f"Unknown training/corpus argument at argv [2], waiting for [train/classify]: {sys.argv[2]}")
    
