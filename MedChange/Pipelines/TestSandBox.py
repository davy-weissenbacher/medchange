'''
Created on Nov 14, 2019

@author: dweissen
'''
import sys
import os
# os.environ["MKL_THREADING_LAYER"] = "GNU"
# os.environ['KERAS_BACKEND']='theano'

import logging as lg
from ActiveLearners.ExpertCommittee import ExpertCommittee
from ActiveLearners.Classifiers.RandomClassifier import RandomClassifier
from numpy import NAN
log = lg.getLogger('TestSandBox')
import pickle as pkl
import copy as cp

from Data.Pool import Pool
from Properties.DavyLocalProperties import DavyLocalProperties
from Properties import PropertiesInt
from Handlers.TwitterHandler import TwitterHandler
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from ActiveLearners.Oracles.GoldStandardOracle import GoldStandardOracle
from ActiveLearners.UncertaintySampler import UncertaintySampler, USQueryingAlgorithm, UncertaintyMeasure
from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
from ActiveLearners.RandomSampler import RandomSampler
from Visualization.PerformanceDrawer import PerformanceDrawer
from Performance.Performance import Scores, Performance


from ActiveLearners.Classifiers.CNNClassifier import CNNClassifier

class TestSandBox(object):
    '''
    This sand box tests and evaluates a given classifier on a simple coprus with and without active learning to see the impact of the algorithms
    The task will be detecting the drug mention in tweets as we have already the data 
    '''
    def __init__(self):
        pass

    def getPool(self, properties: PropertiesInt) -> Pool:
        return self.__loadUnbalancedSMM4HCorpus(properties)
        
    def __loadUnbalancedSMM4HCorpus(self, properties: PropertiesInt) -> Pool:
        #The pool will contains the tweets annotated with drugs
        twth = TwitterHandler()
        annotatedTweets = twth.getAnnotatedExamples(properties.drugTweetsUnbalancedAnnotatedPath)
        
        trainVal = annotatedTweets.sample(frac=0.2, random_state=6)
        unlab = annotatedTweets.drop(trainVal.index, inplace = False)
        
        train = trainVal.sample(frac=0.5, random_state=7)
        val = trainVal.drop(train.index, inplace = False)
        
        test = twth.getTestExamples(properties.drugTweetsUnbalancedTestPath)

        assert len(test['label'].unique())==2, "I have only examples in the test set with one label (most likely negative), I need positive examples too..." 
        assert len(train['label'].unique())==2, "I have only examples in the train set with one label (most likely negative), I need positive examples too..."
        assert len(val['label'].unique())==2, "I have only examples in the val set with one label (most likely negative), I need positive examples too..."
        assert len(unlab['label'].unique())==2, "I have only examples in the unlab set with one label (most likely negative), I need positive examples too..."
        assert (len(train)+len(val)+len(unlab))==len(annotatedTweets), 'I have not correctly split the data, check the code...'
        
        twtPool = Pool(unlabeledExamples = unlab, trainingExamples = train, validationExamples = val, testExamples = test)
        return twtPool
        
    def __loadBalancedSMM4HCorpus(self, properties: PropertiesInt) -> Pool:
        #The pool will contains the tweets annotated with drugs
        twth = TwitterHandler()
        annotatedTweets = twth.getAnnotatedExamples(properties.drugTweetsAnnotatedPath)

        train = annotatedTweets.sample(frac=0.1, random_state=6)
        unlab = annotatedTweets.drop(train.index, inplace = False)
        
        #I'm using the extra validation set which contains duplicates
        val = twth.getUnlabeledExamples(properties.drugTweetsUnlabeledPath)
        
        test = twth.getTestExamples(properties.drugTweetsTestPath)
        
        twtPool = Pool(unlabeledExamples = unlab, trainingExamples = train, validationExamples = val, testExamples = test)
        return twtPool

    def runStandardEvaluation(self, pool: Pool):
        '''
        Run a standard evaluation of a classifier on the data, no AL involved
        :return: write the initial model(s) at /tmp/Initial_classifier.getName().h5, to be reused
        '''
        log.info("Standard Evaluation: I start evaluating the classifier without AL...")
        cpPool: Pool = cp.deepcopy(pool)
        cpPool.addQueryResult(pool.unlabEx, 0)
        #load the word embeddings
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(cpPool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNClassifier('CNN1',process_WebMD))
        #--- To Run with UncertaintySampler:
        actLearner: UncertaintySampler = UncertaintySampler(CNNClassifier('CNN1',process_WebMD))
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        #actLearner: ExpertCommittee = ExpertCommittee([CNNClassifier('CNN1',process_WebMD), CNNClassifier('CNN2',process_WebMD), CNNClassifier('CNN3',process_WebMD), CNNClassifier('CNN4',process_WebMD)])
        #actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))
        
        actLearner.train(cpPool)
         
        actLearner.drawPerfOnEvaluation('/tmp/StandardEvaluation_PerfOnEval.pdf')
        log.info("Start evaluating the classifier on the test set...")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        log.info(scores)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = cpPool.testEx['label']
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','Drug Name Extracted','Drug Name Normalized','Notes'])
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")
        
        
    def runALEvaluation(self, pool: Pool, InitialModelsPath = None):
        '''
        Expect the initial models written in /tmp/Initial_classifier.getName().h5, to be reinstantiate
        '''
        log.info("AL Evaluation: I start evaluating the classifier with AL loop...")        
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy())
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(pool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNClassifier('CNN1', process_WebMD, InitialModelsPath))
        #--- To Run with UncertaintySampler:
        actLearner: UncertaintySampler = UncertaintySampler(CNNClassifier('CNN1', process_WebMD, InitialModelsPath))
        actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        #actLearner: ExpertCommittee = ExpertCommittee([CNNClassifier('CNN1',process_WebMD), CNNClassifier('CNN2',process_WebMD), CNNClassifier('CNN3',process_WebMD), CNNClassifier('CNN4',process_WebMD), CNNClassifier('CNN5',process_WebMD)])
        #actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))

        iteration = 0
        actLearner.train(pool)
        actLearner.drawPerfOnEvaluation(f'/tmp/ALEvaluation_PerfOnEval_I{iteration}.pdf')
        #we compute the improvement on eval
        perfEval = Performance("ExpertCommittee_Evaluation")
        predictionsEval = actLearner.classify(pool.valEx)
        scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
        perfEval.addPerformance(0, scoresEval)
        #we compute the improvement of the al on the test set and add to the scores in performance
        perfTest = Performance("ExpertCommittee_Test")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        perfTest.addPerformance(0, scores)
        
        stillImproving = True
        log.error("TODO: set the budget correctly.")
        budget = len(pool.unlabEx)
         
        while stillImproving:
            if budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={budget}.")
                 
                #Here update of the labels in the visualization
                miExamples = actLearner.getMostInformativeExamples(pool, properties.ExQueriedBatchSize)
                gsOracle.getQueryLabels(miExamples)
                pool.addQueryResult(miExamples, iteration)
                budget = budget - len(miExamples)
                                 
                #training on the batch of examples most recently annotated            
                actLearner.train(pool)
                #update of the decision boundary
                actLearner.drawPerfOnEvaluation(f'/tmp/ALEvaluation_PerfOnEval_I{iteration}.pdf')
                
                #we compute the score on eval
                predictionsEval = actLearner.classify(pool.valEx)
                scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
                perfEval.addPerformance(iteration, scoresEval)
                #we compute the improvement of the classifier on the test set and add to the performance
                predictions = actLearner.classify(pool.testEx)
                scores = actLearner.evaluate(predictions, pool.testEx)
                perfTest.addPerformance(iteration, scores)
                log.error("TODO: see how to compute or interact with human to know if the learner is still improving.")
        
        actLearner.drawPerformance(perfEval, f'/tmp/ALEvaluation_PerfOnEval.pdf')
        actLearner.drawPerformance(perfTest, f'/tmp/ALEvaluation_PerfOnTest.pdf')
        # I recompute the predictions for the last iteration to be displayed
        predictions = actLearner.classify(pool.testEx)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = pool.testEx['label']
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','Drug Name Extracted','Drug Name Normalized','Notes'])
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")
        
if __name__ == '__main__':
    for handler in lg.root.handlers[:]:
        lg.root.removeHandler(handler)
    
    properties = DavyLocalProperties()
    lg.basicConfig(level=lg.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler(properties.logFilePath)])
    lg.info("==========================================START==========================================")
    log = lg.getLogger(__name__)
    
    sb: TestSandBox = TestSandBox()
    properties: PropertiesInt = DavyLocalProperties()
    pool = sb.getPool(properties)
    #sb.runStandardEvaluation(pool)
    sb.runALEvaluation(pool, InitialModelsPath='/tmp/Initial_CNN1.h5')
    
    