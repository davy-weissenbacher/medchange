'''
Created on Jan 17, 2020
Environment: 
Package                          Version            
-------------------------------- -------------------
absl-py                          0.9.0              
appnope                          0.1.0              
astor                            0.8.1              
async-generator                  1.10               
attrs                            19.3.0             
backcall                         0.1.0              
beautifulsoup4                   4.8.2              
bert-for-tf2                     0.13.5             
bleach                           3.1.4              
cachetools                       4.0.0              
certifi                          2019.11.28         
chardet                          3.0.4              
cycler                           0.10.0             
decorator                        4.4.2              
defusedxml                       0.6.0              
Deprecated                       1.2.7              
dill                             0.3.1.1            
entrypoints                      0.3                
gast                             0.2.2              
google-auth                      1.12.0             
google-auth-oauthlib             0.4.1              
google-pasta                     0.2.0              
grpcio                           1.27.2             
h5py                             2.10.0             
idna                             2.9                
importlib-metadata               1.6.0              
ipykernel                        5.2.0              
ipython                          7.13.0             
ipython-genutils                 0.2.0              
jedi                             0.16.0             
Jinja2                           2.11.1             
joblib                           0.14.1             
json5                            0.9.4              
jsonschema                       3.2.0              
jupyter-client                   6.1.2              
jupyter-core                     4.6.3              
jupyter-server                   0.1.1              
jupyterlab                       2.0.1              
jupyterlab-pygments              0.1.0              
jupyterlab-server                1.0.7              
Keras                            2.3.1              
Keras-Applications               1.0.8              
keras-bert                       0.81.0             
keras-embed-sim                  0.7.0              
keras-layer-normalization        0.14.0             
keras-multi-head                 0.22.0             
keras-pos-embd                   0.11.0             
keras-position-wise-feed-forward 0.6.0              
Keras-Preprocessing              1.1.0              
keras-self-attention             0.41.0             
keras-transformer                0.32.0             
kiwisolver                       1.2.0              
lxml                             4.5.0              
Markdown                         3.2.1              
MarkupSafe                       1.1.1              
matplotlib                       3.2.1              
mistune                          0.8.4              
nbconvert                        5.6.1              
nbformat                         5.0.5              
nltk                             3.4.5              
notebook                         6.0.3              
numpy                            1.18.1             
oauthlib                         3.1.0              
opt-einsum                       3.2.0              
pandas                           1.0.2              
pandocfilters                    1.4.2              
params-flow                      0.7.4              
parso                            0.6.2              
pexpect                          4.8.0              
pickleshare                      0.7.5              
pip                              20.0.2             
prometheus-client                0.7.1              
prompt-toolkit                   3.0.5              
protobuf                         3.11.3             
ptyprocess                       0.6.0              
py-params                        0.9.6              
pyasn1                           0.4.8              
pyasn1-modules                   0.2.8              
Pygments                         2.6.1              
pymongo                          3.10.1             
pyparsing                        2.4.6              
pyrsistent                       0.16.0             
python-dateutil                  2.8.1              
pytz                             2019.3             
PyYAML                           5.3.1              
pyzmq                            19.0.0             
requests                         2.23.0             
requests-oauthlib                1.3.0              
rsa                              4.0                
scikit-learn                     0.22.2.post1       
scipy                            1.4.1              
Send2Trash                       1.5.0              
sentencepiece                    0.1.85             
setuptools                       46.0.0.post20200309
six                              1.14.0             
sklearn                          0.0                
soupsieve                        2.0                
tensorboard                      2.1.1              
tensorflow                       2.1.0              
tensorflow-estimator             2.1.0              
tensorflow-hub                   0.8.0              
termcolor                        1.1.0              
terminado                        0.8.3              
testpath                         0.4.4              
tornado                          6.0.4              
tqdm                             4.45.0             
traitlets                        4.3.3              
twitter                          1.18.0             
urllib3                          1.25.8             
vaderSentiment                   3.3.1              
voila                            0.1.21             
wcwidth                          0.1.9              
webencodings                     0.5.1              
Werkzeug                         1.0.1              
wheel                            0.34.2             
wrapt                            1.12.1             
zipp                             3.1.0 
@author: dweissen
'''
import sys
import os
import copy as cp
import logging as lg
import pickle as pkl
from Handlers.MCSTweetsHandler import MCSTweetsHandler
from pandas.core.frame import DataFrame

log = lg.getLogger('TransferTest')
import pandas as pd

from Properties import PropertiesInt
from Properties.DavyLocalProperties import DavyLocalProperties
from Properties.DavyMacProperties import DavyMacProperties
from Handlers.WebMDReviewsHandler import WebMDReviewsHandler
from Data.Pool import Pool
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from ActiveLearners.RandomSampler import RandomSampler
from ActiveLearners.UncertaintySampler import UncertaintySampler, USQueryingAlgorithm, UncertaintyMeasure
from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
from ActiveLearners.Classifiers.CNNClassifier import CNNClassifier
from ActiveLearners.Classifiers.RegExFinder import RegExFinder
from ActiveLearners.Oracles.GoldStandardOracle import GoldStandardOracle
from Performance.Performance import Performance


class TranferTestPipe(object):
    '''
    This pipeline will evaluate the interest of active+transfer learning using our webMD and tweets annotated data
    Uses 2 goldstandardOracles to create the pools used during AL iterations
    '''
    def __init__(self):
        pass
    
    
    def getWebMDPool4GS(self, properties: PropertiesInt) -> Pool:
        '''
        Create a Pool of webMD reviews with 0.9 of the training examples as unlabeled and 0.1 as seed
        I do not use the unlabeled set of 95960 examples, for this pipeline since I am using a goldstandard oracle
        :return: a pool
        '''
        wrh = WebMDReviewsHandler()
     
        train = wrh.getTrainExamples(properties.WebMDTrainingReviewsPath)
        val = wrh.getValExamples(properties.WebMDValReviewsPath)
        test = wrh.getTestExamples(properties.WebMDTestReviewsPath)
        
        #use 10% of the training examples as seed
        seed = train.sample(frac=0.1, random_state=6)
        #remaining as unlabeled
        unlabReviews = train.drop(seed.index, inplace=False)
        assert ((len(seed)+len(unlabReviews))==len(train)), f"Error in the code, seed {len(seed)}+ unlabeled {len(unlabReviews)} should be equal to the training set {len(train)}."
         
        webMDPool4GS = Pool(unlabeledExamples = unlabReviews, trainingExamples = seed, validationExamples = val, testExamples = test)
        return webMDPool4GS
    
    def getTweetsPool4GS(self, properties: PropertiesInt) -> Pool:
        '''
        Create a Pool of tweets annotated with Medical Change Status, 0.9 of the training examples used as unlabeled and 0.1 as seed
        :return: a pool
        '''
        mcsth = MCSTweetsHandler()
        
        train = mcsth.getTrainExamples(properties.MCSTweetsTrainingPath)
        val = mcsth.getValExamples(properties.MCSTweetsValPath)
        test = mcsth.getTestExamples(properties.MCSTweetsTestPath)
        
        #keep only 10% for the seed
        posTrain = train[train['label']==1]
        negTrain = train[train['label']==0]
        posSeed = posTrain.sample(frac=0.1, random_state=6)
        negSeed = negTrain.sample(frac=0.1, random_state=6)
        # remaining are for unlabeled
        posUnlab = posTrain.drop(posSeed.index, inplace=False)
        negUnlab = negTrain.drop(negSeed.index, inplace=False)
        assert ((len(posSeed)+len(posUnlab))==len(posTrain)), f"Error in the code, pos seed {len(posSeed)}+ pos unlabeled {len(posUnlab)} should be equal to the pos training set {len(posTrain)}."        
        assert ((len(negSeed)+len(negUnlab))==len(negTrain)), f"Error in the code, neg seed {len(negSeed)}+ neg unlabeled {len(posUnlab)} should be equal to the neg training set {len(negTrain)}."        
        
        seed = pd.concat([posSeed, negSeed])
        seed = seed.sample(frac=1.0, random_state=33)
#         seed = seed.set_index('docID', verify_integrity=True)
        unlab = pd.concat([posUnlab, negUnlab])
        unlab = unlab.sample(frac=1.0, random_state=33)
#         unlab = unlab.set_index('docID', verify_integrity=True)
        
        tweetPool4GS = Pool(unlabeledExamples = unlab, trainingExamples = seed, validationExamples = val, testExamples = test)
        return tweetPool4GS
    
    def getWEbMDTweetsPool4GS(self, properties: PropertiesInt) -> Pool:
        '''
        Create a Pool of WebMD reviews and tweets annotated with Medical Change Status, 0.9 of the training examples used as unlabeled and 0.1 as seed
        The validation set and test set are the val and test set of the tweets; We did not use the validation and test set of webMD to be able compare the results with transfer
        :return: a pool
        '''
        mcsth = MCSTweetsHandler()
        trainTwt = mcsth.getTrainExamples(properties.MCSTweetsTrainingPath)
        val = mcsth.getValExamples(properties.MCSTweetsValPath)
        test = mcsth.getTestExamples(properties.MCSTweetsTestPath)
        
        wrh = WebMDReviewsHandler()
        trainWbMD = wrh.getTrainExamples(properties.WebMDTrainingReviewsPath)
        #disguised the webMD Reviews into "tweets"
        trainWbMD.rename(columns={'drug':'Drug Name Extracted'}, inplace=True)
        trainWbMD['user_id'] = '000'
        trainWbMD['created_at'] = '3/15/2021 12:00:00 AM'
        trainWbMD['Drug Name Normalized'] = trainWbMD['Drug Name Extracted']
        trainWbMD.drop(columns=['Ease of Use','Effectiveness','Satisfaction','Reason: \n1= Discontinued\n2=Finished Treatment \n3=Switched \n4=Regimen Change \n5=Other','Other Drug','ADR 1=Yes 0=No  '], inplace=True)
        
        #create the mix webMD - Tweets
        train = pd.concat([trainTwt, trainWbMD])
        assert len(train)==(9340+1038+5308+590), 'I did not find the number of examples expected in the concatenated training set, check the code.'
        
        #now create the unlabel pool
        #keep only 10% for the seed
        posTrain = train[train['label']==1]
        negTrain = train[train['label']==0]
        posSeed = posTrain.sample(frac=0.1, random_state=6)
        negSeed = negTrain.sample(frac=0.1, random_state=6)
        # remaining are for unlabeled
        posUnlab = posTrain.drop(posSeed.index, inplace=False)
        negUnlab = negTrain.drop(negSeed.index, inplace=False)
        assert ((len(posSeed)+len(posUnlab))==len(posTrain)), f"Error in the code, pos seed {len(posSeed)}+ pos unlabeled {len(posUnlab)} should be equal to the pos training set {len(posTrain)}."        
        assert ((len(negSeed)+len(negUnlab))==len(negTrain)), f"Error in the code, neg seed {len(negSeed)}+ neg unlabeled {len(posUnlab)} should be equal to the neg training set {len(negTrain)}."        
        
        seed = pd.concat([posSeed, negSeed])
        seed = seed.sample(frac=1.0, random_state=33)
#         seed = seed.set_index('docID', verify_integrity=True)
        unlab = pd.concat([posUnlab, negUnlab])
        unlab = unlab.sample(frac=1.0, random_state=33)
#         unlab = unlab.set_index('docID', verify_integrity=True)
        
        twwbmdPool4GS = Pool(unlabeledExamples = unlab, trainingExamples = seed, validationExamples = val, testExamples = test)
        return twwbmdPool4GS
    
    def runStandardEvaluation(self, pool: Pool, InitialModelsPath:str = None, training=True):
        '''
        Run a standard evaluation of a classifier on the data, no AL involved
        :param: pool with the training, val and test available
        :param: InitialModelsPath a path to a model ready to be loaded (or a folder containing models ready to be loaded), it can be an initial model (random weights) or a trained model ready to be used
        :param: training true if you want to start/continue the training, False will be just an evaluation of the model given
        :return: write the initial model(s) at /tmp/Initial_classifier.getName().h5, to be reused
        '''
        log.info("Standard Evaluation: I start evaluating the classifier without AL...")
        cpPool: Pool = cp.deepcopy(pool)
        # I used train + unlabeled examples
        cpPool.addQueryResult(pool.unlabEx, 0) #the unlabeled are actually labeled for this pool s everything is in training
        #load the word embeddings
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(cpPool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
            
        if not training:
            assert InitialModelsPath is not None, f"I'm asked to run an evaluation but no model is given, check  if it's what you want to do, if this is what you want just pass an empty string."
        
        #--- To Run with RandomSampler:
        actLearner: RandomSampler = RandomSampler(CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath))
        #actLearner: RandomSampler = RandomSampler(RegExFinder('RE1', RegExCorpus='Twitter', verbose=True))
        #--- To Run with UncertaintySampler:
        #actLearner: UncertaintySampler = UncertaintySampler(CNNTF2Classifier('CNN1', process_WebMD, InitialModelsPath))# there is no AL iteration
        #actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        #actLearner: ExpertCommittee = ExpertCommittee([CNNClassifier('CNN1',process_WebMD), CNNClassifier('CNN2',process_WebMD), CNNClassifier('CNN3',process_WebMD), CNNClassifier('CNN4',process_WebMD)])
        #actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/WebMDReviewsWithAL/Committee/Run5/CNN1_bestModel.h5_ALiteration24.0'), 
        #                                              CNNTF2Classifier('CNN2',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/WebMDReviewsWithAL/Committee/Run5/CNN2_bestModel.h5_ALiteration24.0'),
        #                                              CNNTF2Classifier('CNN3',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/WebMDReviewsWithAL/Committee/Run5/CNN3_bestModel.h5_ALiteration24.0'),
        #                                              CNNTF2Classifier('CNN4',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/WebMDReviewsWithAL/Committee/Run5/CNN4_bestModel.h5_ALiteration24.0'),
        #                                              CNNTF2Classifier('CNN5',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/WebMDReviewsWithAL/Committee/Run5/CNN5_bestModel.h5_ALiteration24.0')])
        #actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))
        
        if training:
            log.info("Start training the model...")
            actLearner.train(cpPool)
            actLearner.drawPerfOnEvaluation('/tmp/StandardEvaluation_PerfOnEval.pdf')
         
        log.info("Start evaluating the classifier on the validation set...")
        predictions = actLearner.classify(pool.valEx)
        scores = actLearner.evaluate(predictions, pool.valEx)
        log.info(scores)
        log.info("Start evaluating the classifier on the test set...")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        log.info(scores)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = cpPool.testEx['label']
        if 'user_id' in set(predictions.columns):
            predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','Drug Name Extracted','Drug Name Normalized','Notes'])
        else:
            predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t') 
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")

    
    def runALEvaluation(self, pool: Pool, InitialModelsPath = None):
        '''
        Expect the initial models written in /tmp/Initial_classifier.getName().h5, to be reinstantiated
        '''
        if InitialModelsPath is not None:
            log.info(f"AL Evaluation: I start evaluating the classifier with AL loop, given an existing model @{InitialModelsPath}...")
        else:
            log.info("AL Evaluation: I start evaluating the classifier with AL loop...")        
        gsOracle: GoldStandardOracle = GoldStandardOracle(pool.unlabEx.copy())
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists('/tmp/preprocessWebMD.pkl'):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
        else:
            process_WebMD=PreprocessWebMD(pool.getAllTexts(), properties)
            process_WebMD.generate_indices_embedding(properties)
            pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNTF2Classifier('CNN1', process_WebMD, InitialModelsPath))
        #--- To Run with UncertaintySampler:
#         actLearner: UncertaintySampler = UncertaintySampler(CNNTF2Classifier('CNN1', process_WebMD, InitialModelsPath))
#         actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        #--- To Run with Expert Committee:
        actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath=None), CNNTF2Classifier('CNN2',process_WebMD, InitialModelsPath=None), CNNTF2Classifier('CNN3',process_WebMD, InitialModelsPath=None), CNNTF2Classifier('CNN4',process_WebMD, InitialModelsPath=None), CNNTF2Classifier('CNN5',process_WebMD, InitialModelsPath=None)])
        actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))

        iteration = 0
        actLearner.train(pool)
        actLearner.drawPerfOnEvaluation(f'/tmp/ALEvaluation_PerfOnEval_I{iteration}.pdf')
        #we compute the improvement on eval
        perfEval = Performance("Uncertainty_Evaluation")
        predictionsEval = actLearner.classify(pool.valEx)
        scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
        perfEval.addPerformance(0, scoresEval)
        #we compute the improvement of the al on the test set and add to the scores in performance
        perfTest = Performance("Uncertainty_Test")
        predictions = actLearner.classify(pool.testEx)
        scores = actLearner.evaluate(predictions, pool.testEx)
        perfTest.addPerformance(0, scores)
        
        stillImproving = True
        log.error("TODO: set the budget correctly.")
        budget = len(pool.unlabEx)
         
        while stillImproving:
            if budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={budget}.")
                 
                miExamples = actLearner.getMostInformativeExamples(pool, properties.ExQueriedBatchSize)
                gsOracle.getQueryLabels(miExamples)
                pool.addQueryResult(miExamples, iteration)
                budget = budget - len(miExamples)
                                 
                #training on the batch of examples most recently annotated            
                actLearner.train(pool)
                actLearner.drawPerfOnEvaluation(f'/tmp/ALEvaluation_PerfOnEval_I{iteration}.pdf')
                
                #we compute the score on eval
                predictionsEval = actLearner.classify(pool.valEx)
                scoresEval = actLearner.evaluate(predictionsEval, pool.valEx)
                perfEval.addPerformance(iteration, scoresEval)
                #we compute the improvement of the classifier on the test set and add to the performance
                predictions = actLearner.classify(pool.testEx)
                scores = actLearner.evaluate(predictions, pool.testEx)
                perfTest.addPerformance(iteration, scores)
                log.error("TODO: see how to compute or interact with human to know if the learner is still improving.")

        log.info("=> performance on the VAL set:")
        log.info(perfEval)        
        log.info("=> performance on the TEST set:")
        log.info(perfTest)
        actLearner.drawPerformance(perfEval, f'/tmp/ALEvaluation_PerfOnEval.pdf')
        actLearner.drawPerformance(perfTest, f'/tmp/ALEvaluation_PerfOnTest.pdf')
        # I recompute the predictions for the last iteration to be displayed
        predictions = actLearner.classify(pool.testEx)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = pool.testEx['label']
        if 'user_id' in set(predictions.columns):
            predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','Drug Name Extracted','Drug Name Normalized','Notes'])
        else:
            predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t')
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")


    def predict(self, pathCorpus:str, bestModelPath: str):
        """
        :param pathCorpus: the path to the corpus in tsv format with tweet_id and text column expected
        :param bestModelPath: the path to the model or the folder of the models
        :return write the predictions in the prediction column and write the file in tmp
        """
        embeddingsPath = '/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Pickle/preprocessWebMD.pkl'
                
        tweets = pd.read_csv(pathCorpus, sep='\t')#, columns=['tweet_id', 'user_id','text','created_at','drug'])
        assert 'text' in set(tweets.columns) and 'tweet_id' in set(tweets.columns), f"I was expecting two columns tweet_id and text, I got {tweets.columns}, check the data." 
        tweets.set_index('tweet_id', verify_integrity=True, inplace=True)
        
        log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
        if os.path.exists(embeddingsPath):
            log.warning(f"Im reading an existing process_WebMD pickled in temp.")
            process_WebMD = pkl.load(open(embeddingsPath, 'rb'))
        else:
            raise Exception(f"I was expecting a pickle of the embeddings already available @{embeddingsPath}")

        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNTF2Classifier('CNN1', process_WebMD, InitialModelsPath))
        #actLearner: RandomSampler = RandomSampler(RegExFinder('RE1'))
        #--- To Run with UncertaintySampler:
        #actLearner: UncertaintySampler = UncertaintySampler(CNNTF2Classifier('CNN1', process_WebMD, InitialModelsPath))
        #actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        
        #I take the best model, an expert committee the second run with 0.53 on the test set
        
        #--- To Run with Expert Committee:
        actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/TransferWithAL/ExpertCommittee/Run2/CNN1_bestModel.h5_ALiteration15.0'), CNNTF2Classifier('CNN2',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/TransferWithAL/ExpertCommittee/Run2/CNN2_bestModel.h5_ALiteration15.0'), CNNTF2Classifier('CNN3',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/TransferWithAL/ExpertCommittee/Run2/CNN3_bestModel.h5_ALiteration15.0'), CNNTF2Classifier('CNN4',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/TransferWithAL/ExpertCommittee/Run2/CNN4_bestModel.h5_ALiteration15.0'), CNNTF2Classifier('CNN5',process_WebMD, InitialModelsPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/ExperimentsResults/Experiments/TransferWithAL/ExpertCommittee/Run2/CNN5_bestModel.h5_ALiteration15.0')])
        actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))

        predictions = actLearner.classify(tweets)
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', index=True)
        log.info(f"Predictions have been written @/tmp/prediction_ActiveLearner.tsv")
        

if __name__ == '__main__':
    #properties: PropertiesInt = DavyLocalProperties()
    properties: PropertiesInt = DavyMacProperties()
    lg.basicConfig(level=lg.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler(properties.logFilePath)])
    lg.info("==========================================START==========================================")
    log = lg.getLogger(__name__)
    
    #Create the 2 pools needed for the experiments:
    ttp: TranferTestPipe = TranferTestPipe()
    wmdPool = ttp.getWebMDPool4GS(properties)
    log.info(f'Loaded webmMD pool: {wmdPool}')
    twtPool = ttp.getTweetsPool4GS(properties)
    log.info(f'Loaded MCStweets pool: {twtPool}')
    
    #Create a Pool with the training set of tweets and webMD reviews, val and test are tweets only
    wmdtwtPool = ttp.getWEbMDTweetsPool4GS(properties)
    log.info(f'Loaded tweets+webMD pool: {wmdtwtPool}')

    bestModelPath = None

#======================================================================================================
# Experiments with WebMD transfer and Tweets

    #Comment or uncomment with the expected parameters for the different experiments:
    #log.info("==> Run a standard evaluation on WebMD with MCS")
    #ttp.runStandardEvaluation(wmdPool, bestModelPath, training=False)
    #log.info("==> Run a AL evaluation on WebMD with MCS")
    #ttp.runALEvaluation(wmdPool, bestModelPath)

    #-------------------------------------------------------------------------------------------------
    # Move manually the best model(s) of the best iteration to the path to be loaded
    # None will create a new one
    # A valid path will read a model (trained or randomly initialized)
    # '' will return an error
    #-------------------------------------------------------------------------------------------------
    #bestModelPath = 'somewhere/disk'
    
    #===> think about the process_WebMD pickled

    #log.info("==> Run a standard evaluation on tweets with MCS")
    #ttp.runStandardEvaluation(twtPool, bestModelPath, training=False)
    #log.info("==> Run a AL evaluation on tweets with MCS")
    #ttp.runALEvaluation(twtPool, bestModelPath)

#======================================================================================================
# Additional experiments asked by a reviewer, merging WebMD and Tweets for training

#     log.info("==> Run a standard evaluation on tweets with MCS training WebMD + tweets")
#     ttp.runStandardEvaluation(wmdtwtPool, bestModelPath, training=True)
    log.info("==> Run a AL evaluation on tweets with MCS training WebMD + tweets")
    ttp.runALEvaluation(wmdtwtPool, bestModelPath)
    
#======================================================================================================    
# Running the classifier already trained
    
#     #run the trained classifier on a list of tweets mentioning drugs
#     ttp.predict(pathCorpus='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/ExtraData/TweetsDrugToClassify.tsv', bestModelPath=bestModelPath)
#     # the file generated may be to big, just split on the 1 and delete the 0s
#     tweets = pd.read_csv('/tmp/prediction_ActiveLearner.tsv', sep='\t')#, columns=['tweet_id', 'user_id','text','created_at','drug'])
#     #tweets1 = tweets[tweets['prediction']==1.0 and tweets['drugs']!="['hydroxychloriquine']"]
#     tweets = tweets[tweets['prediction']==1.0]
#     tweets = tweets[tweets['drugs']!="['hydroxychloriquine']"]
#     tweets = tweets[tweets['drugs']!="['hydrochloroquine']"]
#     
#     tweets.to_csv(f"/tmp/prediction_ActiveLearnerOnly1.tsv", sep='\t', index=False)
    