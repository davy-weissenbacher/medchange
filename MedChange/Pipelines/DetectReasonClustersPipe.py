'''
Created on Aug 4, 2020



@author: dweissen
'''
from Properties import PropertiesInt
from Properties.DavyMacProperties import DavyMacProperties
from Handlers.WebMDReviewsHandler import WebMDReviewsHandler

from Clusterers.ClustererInt import ClustererInt
from Clusterers.LDAClusterer import LDAClusterer
from Clusterers.BERTClusterer import BERTClusterer
from Clusterers.BERTAutoEncoderAnomalyDetector import BERTAutoEncoderAnomalyDetector

import pandas as pd
import numpy as np
from pandas.core.frame import DataFrame
from sklearn import metrics
from sklearn.metrics.cluster import contingency_matrix
import logging as lg
import sys
from ast import literal_eval
from Clusterers.DMMClusterer import DMMClusterer
from keras_preprocessing.image import dataframe_iterator


class DetectReasonClustersPipe(object):
    """
    This pipe applies a clustering algorithm to detect clusters where users describe their reasons.
    :inputs: webMDs reviews or tweets already classified as mentioning a change in medication
    :output: the clusters (maybe a visualization if the tool is ready)
    """
    def __init__(self):
        pass
    
    def loadWebMDReviews(self, properties:PropertiesInt):
        """
        Reads all reviews of WebMD collected in summer 2018 and return a DataFrame
        """
        wrh = WebMDReviewsHandler()
        #unlabeled contains all reviews so also the train/val/test sets
        examples = wrh.getUnlabeledExamples(properties.WebMDUnlabeledReviewsPath)
        examples = examples.rename(columns={'TEXT':'text'})
        return examples
    
    def loadWebMDReviewsWithChangesPredicted(self):
        """
        Reads all reviews of WebMD collected in summer 2018 and annotated by the BERT of the REST service of MedChange
        :return: a dataframe with only the reviews assumed to mention a change in medication
        """
        wrh = WebMDReviewsHandler()
        #unlabeled contains all reviews so also the train/val/test sets
        #examples = wrh.getUnlabeledExamples('/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicationStatusChange/Corpus/ExtraData/WebMDChangePrediction_BERTAll.tsv')
        examples = wrh.getUnlabeledExamples('/Users/dweissen/Documents/AvirerBig/drugsWithClasses.tsv')
        examples = examples.rename(columns={'TEXT':'text'})
        examples = examples[examples['prediction']==1]
        assert len(examples)==53593, f"I have an unexpected number of webMD reviews mentioning changes {len(examples)}, I was expecting 53593"
        return examples

    def loadWebMDReviewsWithChangesADRPredicted(self):
        """
        Reads all reviews of WebMD collected in summer 2018 and annotated by the BERT of the REST service of MedChange plus the ADR system from Arjun version 10/22/2020
        :return: a dataframe with only the reviews assumed to mention a change in medication
        """
        wrh = WebMDReviewsHandler()
        #unlabeled contains all reviews so also the train/val/test sets
        #examples = wrh.getUnlabeledExamples('/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicationNonAdherence/Corpora/WebMDChangePrediction_BERTAll_WithADR.tsv')
        examples = wrh.getUnlabeledExamples('/Users/dweissen/Documents/AvirerBig/drugsWithClasses.tsv')
        examples = examples.rename(columns={'TEXT':'text'})
        examples = examples[(examples['prediction']==1) & (examples['predictionADR']=='noADR')]
        examples.to_csv('/tmp/out.tsv', sep='\t')
        return examples
    
    def restrictToDrugClass(self, docs: DataFrame, classID:str):
        """
        The topics are probably different for different classes of drugs. This function select the drugs that belong to the class of interest.
        :note: the drug classes are found by the getRxClassAPI (https://bitbucket.org/pennhlp/getrxclassapi/src/master/)
        :param docs: a dataframe with parentsIDsTherapeuticUses column to search for the drugs of interest
        :return: docs restricted to the drugs subsumed by the the class ID  
        """
        assert 'parentsIDsTherapeuticUses' in docs.columns, "I was expecting the column parentsIDsTherapeuticUses which contains the drug class to search for."
        docs['keep'] = False
        def getClassIDEx(drugClassID:str):
            if drugClassID is np.nan:
                return False
            if classID in literal_eval(drugClassID):
                return True
            else:
                return False
        docs[['keep']] = docs.apply(lambda row: getClassIDEx(row['parentsIDsTherapeuticUses']), axis=1, result_type='expand')
        docs = docs[docs['keep']==True]
        docs.to_csv("/tmp/out.tsv", sep="\t")
        return docs
        
    def loadTweets(self, pathTSV:str) -> DataFrame:
        log.info(f'Read annotated Tweets with Medication Change @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        
        log.fatal("I cut the data to speed up dev...")
        ex = ex[:500]
        
        return ex
    
    def evaluateTopicsOnTweets(self, docWithPredictions:DataFrame, pathGoldStandard:str):
        """
        :param docWithPredictions: a df with the docs and the topic associated (can be a superset containing the docs of the ground truth)
        :param pathGoldStandard: a df with the docs associated with the topics defined by the human annotators
        :return: an accuracy score
        """
        docWithTruth = pd.read_csv(pathGoldStandard, sep='\t')        
        #sanitary check
        docsGold = sorted(set(docWithTruth["tweet_id"].tolist())) 
        docsPred = set(docWithPredictions["docID"].tolist())
        if not (docsPred.issubset(docsGold)):
            for twtID in docsPred:
                if twtID not in docsPred:
                    raise Exception(f"I can't find the tweet annotated by human in the prediction: {twtID}, check why...")

        docWithPredictions.set_index(keys=['docID'], inplace=True, verify_integrity=True)
        docWithTruth.set_index(keys=['tweet_id'], inplace=True, verify_integrity=True)
        docWithTruth['predicted'] = docWithPredictions.loc[docsGold]['TopicPredicted']
        log.info(f"Adjust Rand Index : {metrics.adjusted_rand_score(docWithTruth['Reason ID'], docWithTruth['predicted'])}")
        log.info(f"Adjust Mutual Info: {metrics.adjusted_mutual_info_score(docWithTruth['Reason ID'], docWithTruth['predicted'])}")
        log.info(f"Confusion Matrix  : {contingency_matrix(docWithTruth['Reason ID'], docWithTruth['predicted'])}")
        
    def testClusterOnTweets(self):
        pathTweets = '/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicationStatusChange/ExperimentsResults/Experiments/ManualAnnotationMNA/prediction_ActiveLearnerOnly1.tsv'
        docsMC = self.loadTweets(pathTweets)
        log.info("Apply an LDA clusterer:")
        cl: ClustererInt = LDAClusterer(properties)
        #log.info("Apply an DMM clusterer:")
        #cl: ClustererInt = DMMClusterer(properties)
#         log.info("Apply a BERT clusterer:")
#         cl: ClustererInt = BERTClusterer(properties, 'AggloClust')
        
        docTopicPred = cl.cluster(docsMC)
        #docTopicPred = pd.read_csv("/Users/dweissen/Documents/AvirerBig/BERTClustererOutput.tsv", sep='\t')
        
        #evaluate the topics
        self.evaluateTopicsOnTweets(docWithPredictions=docTopicPred, pathGoldStandard="/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/ExtraData/MSCMNAAnnotation3010Reasons.csv")
        
    def testClusterOnWebMD(self, properties: PropertiesInt):
        docs = self.loadWebMDReviewsWithChangesADRPredicted()
        #docs = self.loadWebMDReviewsWithChangesPredicted()
        docs =self.restrictToDrugClass(docs, classID="D000924")
        log.info("Apply an LDA clusterer:")
        cl: ClustererInt = LDAClusterer(properties)
#         log.info("Apply an DMM clusterer:")
#         cl: ClustererInt = DMMClusterer(properties)
        
        docTopicPred = cl.cluster(docs)
        
    def detectAnomalyInWebMD(self, properties: PropertiesInt):
        """
        Try a new thing to detect rare reasons of non-adherence, reviews with rare MNA should be different from the ones with normal
        """
        #docs = self.loadWebMDReviewsWithChangesPredicted()
        docs = self.loadWebMDReviewsWithChangesADRPredicted()
        docs =self.restrictToDrugClass(docs, classID="D000924")
        print(docs)
        log.info("Search for abnormal reviews:")
        bae = BERTAutoEncoderAnomalyDetector(properties)
         
        bae.detect(docs)
        
    
if __name__ == '__main__':
    properties: PropertiesInt = DavyMacProperties()
    lg.basicConfig(level=lg.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler(properties.logFilePath)])
    lg.info("==========================================START==========================================")
    log = lg.getLogger(__name__)
    

    drcp: DetectReasonClustersPipe = DetectReasonClustersPipe()
    #drcp.testClusterOnTweets()
    drcp.testClusterOnWebMD(properties)
    #drcp.detectAnomalyInWebMD(properties)
    
    
    