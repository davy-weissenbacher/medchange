'''
Created on Oct 7, 2019

@author: davy
'''

import sys
import logging as lg
# from theano.tensor import inplace
log = lg.getLogger('MainPipeline')
import dill
import numpy as np
import os
import pdb
# for gpu training
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import pickle as pkl
sys.path.append("..")
# sys.path='/home/gesy17/MNA/medchange/MedChange/'
print(sys.path)
from Handlers.WebMDReviewsHandler import WebMDReviewsHandler
from Handlers.TwitterHandler import TwitterHandler
from Properties.DavyLocalProperties import DavyLocalProperties
from Properties.DavyLPCProperties import DavyLPCProperties
from Properties.SuyuLocalProperties import SuyuLocalProperties
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Performance.Performance import Scores, Performance
from Data.Pool import Pool
# from ActiveLearners.UncertaintySampler import UncertaintySampler, USQueryingAlgorithm, UncertaintyMeasure
from ActiveLearners.ExpertCommittee import ExpertCommittee, ECQueryingAlgorithm, DisagreementAlgorithm
from ActiveLearners.RandomSampler import RandomSampler
from ActiveLearners.Oracles.GoldStandardOracle import GoldStandardOracle
from ActiveLearners.Oracles.HumanOracle import HumanOracle
from ActiveLearners.Classifiers.RandomClassifier import RandomClassifier
from ActiveLearners.UncertaintySampler import UncertaintySampler, USQueryingAlgorithm, UncertaintyMeasure
# from ActiveLearners.Classifiers.ELMOClassifier import ELMOClassifier
from ActiveLearners.Classifiers.CNNClassifier import CNNClassifier
from ActiveLearners.Classifiers.LSTMClassifier import LSTMClassifier
from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
from ActiveLearners.Classifiers.ELMo import ELMoClassifier
from ActiveLearners.Classifiers.TopicFinder import TopicFinder
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import tensorflow as tf
config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
sess = tf.Session(config=config)

class MedChangePipe_reviewsInformed(object):
    '''
    This pipeline uses active learning and transfer learning to detect reviews and tweets mentioning a change of status in the patient medication plan
    This particular pipeline uses for active learning only the subset of WebMD reviews annotated (i.e. a gold standard oracle), and queries a human annotator for labeling the tweets (i.e. a human oracle)
    Because we don't have a working interface to query human the program has to be call multiple time with different parameters:
    - the first call should be with the "source" parameter: this will run the active learning session on the webMD corpus
    - the second call should be with the "target" "iteration"=0 parameters, this will:
      - run the first iteration of the active learning session on the seed tweets 
      - select the first set of unlabeled tweets to be annotated
      - dump the session and exit while waiting for the annotations  
    - the next calls should be with the "target" "iteration"=i (i>0) parameters, this will:
      - load and resume the session
      - load the new tweets annotated
      - continue training the model(s)
      - check if we have the budget and the need to continue the training:
        - if yes, selecting a new set of unlabeled tweets to be annotated and dump the session and exit while waiting for the annotations
        - if no, exit
    
    => There is already some frameworks implemented for active learning, to be checked -> https://modal-python.readthedocs.io/
    
    '''
        
    def __setwebMDPool(self, properties):
        """
        Create a pool composed by all reviews annotated with a split 70/15/15 for train, dev, test and all unlabeled examples for unlab
        TODO: change with the real data when available
        """
        wrh = WebMDReviewsHandler()

        train = wrh.getTrainExamples(properties.WebMDTrainingReviewsPath)
        val = wrh.getValExamples(properties.WebMDValReviewsPath)
        test = wrh.getTestExamples(properties.WebMDTestReviewsPath)
        
        unlab = wrh.getUnlabeledExamples(properties.WebMDUnlabeledReviewsPath)
        #unlab contain all reviews available, so I need to remove the labeled ones which are in train and val
        totalReviews = len(unlab)
        unlab.drop(test.index, inplace = True)
        unlab.drop(train.index, inplace = True)
        unlab.drop(val.index, inplace = True)
        assert (len(unlab)+len(train)+len(val)+len(test))==totalReviews, "I did not retrieve the total number of reviews expected when checking during the construction of the pool, check the code or data."
        webMDPool = Pool(unlabeledExamples = unlab, trainingExamples = train, validationExamples = val, testExamples = test)
        return webMDPool
    
    
    def __setwebMDPool4GoldStandardOracle(self, properties):
        '''
        Create a Pool of webMD reviews with 0.9 of the training examples as unlabeled and 0.1 as seed
        I do not use the unlabeled set of 95960 examples, for this pipeline since I am using a goldstandard oracle
        :return: a pool
        '''
        wrh = WebMDReviewsHandler()
     
        train = wrh.getTrainExamples(properties.WebMDTrainingReviewsPath)
        val = wrh.getValExamples(properties.WebMDValReviewsPath)
        test = wrh.getTestExamples(properties.WebMDTestReviewsPath)
        
        #use 10% of the training examples as seed
        seed = train.sample(frac=0.1, random_state=6)
        #remaining as unlabeled
        unlabReviews = train.drop(seed.index, inplace=False)
        assert ((len(seed)+len(unlabReviews))==len(train)), f"Error in the code, seed {len(seed)}+ unlabeled {len(unlabReviews)} should be equal to the training set {len(train)}."
         
        webMDPool4GS = Pool(unlabeledExamples = unlabReviews, trainingExamples = seed, validationExamples = val, testExamples = test)
        return webMDPool4GS


    def runSourceLearning(self, properties):
        '''
        :param: properties for the application
        :param: iteration the number of iteration in the active learning process
        '''
        #start the active learning process using a gold standard as oracle
        iteration = 0
        
        # webMDPool = self.__setwebMDPool(properties)
        webMDPool4GS = self.__setwebMDPool4GoldStandardOracle(properties)
        log.info(f"WebMD Pool created -> {str(webMDPool4GS)}")
        gsOracle: GoldStandardOracle = GoldStandardOracle(webMDPool4GS.unlabEx.copy())

        if properties.name=='Davy':
            log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
            if os.path.exists('/tmp/preprocessWebMD.pkl'):
                log.warning(f"Im reading an existing process_WebMD pickled in temp.")
                process_WebMD = pkl.load(open('/tmp/preprocessWebMD.pkl', 'rb'))
            else:
                process_WebMD=PreprocessWebMD(webMDPool4GS.getAllTexts(), properties)
                process_WebMD.generate_indices_embedding(properties)
                pkl.dump(process_WebMD, open('/tmp/preprocessWebMD.pkl', 'wb'))
        else:
            if os.path.exists('/home/gesy17/MNA/pickle/preprocessWebMD.pkl'):
                log.warning(f"Im reading an existing process_WebMD pickled in temp.")
                process_WebMD = pkl.load(open('/home/gesy17/MNA/pickle/preprocessWebMD.pkl', 'rb'))
            else:
                process_WebMD=PreprocessWebMD(webMDPool4GS.getAllTexts(), properties)
                process_WebMD.generate_indices_embedding(properties)
                pkl.dump(process_WebMD, open('/home/gesy17/MNA/pickle/preprocessWebMD.pkl', 'wb'))
                
        
        #--- To Run with RandomSampler:
        #actLearner: RandomSampler = RandomSampler(CNNClassifier('CNN1', process_WebMD))
        
        #--- To Run with UncertaintySampler:
        #tf = TopicFinder('Topic1', properties)        
        #tf.train(webMDPool) # this will create the initial model (load if one exist, or create it on the full set of texts) for the topic classifier and stop the learning 
        #actLearner: UncertaintySampler = UncertaintySampler(tf)
        ##actLearner: UncertaintySampler = UncertaintySampler(CNNClassifier('CNN1', process_WebMD))
        #actLearner.setQueryingAlgorithm(USQueryingAlgorithm(UncertaintyMeasure.Entropy))
        
        #--- To Run with Expert Committee:
        #log.fatal("TODO: handle properly the memory leak, the expert committee does not work so far.")
#         tf = TopicFinder('Topic1', properties)   
#         tf.train(webMDPool) 
        actLearner: ExpertCommittee = ExpertCommittee([LSTMClassifier('LSTM1',process_WebMD),CNNClassifier('LSTM2',process_WebMD), CNNClassifier('CNN2',process_WebMD)])
        actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.voteEntropy, actLearner))
        
        actLearner.train(webMDPool4GS)
        actLearner.clear()
                
        #we compute the score on eval
        perfEval = Performance("ExpertCommittee_Evaluation")
        predictionsEval = actLearner.classify(webMDPool4GS.valEx)
        scoresEval = actLearner.evaluate(predictionsEval, webMDPool4GS.valEx)
        perfEval.addPerformance(0, scoresEval)
        #we compute the score on the test set and add to the scores in performance
        perfTest = Performance("ExpertCommittee_Test")
        predictions = actLearner.classify(webMDPool4GS.testEx)
        scores = actLearner.evaluate(predictions, webMDPool4GS.testEx)
        perfTest.addPerformance(0, scores)
                
        stillImproving = True
        log.error("TODO: set the budget correctly.")
        budget = len(webMDPool4GS.unlabEx)
        
        while stillImproving:
            if budget<=0:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                break
            else:
                iteration = iteration + 1
                log.debug(f"At iteration {iteration}, budget={budget}.")
                miExamples = actLearner.getMostInformativeExamples(webMDPool4GS, properties.ExQueriedBatchSize)
                gsOracle.getQueryLabels(miExamples)
                webMDPool4GS.addQueryResult(miExamples, iteration)
                budget = budget - len(miExamples)
                
                #training on the batch of examples most recently annotated            
                actLearner.train(webMDPool4GS)
                actLearner.clear()
                
                #we compute the score on eval
                predictionsEval = actLearner.classify(webMDPool4GS.valEx)
                scoresEval = actLearner.evaluate(predictionsEval, webMDPool4GS.valEx)
                perfEval.addPerformance(iteration, scoresEval)
                #we compute the improvement of the classifier on the test set and add to the performance
                predictions = actLearner.classify(webMDPool4GS.testEx)
                scores = actLearner.evaluate(predictions, webMDPool4GS.testEx)
                perfTest.addPerformance(iteration, scores)

        actLearner.drawPerformance(perfEval, f'/tmp/ALEvaluation_PerfOnEval.pdf')
        actLearner.drawPerformance(perfTest, f'/tmp/ALEvaluation_PerfOnTest.pdf')
        log.info(f"Performances on Evaluation set:{perfEval}")
        log.info(f"Performances on Test set:{perfTest}")
        # I recompute the predictions for the last iteration to be displayed
        predictions = actLearner.classify(webMDPool4GS.testEx)
        predictions.rename(columns = {"label":"prediction"}, inplace=True)
        predictions['truth'] = webMDPool4GS.testEx['label']
        predictions.to_csv(f"/tmp/prediction_ActiveLearner.tsv", sep='\t', columns=['user_id','text','created_at','truth','prediction','certainty_label_0','certainty_label_1','Drug Name Extracted','Drug Name Normalized','Notes'])
        log.info(f"prediction written @/tmp/prediction_ActiveLearner.tsv")

    def __setTweetsPool__(self, properties):
        '''
        Create a Pool of tweets with 70/15/15 for train, dev, test
        :return: a pool
        '''
        twth = TwitterHandler()
        annotatedTweets = twth.getAnnotatedExamples(properties.drugTweetsAnnotatedPath)
        unlabTweets = twth.getUnlabeledExamples(properties.drugTweetsUnlabeledPath)
        testTweets = twth.getTestExamples(properties.drugTweetsTestPath)

        #:TODO: split this set into training, validation, and test sets
        train = annotatedTweets.sample(frac=0.7, random_state=6)
        dev = annotatedTweets.drop(train.index, inplace = False)

        assert ((len(train)+len(dev))==len(annotatedTweets)), "Error in the code, train {}+dev {} should equal the input df {}.".format(len(train), len(dev), len(annotatedTweets))
        twtPool = Pool(unlabeledExamples = unlabTweets, trainingExamples = train, validationExamples = dev, testExamples = testTweets)
        return twtPool
            
    def runTargetLearning(self, properties, iteration) -> bool:
        '''
        :param: properties all paths needed
        :param: iteration since we have interaction with annotators we need a count of the learning iterations
        :return: True if the learner continue to improve and it still has some budget for annotation, False otherwise 
        '''
        assert iteration.isnumeric(), f"The second parameter is not a valid number of iteration: {iteration}"        
        log.info(f"Run the MedChangePipeline with iteration: {iteration} on the target corpus.")
              
        if iteration=='0':
            twtPool = self.__setTweetsPool__(properties)
            hmOracle: HumanOracle = HumanOracle()
            
            if properties.name=='Davy':
                log.error("TODO: embeddings take to long to read, so I pickled it, to be removed...")
                if os.path.exists('/tmp/preprocessTweet.pkl'):
                    log.warning(f"Im reading an existing process_Tweet pickled in temp.")
                    process_Tweet = pkl.load(open('/tmp/preprocessTweet.pkl', 'rb'))
                else:
                    process_Tweet=PreprocessWebMD(twtPool.getAllTexts(), properties)
                    process_Tweet.generate_indices_embedding(properties)
                    pkl.dump(process_Tweet, open('/tmp/preprocessTweet.pkl', 'wb'))
            else:
                if os.path.exists('/home/gesy17/MNA/pickle/preprocessTweet.pkl'):
                    log.warning(f"Im reading an existing process_Tweet pickled in temp.")
                    process_Tweet = pkl.load(open('/home/gesy17/MNA/pickle/preprocessWebMD.pkl', 'rb'))
                else:
                    process_Tweet=PreprocessWebMD(twtPool.getAllTexts(), properties)
                    process_Tweet.generate_indices_embedding(properties)
                    pkl.dump(process_Tweet, open('/home/gesy17/MNA/pickle/preprocessWebMD.pkl', 'wb'))

            actLearner: ExpertCommittee = ExpertCommittee([CNNClassifier('CNN1',process_Tweet),CNNClassifier('CNN2',process_Tweet)])
            actLearner.setQueryingAlgorithm(ECQueryingAlgorithm(DisagreementAlgorithm.maxDisagreement, actLearner))
            
            #at iteration 0, I am not testing if the learner is still improving, hopefully it does...
            actLearner.train(twtPool)
            
            self.budget = len(twtPool.unlabEx)
            log.debug(f"At iteration {iteration}, budget={self.budget}.")
            if self.budget>=0:
                self.miExamples = actLearner.getMostInformativeExamples(twtPool, properties.ExQueriedBatchSize)
                hmOracle.sendQueryLabels(properties, self.miExamples, (int(iteration)+1))
                return True
            else:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                return False
        else:
            #we are reading the unlabeled examples from the current iteration           
            self.miExamples = hmOracle.getQueryLabels(properties, self.miExamples, iteration=int(iteration))
            twtPool.addQueryResult(self.miExamples, iteration=int(iteration))
            self.budget = self.budget - len(self.miExamples)
            log.debug(f"At iteration {iteration}, budget={self.budget}.")
            
            actLearner.train(twtPool)
            #at this stage we may not have any budget left
            if self.budget>0:                
                self.miExamples = self.actLearner.getMostInformativeExamples(twtPool, properties.ExQueriedBatchSize)
                hmOracle.sendQueryLabels(properties, self.miExamples, (int(iteration)+1))
                return True
            else:
                log.info("The learner may still improve but it ran out of budget for annotation. Exit...")
                return False

if __name__ == '__main__':
    assert len(sys.argv)==4, "This pipeline is waiting for 3 arguments: the running location [local/lpc], the mode [source/target] and the number of iteration"
    # Remove all handlers associated with the root logger object. (avoid import library take priority over my settings)
    
    for handler in lg.root.handlers[:]:
        lg.root.removeHandler(handler)
    
    #define the running environment local/lpc
    if sys.argv[1]=='local':
    #    properties = DavyLocalProperties()
       properties = SuyuLocalProperties()
       
       lg.basicConfig(level=lg.DEBUG,
                       format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                       handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler(properties.logFilePath)])
       lg.info("Running locally.")

    elif sys.argv[1] == 'lpc':
        properties = DavyLPCProperties()
        lg.basicConfig(level=lg.DEBUG,
                       format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                       handlers=[lg.StreamHandler(sys.stdout)])
        lg.info("Running on the lpc.")
    else:
        raise Exception(f"Unknown location for the running location at argv [1], waiting for [local/lpc]: {sys.argv[1]}")
    
    log = lg.getLogger(__name__)
    mcPipe = MedChangePipe_reviewsInformed()
    if sys.argv[2]=='source':
        mcPipe.runSourceLearning(properties)
    elif sys.argv[2]=='target':
        if sys.argv[3]=='0':
            toContinue = mcPipe.runTargetLearning(properties, sys.argv[3])
            #I dump the full session to go faster
            if toContinue:
                log.info(f"Dumping the session while examples are getting annotated.")
                # This should be noted: dump_session function can not work with logger. I tried to remove all logger, the session can be dumped successfully.
                dill.dump_session(filename=properties.picklePath)
            else:
                log.info("End of the active learning session, exit.")
             
        else:
            log.info(f"Loading the session and resume activity.")
            assert (os.path.exists(properties.picklePath) and os.path.isfile(properties.picklePath)), f"The session serialized was not found at: [{properties.picklePath}], either there was a problem when dumping the session or the learner ran out of budget and the session has terminated." 
            dill.load_session(filename=properties.picklePath)
            toContinue = mcPipe.runTargetLearning(properties, sys.argv[3])
            if toContinue:
                log.info(f"Dumping the session while examples are getting annotated.")
                dill.dump_session(filename=properties.picklePath)
            else:
                if os.path.exists(properties.picklePath) and os.path.isfile(properties.picklePath):
                    log.info("deleting dill session.")
                    os.remove(path=properties.picklePath)
                log.info("End of the active learning session, exit.")
    else:
        raise Exception(f"Unknown mode for the running pipeline at argv [2], waiting for [source/target]: {sys.argv[2]}")
    
