'''
Created on Nov 18, 2019

@author: dweissen
'''

class Scores(object):
    '''
    The scores obtained during a single evaluation
    '''
    def __init__(self, confusionMatrix: list, classificationReport: str, F1positive: float, PrecisionPositive: float, RecallPositive: float, Accuracy: float):
        self.scores = {"confusionMatrix": confusionMatrix, "classificationReport": classificationReport, "F1positive": F1positive, "PrecisionPositive": PrecisionPositive, "RecallPositive": RecallPositive, "Accuracy": Accuracy}
    def getDetails(self):
        return self.scores
    
    def __str__(self):
        out = ""
        for k,v in self.scores.items():
            out = out+f"{k}:\n{v}\n"
        return out


class Performance(object):
    '''
    A class representing the performance of a classifier during multiple evaluation
    This is a dictionary iteration -> scores
    '''
    def __init__(self, classifierName: str):
        '''
        Constructor
        '''
        self.classifier = classifierName
        # a dictionary iteration -> scores
        self.perfHistory = {}

    
    def addPerformance(self, iteration:int, scores: Scores):
        assert not iteration in self.perfHistory.keys(), f"A performance for the iteration {iteration} has already been inserted, check the code."
        self.perfHistory[iteration] = scores
    
        
    def getPerformance(self):
        return self.perfHistory

    
    def __str__(self):
        out = f"Classifier: {self.classifier}\n"
        for ind, score in self.perfHistory.items():
            out =  out + f"Iteration: {ind} => {score}"
        return out
