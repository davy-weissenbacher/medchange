'''
Created on Oct 15, 2019

@author: davy
'''
import abc
from pandas.core.frame import DataFrame
class HandlerInt(object):
    '''
    Interface to define an handler.
    Returns a dataframe containing one example per row with columns docID, text
    '''

    @abc.abstractmethod
    def getAnnotatedExamples(self) -> DataFrame:
        '''
        :return: a set of annotated examples, the dataframe should contains the columns docID, text, label
        '''
        pass
    
    @abc.abstractmethod    
    def getUnlabeledExamples(self) -> DataFrame:
        '''
        :return: a set of unlabeled examples, the dataframe should contains the columns docID, text, label
        '''
        pass
    
    @abc.abstractmethod    
    def getTestExamples(self) -> DataFrame:
        '''
        :return: a set of Test examples, the dataframe should contains the columns docID, text, label
        '''
        pass
    
    @abc.abstractmethod    
    def getValidationExamples(self) -> DataFrame:
        '''
        :return: a set of Validation examples, the dataframe should contains the columns docID, text, label
        '''
        pass