'''
Created on Oct 7, 2019

@author: davy
'''
import numpy as np
from pandas.core.frame import DataFrame
import pandas as pd
import logging as log
log = log.getLogger('WebMDReviewsHandler')

from Handlers.HandlerInt import HandlerInt

class WebMDReviewsHandler(HandlerInt):
    '''
    The handler will read the reviews annotated and returned the examples as dataframe
    '''

    def __init__(self):
        '''
        Constructor
        '''
    
    def getTrainExamples(self, pathTSV)->DataFrame:
        return self.__getTrainReviews(pathTSV)
    def getUnlabeledExamples(self, pathTSV)->DataFrame:
        return self.__getUnlabeledReviews(pathTSV)
    def getValExamples(self, pathTSV)->DataFrame:
        return self.__getValReviews(pathTSV)
    def getTestExamples(self, pathTSV) -> DataFrame:
        return self.__getTestReviews(pathTSV)
        
        
    def __getTrainReviews(self, pathTSV) -> DataFrame:
        '''
        :return: the set of training examples as dataframe with the docID as index, text, label, certainty as columns
        '''
        log.info(f'Read annotated reviews @{pathTSV}')
        df = pd.read_csv(pathTSV, sep='\t')#,encoding='ISO-8859-1')
        log.error("TODO: correctly handle the label when the annotations will be available.")
        assert({'SOURCE_FILE', 'TEXT', 'Medication Status Change \n1=yes\n0=no', 'DRUG'}.issubset(set(df.columns))), "Apparently one column is missing in the file loaded, check the data."
        df.rename(columns={"SOURCE_FILE":"docID", "TEXT":"text", 'Medication Status Change \n1=yes\n0=no':'label', 'DRUG':"drug"}, inplace=True)
        #the validation set has not been extracted from the mongo DB and have a special format for the docID, just converting them to the format expected 
        self.__formatDocID(df)
        df.set_index(keys='docID', inplace=True, verify_integrity=True)
        return df
    
    def __getUnlabeledReviews(self, pathTSV) -> DataFrame:
        '''
        :return: the set of unlabeled examples as dataframe with the docID as index and text as column
        '''
        log.info(f'Read unlabeled reviews @{pathTSV}')
        df = pd.read_csv(pathTSV, sep='\t')
        log.error("TODO: correctly handle the label when the annotations will be available.")
        assert({'pid', 'text', 'drug'}.issubset(set(df.columns))), "Apparently one column is missing in the file loaded, check the data."
        df.rename(columns={"pid":"docID"}, inplace=True)
        df.set_index(keys='docID', inplace=True, verify_integrity=True)
        return df
    
    def __getTestReviews(self, pathTSV) -> DataFrame:
        '''
        :return: the path to the set of test examples as dataframe with the docID as index, text, label, certainty as columns
        '''
        log.info(f'Read validation reviews @{pathTSV}')
        df = pd.read_csv(pathTSV, sep='\t')
        log.error("TODO: correctly handle the label when the annotations will be available.")
        assert({'SOURCE_FILE', 'TEXT', 'Medication Status Change \n1=yes\n0=no','DRUG'}.issubset(set(df.columns))), "Apparently one column is missing in the file loaded, check the data."
        df.rename(columns={"SOURCE_FILE":"docID", "TEXT":"text",'DRUG':"drug", 'Medication Status Change \n1=yes\n0=no':'label'}, inplace=True)
        #the validation set has not been extracted from the mongo DB and have a special format for the docID, just converting them to the format expected 
        self.__formatDocID(df)
        df.set_index(keys='docID', inplace=True, verify_integrity=True)
        return df

    def __getValReviews(self, pathTSV) -> DataFrame:
        '''
        :return: the path to the set of validation examples as dataframe with the docID as index, text, label, certainty as columns
        '''
        log.info(f'Read validation reviews @{pathTSV}')
        df = pd.read_csv(pathTSV, sep='\t')
        log.error("TODO: correctly handle the label when the annotations will be available.")
        assert({'SOURCE_FILE', 'TEXT', 'Medication Status Change \n1=yes\n0=no','DRUG'}.issubset(set(df.columns))), "Apparently one column is missing in the file loaded, check the data."
        df.rename(columns={"SOURCE_FILE":"docID", "TEXT":"text",'DRUG':"drug", 'Medication Status Change \n1=yes\n0=no':'label'}, inplace=True)
        #the validation set has not been extracted from the mongo DB and have a special format for the docID, just converting them to the format expected 
        self.__formatDocID(df)
        df.set_index(keys='docID', inplace=True, verify_integrity=True)
        return df
    
    def __formatDocID(self, examples: DataFrame):
        '''
        :param: examples, set of examples where the docID is formated as SOURCE_FILE ex. reviews_aug07_parsed/10094_000122.txt,
        change the format to a standard one 10094-000122
        '''
        def formatID(id__):
            assert id__.startswith('reviews_aug07_parsed/') and id__.endswith('.txt'), f"I was expecting a string starting with reviews_aug07_parsed/ and finishing with .txt, I got {id__}, check the data."
            id__ = id__[21:-4]
            id__ = id__.replace('_', '-')
            return id__ 
        examples['docID'] = examples['docID'].apply(lambda id__: formatID(id__))
        
        