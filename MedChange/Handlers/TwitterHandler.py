'''
Created on Oct 7, 2019

@author: davy
'''
import pandas as pd
from pandas.core.frame import DataFrame
import logging as log
log = log.getLogger('TwitterHandler')

from Handlers.HandlerInt import HandlerInt

class TwitterHandler(HandlerInt):
    '''
    This handler will read the collection of tweets provided as input
    Returns a dataframe containing one example per row with columns docID, text
    '''


    def __init__(self):
        '''
        Constructor
        '''

    def getAnnotatedExamples(self, pathTSV)->DataFrame:
        return self.__getAnnotatedTweets(pathTSV)
    def getUnlabeledExamples(self, pathTSV)->DataFrame:
        return self.__getUnlabeledTweets(pathTSV)
    def getTestExamples(self, pathTSV)->DataFrame:
        return self.__getTestTweets(pathTSV)

    
    def __getAnnotatedTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of examples annotated
        '''
        log.info(f'Read annotated Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex 
    
    def __getUnlabeledTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of unlabeled examples
        '''
        log.info(f'Read unlabeled Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        log.error("TODO: I'm using the extra validation set which contains duplicates, to remove when the final sets of data will be available")
        ex.drop_duplicates('docID', keep='first', inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex
    
    def __getTestTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of test examples
        '''
        log.info(f'Read Test Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex