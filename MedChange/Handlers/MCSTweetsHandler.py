'''
Created on Jan 17, 2020

@author: dweissen
'''
import pandas as pd
from pandas.core.frame import DataFrame
import logging as log
log = log.getLogger('MCSTweetsHandler')

from Handlers.HandlerInt import HandlerInt


class MCSTweetsHandler(HandlerInt):
    '''
    This handler read the set tweets annotated for Medical change status, they all contain drugs names
    Returns a dataframe containing one example per row with columns docID, text, label
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    def getTrainExamples(self, pathTSV)->DataFrame:
        return self.__getTrainTweets(pathTSV)
    def getUnlabeledExamples(self, pathTSV)->DataFrame:
        raise Exception("Not implemented, we have no unlabeled ex.")
    def getValExamples(self, pathTSV)->DataFrame:
        return self.__getValTweets(pathTSV)
    def getTestExamples(self, pathTSV) -> DataFrame:
        return self.__getTestTweets(pathTSV)
    
   
    def __getTrainTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of examples annotated
        '''
        log.info(f'Read annotated Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex
    
    
    def __getValTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of examples annotated
        '''
        log.info(f'Read annotated Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex
    
    
    def __getTestTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of examples annotated
        '''
        log.info(f'Read annotated Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex