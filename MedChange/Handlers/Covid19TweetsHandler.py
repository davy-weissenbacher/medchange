'''
Created on Apr 1, 2020

@author: dweissen
'''
import pandas as pd
from pandas.core.frame import DataFrame
import logging as log
from builtins import str

log = log.getLogger('Covid19TweetsHandler')

from Handlers.HandlerInt import HandlerInt
import Properties

class Covid19TweetsHandler(HandlerInt):
    '''
    This handler read the set tweets annotated for Covid-19 virus 3 classes possible/probable/other
    Returns a dataframe containing one example per row with columns docID, text, label
    '''


    def __init__(self, properties: Properties):
        '''
        Constructor
        '''
        self.train, self.val, self.test, self.unlab = self.__splitExampleFile(properties)

    def __splitExampleFile(self, properties: Properties) -> DataFrame:
        log.info(f'Read annotated tweets @{properties.covidTweetsExamples1setPath}')
        #read the first set of examples
        ex1 = pd.read_csv(properties.covidTweetsExamples1setPath, sep='\t', dtype=str)
        ex1.drop_duplicates('Tweet_ID', inplace=True)
        ex1.rename(columns = {"Tweet_ID":"docID", "User_id":"user_id", "Tweet":"text", "Date":"created_at", "0 = Other Mention,    1 = Probable,  2 = Possible":"label", "Notes":"notes"}, inplace=True)
        ex1.set_index(keys='docID', inplace=True, verify_integrity=True)
        #read the second set of examples
        log.info(f'Read annotated tweets @{properties.covidTweetsExamples2setPath}')
        ex2 = pd.read_csv(properties.covidTweetsExamples2setPath, sep='\t', dtype=str)
        ex2.drop_duplicates('TweetID', inplace=True)
        ex2.rename(columns = {"TweetID":"docID", "UserID":"user_id", "Text":"text", "Date":"created_at", "0; 1; 2":"label", "Note":"notes"}, inplace=True)
        ex2.set_index(keys='docID', inplace=True, verify_integrity=True)
        
        #append the 2 sets
        ex = ex1.append(ex2, ignore_index=False, verify_integrity=True)
        ex.to_csv('/tmp/ex.tsv', sep='\t')
        assert len(ex)==(4800+5200), "Oups the sum do not correspond to the number of tweets expected..."
        ex = ex.astype({'label':int})
        
        lenEx = len(ex)
        test = ex.sample(frac=0.20, random_state=6)
        ex.drop(test.index, inplace=True)
        val = ex.sample(frac=0.1, random_state=6)
        ex.drop(val.index, inplace=True)
        train = ex.sample(frac=0.1, random_state=6)
        ex.drop(train.index, inplace=True)
        #the remaining are the 'unlabeled examples' used by the goldStandard oracle
        unlab = ex
        
        assert (len(train)+len(val)+len(test)+len(unlab))==lenEx, "I did not retrieve the number of examples after splitting train, val, test, unlab, check the code..."
        return [train, val, test, unlab]
    
    
    def getTrainExamples(self)->DataFrame:
        return self.train
    def getUnlabeledExamples(self)->DataFrame:
        return self.unlab
    def getValExamples(self)->DataFrame:
        return self.val
    def getTestExamples(self) -> DataFrame:
        return self.test
    
    
    def __getTrainTweets(self, pathTSV) -> DataFrame:
        '''
        :return: the set of examples annotated
        '''
        log.info(f'Read annotated Tweets @{pathTSV}')
        ex = pd.read_csv(pathTSV, sep='\t')
        ex.rename(columns = {"tweet_id":"docID"}, inplace=True)
        ex.set_index(keys='docID', inplace=True, verify_integrity=True)
        return ex
    
    