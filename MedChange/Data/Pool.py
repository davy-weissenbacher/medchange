'''
Created on Oct 7, 2019

@author: davy
'''

import logging as log
from pandas.core.series import Series
log = log.getLogger('Pool')
import pandas as pd
from pandas.core.frame import DataFrame


class Pool(object):
    '''
    A pool of examples. the pool contains unlabeled and labeled examples.
    The pool expect a column 'text' for each example and a column "docID" as dataframe index, plus a column label for the annotated examples
    '''

    def __init__(self, unlabeledExamples, trainingExamples, validationExamples, testExamples):
        '''
        :param: unlabeledExamples set of examples for which no label was provided
        :param: trainingExamples, the seed; they will be assigned with iteration 0
        :param: validationExamples
        :param: testExamples
        '''
        self.unlabEx = unlabeledExamples
        self.trainEx = trainingExamples
        # These examples are the seed examples 
        self.trainEx['iteration'] = 0
        self.valEx   = validationExamples
        self.testEx  = testExamples
        assert {'text', 'label'}.issubset(set(self.trainEx.columns)) and self.trainEx.index.name == 'docID', "An expected column is missing in the set of columns given for the training examples"
        assert {'text', 'label'}.issubset(set(self.testEx.columns)) and self.testEx.index.name == 'docID', "An expected column is missing in the set of columns given for the test examples"
        assert {'text', 'label'}.issubset(set(self.valEx.columns)) and self.valEx.index.name == 'docID', "An expected column is missing in the set of columns given for the validation examples"
        assert {'text'}.issubset(set(self.unlabEx.columns)) and self.unlabEx.index.name == 'docID', "An expected column is missing in the set of columns given for the unlabeled examples"
        assert len(self.unlabEx.index.intersection(self.trainEx.index)) == 0, f"I found {len(self.unlabEx.index.intersection(self.trainEx.index))} examples present in the training and the unlabeled sets, which should not be. Check the data"
        assert len(self.unlabEx.index.intersection(self.valEx.index)) == 0, f"I found {len(self.unlabEx.index.intersection(self.valEx.index))} examples present in the unlabeled and validation sets, which should not be. Check the data"
        assert len(self.unlabEx.index.intersection(self.testEx.index)) == 0, f"I found {len(self.unlabEx.index.intersection(self.testEx.index))} examples present in the unlabeled and test sets, which SHOULD NOT be. Check the data"
        
        assert len(self.trainEx.index.intersection(self.valEx.index)) == 0, f"I found {len(self.trainEx.index.intersection(self.valEx.index))} examples present in the training and validation sets, which should not be. Check the data"
        assert len(self.trainEx.index.intersection(self.testEx.index)) == 0, f"I found {len(self.trainEx.index.intersection(self.testEx.index))} examples present in the training and test sets, which SHOULD NOT be. Check the data"
        
        assert len(self.valEx.index.intersection(self.testEx.index)) == 0, f"I found {len(self.valEx.index.intersection(self.testEx.index))} examples present in the validation and test sets, which SHOULD NOT be. Check the data"

    
    def getLastIteration(self):
        '''
        :return: the last iteration of active learning
        '''
        return self.trainEx['iteration'].max()
    
    
    def getMostRecentTrainingExamples(self) -> DataFrame:
        '''
        :return: the most recent training examples annotated, the seed for the first iteration
        '''
        maxIteration = self.trainEx['iteration'].max()
        return self.trainEx.loc[self.trainEx['iteration']==maxIteration]
    
    
    def addQueryResult(self, examples: DataFrame, iteration: int):
        '''
        :param: examples queried by the active learner have been returned and are ready to be added in the pool
        :param: iteration recording at which iteration the examples have been annotated; 0 is the seed, 0+i are examples annotated by an oracle
        :return: add the newly labeled examples in the pool with the expected iteration number
        '''
        #the set of examples annotated by the oracle should be found in unlabex
        assert len(self.unlabEx.index.intersection(examples.index)) == len(examples), "some examples queried have not been found in the unlabel set of examples in the pool, which should not be. Check the code."
        assert len(self.trainEx.index.intersection(examples.index)) == 0, f"I found {len(self.trainEx.index.intersection(examples.index))} unlabeled examples, just annotation by the oracle already in the training set, which should not be. Check code and data"
        
        #We update their labels and their certainty with the solutions
        #TODO: assign the values in the same time...
        self.unlabEx.loc[examples.index, 'label'] = examples.loc[examples.index, 'label']
        #and we move them from the unlabeled set to the training set
        self.trainEx = pd.concat([self.trainEx, self.unlabEx.loc[examples.index]], ignore_index=False, sort=False)
        self.unlabEx.drop(examples.index, inplace=True)
        #now adding the iteration for these new examples
        self.trainEx.loc[examples.index, 'iteration'] = int(iteration)
    
        
    def getAllTexts(self) -> Series:
        '''
        :return: the texts of all examples (train, validation, test) merged in a unique column from the text column
        '''
        alltxt = [self.trainEx['text'], self.valEx['text'], self.testEx['text'], self.unlabEx['text']]
#         alldrug= [self.trainEx['drug'], self.valEx['drug'], self.testEx['drug'], self.unlabEx['drug']]
#         df = pd.DataFrame({'text': pd.concat(alltxt),'drug': pd.concat(alldrug)})
        df = pd.DataFrame({'text': pd.concat(alltxt)})
        return df
    
    
    def __str__(self):
        return (f"Pool: unlab({len(self.unlabEx)}), train({len(self.trainEx)}), val({len(self.valEx)}), test({len(self.testEx)})")
    
    