'''
Created on Oct 8, 2019

@author: davy
'''

import abc
from typing import List

from Data.Pool import Pool
from pandas.core.frame import DataFrame
from Performance.Performance import Scores
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt

class ActiveLearnerInt(object):
    '''
    The interface all active learners should implement
    '''
    def __init__(self, properties):
        self.batchSize
        
    
    @abc.abstractmethod
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of most informative examples returned to be annotated
        :return: a set of examples that should be the most informative to annotate
        '''
        pass
    
        
    @abc.abstractmethod
    def train(self, pool: Pool):
        '''
        :param: pool a pool which contains the most recent annotated examples to run the next training iteration
        :return: a model updated
        '''
        pass
    
        
    @abc.abstractmethod
    def classify(self, examples: DataFrame):
        '''
        :param: examples to classify
        :return: a copy of the examples updated with 1 columns label and n columns certainty_label_n:
          - label: the best prediction made by the learner, ie, the label with the highest certainty 
          - certainty_label_n: a column where the learner assign the probability for the label n for each example
        '''
        pass
    
    
    @abc.abstractmethod
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: the performance of the learner
        '''
        pass
    
    
    @abc.abstractmethod
    def getClassifiers(self) -> List[ClassifierInt]:
        '''
        :return the list of Classifiers used by the learner
        '''
        pass
    
    @abc.abstractmethod
    def clear(self):
        '''
        If the active learner needs to clear anything between 2 active learning iterations
        '''
        pass
