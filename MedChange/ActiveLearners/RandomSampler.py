'''
Created on Nov 11, 2019

@author: dweissen
'''

import logging as lg
log = lg.getLogger('RandomSampler')
from pandas.core.frame import DataFrame
import numpy as np
from Performance.Performance import Scores, Performance
from Visualization.PerformanceDrawer import PerformanceDrawer
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score

from Data.Pool import Pool
from ActiveLearners.ActiveLearnerInt import ActiveLearnerInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt


class RandomSampler(ActiveLearnerInt):
    '''
    A baseline active learner which select the most informative examples randomly...
    '''

    def __init__(self, classifier: ClassifierInt):
        '''
        Constructor
        '''
        self.classifier = classifier
    
        
    def getClassifiers(self) -> ClassifierInt:
        return [self.classifier]
    
    
    def train(self, pool: Pool):
        '''
        :param: a pool which contains the most recent annotated examples to run the next training iteration
        :return: a model updated
        '''
        log.info("Training the classifier...")
        self.getClassifiers()[0].train(pool)
        log.info("The classifier has finished its training...")
    
    def classify(self, examples: DataFrame):
        return self.getClassifiers()[0].classify(examples)
        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 scores
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."        

        cf = confusion_matrix(list(df['truths']), list(df['preds']))        
        cr = classification_report(list(df['truths']), list(df['preds']),digits=4)
        acc=accuracy_score(list(df['truths']), list(df['preds']))
        if df['truths'].nunique()>2 or df['preds'].nunique()>2:
            #We are in a multinomial classification
            prec = precision_score(list(df['truths']), list(df['preds']), average='macro')
            rec = recall_score(list(df['truths']), list(df['preds']), average='macro')
            f1 = f1_score(list(df['truths']), list(df['preds']), average='macro')
        else:
            #binary
            prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return Scores(confusionMatrix=cf, classificationReport=cr, F1positive=f1, PrecisionPositive=prec, RecallPositive=rec, Accuracy=acc)
    
    def drawPerfOnEvaluation(self, path:str):
        if len(self.getClassifiers())==1 and self.getClassifiers()[0].histories is None:
            log.warn("Trying to draw an history for a classifier which does not compute one, nothing done.")
        else:
            drawer = PerformanceDrawer()
            assert len(self.getClassifiers()[0].histories)>0, "Try to draw performance on the evaluation set before computing it..."
            drawer.drawHistory(self.getClassifiers()[0].histories[-1], path)
        
    def drawPerformance(self, performance: Performance, path:str):
        drawer = PerformanceDrawer()
        drawer.drawPerformance(performance, path)
                        
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of examples returned to be annotated
        :return: a set of examples randomly selected
        '''
        miExamples = pool.unlabEx.sample(frac=1.0, replace=False)
        miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])
        
        miExamples['label'] = np.NaN
        
        return miExamples
    
    def clear(self):
        #I have only one classifier nothing to do
        return
        
                
#     def drawImprovement(self):
#         '''
#         draw the improvement observed iteration after iteration of active learning
#         '''
#         df = DataFrame()
#         iterationsX = sorted(self.classifier.getPerformanceHistory().keys())
#         assert len(iterationsX)>0, "I am asked to draw an history which is empty, check the code."
#         
#         df['x'] = iterationsX 
#         f1s = []
#         for iterationx in iterationsX:
#             f1s.append(self.classifier.getPerformanceHistory()[iterationx]['f1'])
#         df[self.classifier.getName()] = f1s
#                     
#         pp = PdfPages(f'/tmp/classifiersImprovement_{str(iterationsX[-1])}.pdf')
#         
#         fig = plt.figure()    
#         plt.plot('x', self.classifier.getName(), data=df, marker='', color='blue', linewidth=2)
#         plt.legend()
#         pp.savefig(fig)        
#         pp.close()        
        
        