'''
Created on Oct 8, 2019

@author: davy
'''
from ActiveLearners.Oracles.OracleInt import OracleInt
from pandas.core.frame import DataFrame
import logging as log
log = log.getLogger('GoldStandardOracle')

class GoldStandardOracle(OracleInt):
    '''
    This oracle knows the labels from a preloaded gold standard
    '''

    def __init__(self, goldStandardExamples: DataFrame):
        '''
        :param: all labeled examples annotated since the pool is only composed of gold standard corpus
        '''
        self.goldStandard = goldStandardExamples
        
    def getQueryLabels(self, examples: DataFrame):
        """
        :param: examples, the set of unlabeled examples the learner is asking for the labels
        :return: update de examples with their labels
        """
        assert len(self.goldStandard.index.intersection(examples.index)) == len(examples), "some examples queried have not been found in the gold standard set of examples in the gold standard oracle, which should not be. Check the code."
        examples.loc[examples.index, 'label'] = self.goldStandard.loc[examples.index, 'label']