'''
Created on Dec 12, 2019

@author: dweissen
'''

from __future__ import absolute_import, division, print_function, unicode_literals

import logging as lg
log = lg.getLogger('CNNClassifier')
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Data.Pool import Pool
from Performance.Performance import Scores, Performance

from deprecated import deprecated
import os
# os.environ["MKL_THREADING_LAYER"] = "GNU"
# os.environ['KERAS_BACKEND']='theano'
import keras
from keras import backend as K
from keras.layers import *
from keras.callbacks import ModelCheckpoint, History

# import tensorflow as tf
# from tensorflow.keras import datasets, layers, models, backend 
from keras.utils.np_utils import to_categorical
import numpy as np
from pandas.core.frame import DataFrame


class CNNTF2Classifier(ClassifierInt):
    '''
    I reimplement a CNN in TF2, this may help with the memory leak problem...
    '''
    def __init__(self,  name:str, preprocessWebMD: PreprocessWebMD, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        :param: preprocessWebMD an access to the word embeddings the classifier needs
        :param: initialModel if none, the CNNClassifier will compile a new network, if a path to a h5 describing a valid model is given, the model is loaded instead 
        :return: save the model compiled in /tmp/Initial_self.getName().h5
        '''
        super(CNNTF2Classifier, self).__init__(name)
        log.info(f"A CNN classifier: {self.getName()} is used")
        
        self.batchSize = 64
        self.firstIterEpoch=10
        self.maxEpoch=10
        self._preprocessWebMD = preprocessWebMD
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'/tmp/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/tmp/Initial_{self.getName()}.h5'
            self.currentModelPath = f'/tmp/Current_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildCNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = keras.models.load_model(self.initialModelPath)
        
        else:
            self.checkPointPath = f'/home/gesy17/MNA/pickle/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/home/gesy17/MNA/pickle/Initial_{self.getName()}.h5'
            self.currentModelPath = f'/home/gesy17/MNA/pickle/Current_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildCNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = keras.models.load_model(self.initialModelPath)

        #the list of historie return after each training
        self.histories = []
    
    
    def __getCopyInitialModel(self):
        '''
        :return: a copy of the initial model given or build, just read the model from the disk
        '''
        return keras.models.load_model(self.initialModelPath)
    
    def __buildCNN(self):
        '''
        Build and compile the architecture of the CNN
        '''
        self.myadam = keras.optimizers.Adam(lr=0.001, amsgrad=True)
        self.text_input = keras.layers.Input(shape=(100,), dtype='int32') 
        self.text_embedding_layer = keras.layers.Embedding(input_dim = self._preprocessWebMD.lister.shape[0], output_dim = 400, weights = [self._preprocessWebMD.lister], trainable = True)

        self.embedded_text = self.text_embedding_layer(self.text_input)
        
        self.d_embedded_text = keras.layers.Dropout(0.2)(self.embedded_text)

        self.cnn1 = keras.layers.Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(self.d_embedded_text)
        self.d_cnn1 = keras.layers.Dropout(0.2)(self.cnn1)
        self.p = keras.layers.GlobalMaxPooling1D()(self.d_cnn1)
        # self.cp=concatenate([self.p,Flatten()(self.d_embedded_ease),Flatten()(self.d_embedded_effect),Flatten()(self.d_embedded_satis)],axis=-1)

        self.dense = keras.layers.Dense(200, activation='relu')(self.p)
        self.final = keras.layers.Dense(2, activation='softmax')(self.dense)
        self.model = keras.models.Model(self.text_input, self.final)
        # self.model = Model([self.text_input,self.ease_input,self.effect_input,self.satis_input], self.final)
        self.model.compile(loss='categorical_crossentropy', optimizer=self.myadam, metrics=['accuracy'])
        self.model.save(f'/tmp/Initial_{self.getName()}.h5')


    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        history = self.__trainCheckPoints(pool)
        #take too much memory, since the history keep the annotations
        #self.histories.append(history)
        self.histories = [history]

    def load(self):
        self.model = keras.models.load_model(self.checkPointPath)

        
    def __trainCheckPoints(self, pool: Pool) -> History:
        '''
        Train the model and outputs a check point to restart training the model between each iteration, use only the last examples annotated
        Load the best model found after the iteration 
        
        ==> There is a memory leak somewhere in Keras, loading a model do not free the memory. Adding reset default graph help but the usage of memory still increase in long term.
        ==> see if keeping all history is a good idea...        
        '''
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            # the model fit and keep the best model (based on its performance on the evaluation set)
            self.model = keras.models.load_model(self.checkPointPath)
        else:
            epochNum = self.maxEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            self.model = keras.models.load_model(self.checkPointPath)
        self.model.save(self.checkPointPath+f'_ALiteration{pool.getLastIteration()}')
        #history = []
        return history

#     def train(self, mods: list):
#         inpt = np.random.randint(low=0, high=17812, size=((2000, 100)))
#         labs = np.random.randint(low=0, high=2, size=2000)
#         cpM1 = keras.callbacks.ModelCheckpoint(filepath='/tmp/chpM1.ckpt',
#                                                  save_weights_only=False,
#                                                  verbose=1)
#         mods[0].fit(inpt, to_categorical(labs, num_classes=2), epochs=2, callbacks=[cpM1])
#         cpM2 = keras.callbacks.ModelCheckpoint(filepath='/tmp/chpM2.ckpt',
#                                                  save_weights_only=False,
#                                                  verbose=1)
#         mods[1].fit(inpt, to_categorical(labs, num_classes=2), epochs=2, callbacks=[cpM2])
#         cpM3 = keras.callbacks.ModelCheckpoint(filepath='/tmp/chpM3.ckpt',
#                                                  save_weights_only=False,
#                                                  verbose=1)
#         mods[2].fit(inpt, to_categorical(labs, num_classes=2), epochs=2, callbacks=[cpM3])    
    
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        log.info(f"CNN assigns labels and certainty to the {len(examples)} examples")
        evalExamples = examples.copy()
        evalText=evalExamples['text']
        evalIndices=self._preprocessWebMD.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalLabel=self.model.predict(evalIndices,batch_size=self.batchSize,verbose=1)
        evalExamples['certainty_label_0'] = evalLabel[:,0]
        evalExamples['certainty_label_1'] = evalLabel[:,1]        
        evalLabel = np.argmax(evalLabel, axis=1)
        evalExamples['prediction']=evalLabel

        return evalExamples    
    
    
#     def classify(self, mods: list):
#         input = np.random.randint(low=0, high=17812, size=((2000, 100)))
#         out = mods[0].predict(input)
#         print(f"model 1: {out}")        
#         out = mods[1].predict(input)
#         print(f"model 2: {out}")        
#         out = mods[2].predict(input)
#         print(f"model 3: {out}")        

if __name__ == '__main__':
    cnn1 = CNNTF2Classifier('CNN1')
    cnn2 = CNNTF2Classifier('CNN2')
    cnn3 = CNNTF2Classifier('CNN3')
    m1 = cnn1.buildCNN()
    m2 = cnn2.buildCNN()
    m3 = cnn3.buildCNN()
    cnn1.train([m1,m2,m3])
    cnn1.classify([m1,m2,m3])
    #K.clear_session() to be commented if Theano is running in backend
    K.clear_session()
    for i in range(5000):
        print(f"iteration: {i}")
        m1 = cnn1.loadCNN('CNN1')
        m2 = cnn2.loadCNN('CNN2')
        m3 = cnn3.loadCNN('CNN3')
        cnn1.train([m1,m2,m3])
        cnn1.classify([m1,m2,m3])
        #K.clear_session() to be commented if Theano is running in backend
        K.clear_session()
    print("==> Finish!")