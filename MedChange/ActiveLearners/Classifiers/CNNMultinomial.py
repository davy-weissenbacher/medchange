'''
Created on Apr 1, 2020

@author: dweissen
'''

import logging as lg
log = lg.getLogger('CNNClassifier')
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Data.Pool import Pool

import keras
from keras import backend as K
from keras.callbacks import ModelCheckpoint, History
from keras.utils.np_utils import to_categorical
import numpy as np
from pandas.core.frame import DataFrame

class CNNMultinomial(ClassifierInt):
    '''
    A classifier based on a CNN which classify the input into multiple classes
    '''

    def __init__(self,  name:str, preprocessWebMD: PreprocessWebMD, InitialModelsPath=None, numberClasses:int = 2):
        '''
        :param: name, the name of the classifier for further reference
        :param: preprocessWebMD an access to the word embeddings the classifier needs
        :param: initialModel if none, the CNNClassifier will compile a new network, if a path to a h5 describing a valid model is given, the model is loaded instead 
        :param: numberClasses number of classes for this multinomial classifier, default 2
        :return: save the model compiled in /tmp/Initial_self.getName().h5
        '''
        super(CNNMultinomial, self).__init__(name)
        log.info(f"A CNN classifier: {self.getName()} is used")
        
        self.batchSize = 64
        self.firstIterEpoch=13#3
        self.maxEpoch=13#5
        self._preprocessWebMD = preprocessWebMD
        self.nbClasses = numberClasses
        
        self.checkPointPath = f'/tmp/{self.getName()}_bestModel.h5'
        self.initialModelPath = f'/tmp/Initial_{self.getName()}.h5'
        self.currentModelPath = f'/tmp/Current_{self.getName()}.h5'
        if InitialModelsPath is None:
            self.__buildCNN()
        else:
            log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
            self.initialModelPath = InitialModelsPath
            self.model = keras.models.load_model(self.initialModelPath)

        #the list of historie return after each training
        self.histories = []
        
    def __getCopyInitialModel(self):
        '''
        :return: a copy of the initial model given or build, just read the model from the disk
        '''
        return keras.models.load_model(self.initialModelPath)
    
    def __buildCNN(self):
        '''
        Build and compile the architecture of the CNN
        '''
        self.myadam = keras.optimizers.Adam(lr=0.001, amsgrad=True)
        self.text_input = keras.layers.Input(shape=(100,), dtype='int32') 
        self.text_embedding_layer = keras.layers.Embedding(input_dim = self._preprocessWebMD.lister.shape[0], output_dim = 400, weights = [self._preprocessWebMD.lister], trainable = True)

        self.embedded_text = self.text_embedding_layer(self.text_input)
        
        self.d_embedded_text = keras.layers.Dropout(0.2)(self.embedded_text)

        self.cnn1 = keras.layers.Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(self.d_embedded_text)
        self.d_cnn1 = keras.layers.Dropout(0.2)(self.cnn1)
        self.p = keras.layers.GlobalMaxPooling1D()(self.d_cnn1)
        # self.cp=concatenate([self.p,Flatten()(self.d_embedded_ease),Flatten()(self.d_embedded_effect),Flatten()(self.d_embedded_satis)],axis=-1)

        self.dense = keras.layers.Dense(200, activation='relu')(self.p)
        self.final = keras.layers.Dense(self.nbClasses, activation='softmax')(self.dense)
        self.model = keras.models.Model(self.text_input, self.final)
        # self.model = Model([self.text_input,self.ease_input,self.effect_input,self.satis_input], self.final)
        self.model.compile(loss='categorical_crossentropy', optimizer=self.myadam, metrics=['accuracy'])
        self.model.save(f'/tmp/Initial_{self.getName()}.h5')

        
    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        history = self.__trainCheckPoints(pool)
        #take too much memory, since the history keep the annotations
        #self.histories.append(history)
        self.histories = [history]


    def load(self):
        self.model = keras.models.load_model(self.checkPointPath)

        
    def __trainCheckPoints(self, pool: Pool) -> History:
        '''
        Train the model and outputs a check point to restart training the model between each iteration, use only the last examples annotated
        Load the best model found after the iteration 
        '''
        
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=self.nbClasses), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            # the model fit and keep the best model (based on its performance on the evaluation set)
            self.model = keras.models.load_model(self.checkPointPath)
        else:
            epochNum = self.maxEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=self.nbClasses), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            self.model = keras.models.load_model(self.checkPointPath)
        #history = []
        return history

    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 1 column for label and n columns for certainty assigned to each possible values of the label:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        log.info(f"CNN assigns labels and certainty to the {len(examples)} examples")
        evalExamples = examples.copy()
        evalText=evalExamples['text']
        evalIndices=self._preprocessWebMD.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalLabel=self.model.predict(evalIndices,batch_size=self.batchSize,verbose=1)
        evalExamples['certainty_label_0'] = evalLabel[:,0]
        evalExamples['certainty_label_1'] = evalLabel[:,1]
        evalExamples['certainty_label_2'] = evalLabel[:,2]
        evalLabel = np.argmax(evalLabel, axis=1)
        evalExamples['prediction']=evalLabel

        return evalExamples    