'''
a simple multi-head self-attention classifier

In my original implement, I use text information along with discrete feature information (ease of use level, effectiveness level, satisfication level). Here, to match the "pool" dataframe, I omit all the discrete features. Certainly, they can be added to boost performances.
'''

'''
To be finished
'''

import logging as lg
log = lg.getLogger('SelfAttClassifier')
from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Textpreprocess.WebMDPreprocess import *
from Data.Pool import Pool

import tensorflow as tf
import sklearn
import keras
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import backend as K
from keras.engine.topology import Layer, InputSpec
from keras import initializers 
from keras.utils import plot_model
from sklearn.metrics import *
from keras.layers import *
from keras.optimizers import Optimizer

class AttLayer(Layer):
    def __init__(self, **kwargs):
        # self.init = initializations.get('normal')#keras1.2.2
        self.init = initializers.get('normal')

        # self.input_spec = [InputSpec(ndim=3)]
        super(AttLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3
        self.W = K.variable(self.init((input_shape[-1], 1)))
        # self.W = self.init((input_shape[-1],))
        # self.input_spec = [InputSpec(shape=input_shape)]
        self.trainable_weights = [self.W]
        super(AttLayer, self).build(input_shape)  # be sure you call this somewhere!

    def call(self, x, mask=None):
        # eij = K.tanh(K.dot(x, self.W))
        #print(x.shape)
        #print(self.W.shape)
        eij = K.tanh(K.dot(x, self.W))

        ai = K.exp(eij)
        print(ai.shape)
        # weights = ai / K.sum(ai, axis=1).dimshuffle(0, 'x')
        weights = ai / K.expand_dims(K.sum(ai, axis=1), 1)
        #print('weights', weights.shape)
        # weighted_input = x * weights.dimshuffle(0, 1, 'x')
        weighted_input = x * weights

        # return weighted_input.sum(axis=1)
        return K.sum(weighted_input, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[-1]

class Adam(Optimizer):
    def __init__(self, lr=0.001, beta_1=0.9, beta_2=0.999,
                 epsilon=None, decay=0., amsgrad=False, **kwargs):
        super(Adam, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.lr = K.variable(lr, name='lr')
            self.beta_1 = K.variable(beta_1, name='beta_1')
            self.beta_2 = K.variable(beta_2, name='beta_2')
            self.decay = K.variable(decay, name='decay')
        if epsilon is None:
            epsilon = K.epsilon()
        self.epsilon = epsilon
        self.initial_decay = decay
        self.amsgrad = amsgrad

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        if self.initial_decay > 0:
            lr = lr * (1. / (1. + self.decay * K.cast(self.iterations,
                                                      K.dtype(self.decay))))

        t = K.cast(self.iterations, K.floatx()) + 1
        lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) /
                     (1. - K.pow(self.beta_1, t)))

        ms = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        vs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        if self.amsgrad:
            vhats = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        else:
            vhats = [K.zeros(1) for _ in params]
        self.weights = [self.iterations] + ms + vs + vhats

        for p, g, m, v, vhat in zip(params, grads, ms, vs, vhats):
            m_t = (self.beta_1 * m) + (1. - self.beta_1) * g
            v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square(g)
            if self.amsgrad:
                vhat_t = K.maximum(vhat, v_t)
                p_t = p - lr_t * m_t / (K.sqrt(vhat_t) + self.epsilon)
                self.updates.append(K.update(vhat, vhat_t))
            else:
                p_t = p - lr_t * m_t / (K.sqrt(v_t) + self.epsilon)

            self.updates.append(K.update(m, m_t))
            self.updates.append(K.update(v, v_t))
            new_p = p_t

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, new_p))
        return self.updates

class Attention(Layer):
    def __init__(self, nb_head, size_per_head, **kwargs):
        self.nb_head = nb_head
        self.size_per_head = size_per_head
        self.output_dim = nb_head*size_per_head
        super(Attention, self).__init__(**kwargs)
    def build(self, input_shape):
        self.WQ = self.add_weight(name='WQ', 
                                  shape=(input_shape[0][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        self.WK = self.add_weight(name='WK', 
                                  shape=(input_shape[1][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        self.WV = self.add_weight(name='WV', 
                                  shape=(input_shape[2][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        super(Attention, self).build(input_shape)
    def Mask(self, inputs, seq_len, mode='mul'):
        if seq_len == None:
            return inputs
        else:
            mask = K.one_hot(seq_len[:,0], K.shape(inputs)[1])
            mask = 1 - K.cumsum(mask, 1)
            for _ in range(len(inputs.shape)-2):
                mask = K.expand_dims(mask, 2)
            if mode == 'mul':
                return inputs * mask
            if mode == 'add':
                return inputs - (1 - mask) * 1e12        
    def call(self, x):
        if len(x) == 3:
            Q_seq,K_seq,V_seq = x
            Q_len,V_len = None,None
        elif len(x) == 5:
            Q_seq,K_seq,V_seq,Q_len,V_len = x
        Q_seq = K.dot(Q_seq, self.WQ)
        Q_seq = K.reshape(Q_seq, (-1, K.shape(Q_seq)[1], self.nb_head, self.size_per_head))
        Q_seq = K.permute_dimensions(Q_seq, (0,2,1,3))
        K_seq = K.dot(K_seq, self.WK)
        K_seq = K.reshape(K_seq, (-1, K.shape(K_seq)[1], self.nb_head, self.size_per_head))
        K_seq = K.permute_dimensions(K_seq, (0,2,1,3))
        V_seq = K.dot(V_seq, self.WV)
        V_seq = K.reshape(V_seq, (-1, K.shape(V_seq)[1], self.nb_head, self.size_per_head))
        V_seq = K.permute_dimensions(V_seq, (0,2,1,3))
        A = K.batch_dot(Q_seq, K_seq, axes=[3,3]) / self.size_per_head**0.5
        A = K.permute_dimensions(A, (0,3,2,1))
        A = self.Mask(A, V_len, 'add')
        A = K.permute_dimensions(A, (0,3,2,1))    
        A = K.softmax(A)
        O_seq = K.batch_dot(A, V_seq, axes=[3,2])
        O_seq = K.permute_dimensions(O_seq, (0,2,1,3))
        O_seq = K.reshape(O_seq, (-1, K.shape(O_seq)[1], self.output_dim))
        O_seq = self.Mask(O_seq, Q_len, 'mul')
        return O_seq
    def compute_output_shape(self, input_shape):
        return (input_shape[0][0], input_shape[0][1], self.output_dim)

class SelfAttClassifier(ClassifierInt):

    def __init__(self, name:str,preprocessWebMD):
        '''
        :param: name, the name of the classifier for further reference
        '''
        super(SelfAttClassifier, self).__init__(name)
        log.info(f"A multi-head self attention classifier: {self.getName()} is used")
        #A map to record the performance of the classifier after each training iteration
        #iteration -> {precision,recall,f1}
        self.performance = {}

        self.myadam = Adam(lr=0.001,amsgrad=True)
        self.batchSize = 128
        self.maxEpoch=10
        
        self.text_input = Input(shape=(100,), dtype='int32')
        # self.ease_input= Input(shape=(1,), dtype='int32')
        # self.effect_input= Input(shape=(1,), dtype='int32')
        # self.satis_input=Input(shape=(1,), dtype='int32')

        self.text_embedding_layer = Embedding(preprocessWebMD.lister.shape[0],300,weights=[preprocessWebMD.lister], trainable=True)
        # self.ease_embedding_layer = Embedding(5,5,input_length=1,trainable=True)
        # self.effect_embedding_layer = Embedding(5,5,input_length=1,trainable=True)
        # self.satis_embedding_layer = Embedding(2,5,input_length=1,trainable=True)

        self.embedded_text = self.text_embedding_layer(self.text_input)
        # self.embedded_ease = self.ease_embedding_layer(self.ease_input)
        # self.embedded_effect = self.effect_embedding_layer(self.effect_input)
        # self.embedded_satis = self.satis_embedding_layer(self.satis_input)
        
        self.d_embedded_text=Dropout(0.2)(self.embedded_text)
        # self.d_embedded_ease= Dropout(0.2)(self.embedded_ease)
        # self.d_embedded_effect= Dropout(0.2)(self.embedded_effect)
        # self.d_embedded_satis= Dropout(0.2)(self.embedded_satis)

        self.att_self=Attention(16,16)([self.d_embedded_text,self.d_embedded_text,self.d_embedded_text])
        self.d_att_self=Dropout(0.2)(self.att_self)
        self.att_out=AttLayer()(self.d_att_self)
        self.d_att_out= Dropout(0.2)(self.att_out)
        # self.cp=concatenate([self.p,Flatten()(self.d_embedded_ease),Flatten()(self.d_embedded_effect),Flatten()(self.d_embedded_satis)],axis=-1)

        self.dense= Dense(50, activation='relu')(self.d_att_out)   
        self.final= Dense(2, activation='softmax')(self.dense)
        self.model = Model(self.text_input, self.final)
        # self.model = Model([self.text_input,self.ease_input,self.effect_input,self.satis_input], self.final)
        self.model.compile(loss='categorical_crossentropy', optimizer=self.myadam, metrics=['acc'])

    def train(self, pool: Pool, preprocessWebMD, evaluate: bool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training SelfAtt.")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        history=self.model.fit(trainIndices,to_categorical(trainLabel),batch_size=self.batchSize,epochs=self.maxEpoch,shuffle=True,verbose=1)
        if evaluate:
            log.info("I am evaluating my performance on the evaluation set after my training.")
            valExPredicted = self.classify(pool.valEx)
            perf = self.evaluate(valExPredicted, pool.valEx)
            self.performance[pool.getLastIteration()] = perf
    
    def classify(self, preprocessWebMD, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty: a dictionary where each possible label is assigned a probability
        '''
        log.info(f"SelfAtt assigns labels and certainty to the {len(examples)} examples")
        eexamples = examples.copy()
        evalText=eexamples['text']
        evalIndices=preprocessWebMD.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalLabel=self.model.predict(evalIndices,batch_size=self.batchSize,verbose=1)
        evalLabel = np.argmax(evalLabel, axis=1)
        eexamples['prediction']=evalLabel
        
        # rand0 = np.random.rand(1,len(examples))[0]
        # examples['certainty_label_0'] = rand0
        # examples['certainty_label_1'] = 1-rand0
        
        return eexamples
        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> dict:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 score
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'label' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        log.fatal("Triple check the evaluation here.")
        return self.__getScores__(examplesTruth, examplesPredicted)
        
    def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        '''
        :param: examplesTruth a dataframe with the docIDs and label  
        :param: examplesPredicted a dataframe with the docIDs and label
        :return: list with the Precision, Recall, F1 scores computed
        '''        
        #df = DataFrame({'docIDTruth':examplesTruth['docID'], 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted['docID'], 'preds':examplesPredicted['label']})
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['label']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
        df.to_csv("/tmp/SelfAttClassifierEvaluationOutput.tsv", sep='\t')
        print(confusion_matrix(list(df['truths']), list(df['preds'])))
        #precision, recall, f1, support
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return {'precision':prec, 'recall':rec, 'f1':f1}
    
    
    @deprecated(version='0.0', reason="Too slow to iterrows, use __getScores__")
    def __getScoresByIterrows__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        #now I can compute the scores, I'll go by rows to be sure to do what I want to do...
        docIDsPred = []
        docIDsTruth = []
        preds = []
        truths = []
        
        for _, row in examplesTruth.iterrows():
            docIDsTruth.append(row['docID'])
            truths.append(row['label'])
            predRow = examplesPredicted.loc[examplesPredicted['docID']==row['docID'], ['docID','label']]
            assert len(predRow)==1, f"I retrieve multliple line in the prediction set for the docID {row['docID']}, whereas this should be unique, check the data."
            docIDsPred.append(predRow['docID'].values[0])
            preds.append(predRow['label'].values[0])
        df = DataFrame({'docIDsTruth':docIDsTruth, 'truths':truths, 'docIDsPred':docIDsPred, 'preds':preds})
        df.to_csv("/tmp/RandomClassifierEvaluationOutput.tsv", sep='\t', index=False)
        print(confusion_matrix(list(df['truths']), list(df['preds'])))
        #precision, recall, f1, support
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return {'precision':prec, 'recall':rec, 'f1':f1}

    def getPerformanceHistory(self) -> dict:
        '''
        :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
        '''
        return self.performance    
        
        