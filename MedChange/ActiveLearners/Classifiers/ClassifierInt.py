'''
Created on Oct 8, 2019

@author: davy
'''

import abc
from typing import List

from Data.Pool import Pool
from pandas.core.frame import DataFrame
from keras.callbacks import History
from Performance.Performance import Scores

class ClassifierInt(object):
    '''
    Define the basic services for a classifier
    '''
    def __init__(self, name:str):
        """
        :param: name, the name of the classifier for further reference
        """
        self.name = name
        
    def getName(self) -> str:
        """
        :return: the name of the classifier
        """
        return self.name

    @abc.abstractmethod
    def train(self, pool: Pool) -> History:
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :return: history if it was computed, none otherwise
        :return: a model updated
        '''
        pass
    
    @abc.abstractmethod
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: examples to classify
        :return: a copy of the examples updated with 1 columns label and n columns certainty_label_n:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        pass
        
    @abc.abstractmethod
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: the performance of the system
        '''
        pass
    
    