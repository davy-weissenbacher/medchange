'''
From your comment, it seems that some problems exist in the ELMo classifier implemented via flair.
For convenience, I implement another version with the help of allen-nlp. The model is under tensorflow-hub.
If you want to use it, please install allennlp in your environment first.
'''
from __future__ import absolute_import, division, print_function, unicode_literals
import logging as lg
log = lg.getLogger('ELMoClassifier')
from pandas.core.frame import DataFrame
import numpy as np
from deprecated import deprecated

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Data.Pool import Pool
from Performance.Performance import Scores, Performance

import random
import os
# for gpu training
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import torch
from allennlp.commands.elmo import ElmoEmbedder
import tensorflow as tf
import pandas as pd
import keras
from keras import backend as K
from keras.models import Model, load_model
from keras.engine import Layer
from keras.utils.np_utils import *
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.layers import *
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.optimizers import *

class AttLayer(Layer):
    def __init__(self, **kwargs):
        # self.init = initializations.get('normal')#keras1.2.2
        self.init = initializers.get('normal')

        # self.input_spec = [InputSpec(ndim=3)]
        super(AttLayer, self).__init__(**kwargs)


    def build(self, input_shape):
        assert len(input_shape) == 3
        self.W = K.variable(self.init((input_shape[-1], 1)))
        # self.W = self.init((input_shape[-1],))
        # self.input_spec = [InputSpec(shape=input_shape)]
        self.trainable_weights = [self.W]
        super(AttLayer, self).build(input_shape)  # be sure you call this somewhere!


    def call(self, x, mask=None):
        # eij = K.tanh(K.dot(x, self.W))
        #print(x.shape)
        #print(self.W.shape)
        eij = K.tanh(K.dot(x, self.W))

        ai = K.exp(eij)
        print(ai.shape)
        # weights = ai / K.sum(ai, axis=1).dimshuffle(0, 'x')
        weights = ai / K.expand_dims(K.sum(ai, axis=1), 1)
        #print('weights', weights.shape)
        # weighted_input = x * weights.dimshuffle(0, 1, 'x')
        weighted_input = x * weights

        # return weighted_input.sum(axis=1)
        return K.sum(weighted_input, axis=1)


    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[-1]


class ELMoClassifier(ClassifierInt):

    def __init__(self, name:str, preprocessWebMD: PreprocessWebMD, InitialModelsPath=None):
        super(ELMoClassifier, self).__init__(name)
        log.info(f"A ELMo classifier: {self.getName()} is used")
        
        assert torch.cuda.is_available(), "Cuda is not correctly set up the code will run very slowly..."
        #device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        
        self.elmo = ElmoEmbedder(cuda_device=0)
        self.batchSize = 64
        self.firstIterEpoch=10
        self.maxEpoch=10
        self._preprocessWebMD = preprocessWebMD
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'/tmp/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/tmp/Initial_{self.getName()}.h5'
            self.currentModelPath = f'/tmp/Current_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildELMo()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = keras.models.load_model(self.initialModelPath)
        
        else:
            self.checkPointPath = f'/home/gesy17/MNA/pickle/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/home/gesy17/MNA/pickle/Initial_{self.getName()}.h5'
            self.currentModelPath = f'/home/gesy17/MNA/pickle/Current_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildELMo()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = keras.models.load_model(self.initialModelPath)

        self.histories = []

    
    def __getCopyInitialModel(self):
        return keras.models.load_model(self.initialModelPath)


    def __buildELMo(self):
        self.myadam = keras.optimizers.Adam(lr=0.001, amsgrad=True)
        self.text_input = Input(shape=(100,), dtype='int32') 
        self.elmo_input = Input(shape=(100,1024), dtype='float32') 
        self.elmo_vec = TimeDistributed(Dense(400,bias=True))(self.elmo_input)
        self.text_embedding_layer = Embedding(input_dim = self._preprocessWebMD.lister.shape[0], output_dim = 400, weights = [self._preprocessWebMD.lister], trainable = True)
        self.embedded_text = self.text_embedding_layer(self.text_input)
        self.d_embedded_text = Dropout(0.2)(self.embedded_text)
        self.combined_vector=concatenate([self.d_embedded_text,self.elmo_vec])
        self.cnn1 = Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(self.combined_vector)
        self.d_cnn1 = Dropout(0.2)(self.cnn1)
        self.p = GlobalMaxPooling1D()(self.d_cnn1)
        self.dense = keras.layers.Dense(200, activation='relu')(self.p)
        self.final = keras.layers.Dense(2, activation='softmax')(self.dense)
        self.model = keras.models.Model([self.text_input,self.elmo_input], self.final)
        self.model.compile(loss='categorical_crossentropy', optimizer=self.myadam, metrics=['accuracy'])
        self.model.save(self.initialModelPath)

#     @deprecated(version='0.0', reasom='Try to vectorize the function but not much improvement in speed maybe withe the original df...')
#     def __generate_elmo_embedding(self,texts):
#         output_convs=[]
#         df = DataFrame({'sent':texts})
#         elmo = ElmoEmbedder(cuda_device=0)
#         #elmo = ElmoEmbedder()
#         def applyMyElmoEmbedder(count, total, sent, output_convs):
#             output_sentence=[]
#             if (total==count) or ((count[0]%50)==0):
#                 log.debug(f"Embedding the sentence n'{count[0]}/{len(texts)}: {sent['sent']}")
#             count[0] = count[0]+1
#             elmo_sent=elmo.embed_sentences([sent['sent']])
#             for elmo_embedding in elmo_sent: 
#                 if len(sent)==0:
#                     avg_elmo_embedding=np.zeros([1,1024])
#                 else:
#                     avg_elmo_embedding = np.average(elmo_embedding, axis=0)
#                 output_sentence.append(avg_elmo_embedding)
#             elmo_rep=np.concatenate([output_sentence[0],np.zeros([max(100-len(output_sentence[0]),0),1024])])
#             output_convs.append(elmo_rep[:100])
#         count = [1]
#         df['elmoEmb'] = df.apply(lambda sent: applyMyElmoEmbedder(count, len(texts), sent, output_convs), axis=1)
#  
#         output_convs=np.array(output_convs)
#         return output_convs


    def __generate_elmo_embedding(self,texts):
        output_convs=[]
        output_sentence=[]
        
        count = 1
        for sent in texts:
            if (len(texts)==count) or ((count%50)==0):
                log.debug(f"Embedding the sentence n'{count}/{len(texts)}")
            count = count+1
            elmo_sent=self.elmo.embed_sentences([sent])
            for elmo_embedding in elmo_sent: 
                if len(sent)==0:
                    avg_elmo_embedding=np.zeros([1,1024])
                else:
                    avg_elmo_embedding = np.average(elmo_embedding, axis=0)
                output_sentence.append(avg_elmo_embedding)
            elmo_rep=np.concatenate([output_sentence[0],np.zeros([max(100-len(output_sentence[0]),0),1024])])
            output_convs.append(elmo_rep[:100])
            output_sentence=[]
        output_convs=np.array(output_convs)
        return output_convs


    def train(self, pool: Pool):
        history = self.__trainCheckPoints(pool)
        self.histories = [history]

    
    def load(self):
        self.model = keras.models.load_model(self.checkPointPath)

      
    def __trainCheckPoints(self, pool: Pool) -> History:
        trainPool=pool.getMostRecentTrainingExamples()
        
        log.info(f"I received {len(trainPool)} examples for training ELMo. Use __trainCheckPoints")
        trainText= list(trainPool['text'])
        trainLabel=trainPool['label']
                
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainTokenized=self._preprocessWebMD.tokenize(trainText)
        trainELMo=self.__generate_elmo_embedding(trainTokenized)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = list(pool.valEx['text'])
        valLabels = pool.valEx['label']
                
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valTokenized=self._preprocessWebMD.tokenize(valTexts)
        valELMo=self.__generate_elmo_embedding(valTokenized)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit([trainIndices,trainELMo], to_categorical(trainLabel, num_classes=2), validation_data = ([valIndices,valELMo], to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            # the model fit and keep the best model (based on its performance on the evaluation set)
            self.model = keras.models.load_model(self.checkPointPath)
        else:
            epochNum = self.maxEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit([trainIndices,trainELMo], to_categorical(trainLabel, num_classes=2), validation_data = ([valIndices,valELMo], to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            self.model = keras.models.load_model(self.checkPointPath)
        #history = []
        return history


    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''        
        log.info(f"ELMo assigns labels and certainty to the {len(examples)} examples")
        evalExamples = examples.copy()
        evalText=list(evalExamples['text'])
        
        evalIndices=self._preprocessWebMD.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalTokenized=self._preprocessWebMD.tokenize(evalText)
        evalELMo=self.__generate_elmo_embedding(evalTokenized)
        evalLabel=self.model.predict([evalIndices,evalELMo],batch_size=self.batchSize,verbose=1)
        evalExamples['certainty_label_0'] = evalLabel[:,0]
        evalExamples['certainty_label_1'] = evalLabel[:,1]        
        evalLabel = np.argmax(evalLabel, axis=1)
        evalExamples['prediction']=evalLabel

        return evalExamples    
