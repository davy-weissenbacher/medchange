#%tensorflow_version 2.x

import logging as lg
log = lg.getLogger('BertClassifier')

from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
#from Textpreprocess.WebMDPreprocess import *
from Data.Pool import Pool

import tensorflow as tf
import tensorflow_hub as hub
import bert
from tensorflow.keras.models import Model, load_model
from tqdm import tqdm
import numpy as np
import pandas as pd
import os
from collections import namedtuple
print("TensorFlow Version:",tf.__version__)
print("Hub version: ",hub.__version__)


# import random
import keras
# from keras import backend as K
#from keras.models import Model, load_model
# from keras.engine import Layer
# from keras.utils.np_utils import *
# from keras.preprocessing.text import Tokenizer, text_to_word_sequence
# from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
# from keras.layers import *
from keras.callbacks import History
# from keras.engine.topology import Layer
# from sklearn.metrics import accuracy_score, classification_report
# from keras.optimizers import *



class BertClassifier(ClassifierInt):
    """
    IMPOSSIBLE to make it works for now, everything is classified in one class when BERT is fine tuned, work normally when it's frozen...
    I have another implementation in BertTF2Classifier using another layer for calling BERT can be trained but still sensitive to imbalance data
    """

    def __init__(self, name:str, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        '''
        super(BertClassifier, self).__init__(name)
        log.info(f"A Bert classifier: {self.getName()} is used")
        #A map to record the performance of the classifier after each training iteration
        #iteration -> {precision,recall,f1}
        self.performance = {}

        self.batchSize = 32
        self.firstIterEpoch=1
        self.maxEpoch=1
        self.MAX_SEQ_LEN=128
        
        self.bert_layer=hub.KerasLayer("https://tfhub.dev/tensorflow/bert_en_uncased_L-12_H-768_A-12/1",trainable=True)        
        
        FullTokenizer=bert.bert_tokenization.FullTokenizer
        vocab_file=self.bert_layer.resolved_object.vocab_file.asset_path.numpy()
        do_lower_case=self.bert_layer.resolved_object.do_lower_case.numpy()
        self.tokenizer=FullTokenizer(vocab_file,do_lower_case)
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'/tmp/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/tmp/Initial_{self.getName()}.h5'
            self.currentModelPath = f'/tmp/Current_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = keras.models.load_model(self.initialModelPath)
        
        #the list of historie return after each training
        self.histories = []
        
    
    def __buildNN(self):
        '''
        Build and compile the architecture of the NN
        '''
        self.input_word_ids = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="input_word_ids")
        self.input_mask = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="input_mask")
        self.segment_ids = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="segment_ids")
        
        self.pooled_output, self.sequence_output = self.bert_layer([self.input_word_ids, self.input_mask, self.segment_ids])
        self.x = tf.keras.layers.Dense(units=768, activation='relu')(self.pooled_output)
        self.x = tf.keras.layers.Dropout(0.1)(self.x)
        self.out = tf.keras.layers.Dense(units=2, activation="softmax")(self.x)
        
        self.model = tf.keras.models.Model(inputs=[self.input_word_ids, self.input_mask, self.segment_ids], outputs=self.out)
        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        self.model.save(f'/tmp/Initial_{self.getName()}.h5')
        
        
#         self.input_word_ids = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="input_word_ids")
#         self.input_mask = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="input_mask")
#         self.segment_ids = tf.keras.layers.Input(shape=(self.MAX_SEQ_LEN,), dtype=tf.int32, name="segment_ids")
#         self.pooled_output, self.sequence_output = self.bert_layer([self.input_word_ids, self.input_mask, self.segment_ids])
#         self.x = tf.keras.layers.GlobalAveragePooling1D()(self.sequence_output)
#         self.y = tf.keras.layers.Dropout(0.2)(self.x)
#         self.out = tf.keras.layers.Dense(2, activation="softmax", name="dense_output")(self.y)
#         self.model = tf.keras.models.Model(inputs=[self.input_word_ids, self.input_mask, self.segment_ids], outputs=self.out)
#         self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
#         self.model.save(f'/tmp/Initial_{self.getName()}.h5')
#         #model.save may not work in that case save as json and load weights 
#         #import json
#         #json.dump(self.model.to_json(), open("/tmp/model.json", "w"))
#         #self.model = tf.keras.models.model_from_json(json.load(open("/tmp/model.json")), custom_objects={'KerasLayer':hub.KerasLayer})

    def __generate_sentence_index(self, texts):
        """
        Encode the text given for BERT 
        """
        def get_masks(tokens, max_seq_length):
            return [1]*len(tokens) + [0] * (max_seq_length - len(tokens))
        def get_segments(tokens, max_seq_length):
            """Segments: 0 for the first sequence, 1 for the second"""
            segments = []
            current_segment_id = 0
            for token in tokens:
                segments.append(current_segment_id)
                if token == "[SEP]":
                    current_segment_id = 1
            return segments + [0] * (max_seq_length - len(tokens))
        def get_ids(tokens, tokenizer, max_seq_length):
            """Token ids from Tokenizer vocab"""
            token_ids = tokenizer.convert_tokens_to_ids(tokens,)
            input_ids = token_ids + [0] * (max_seq_length-len(token_ids))
            return input_ids
        def create_single_input(sentence,MAX_LEN):
            """"""
            stokens = self.tokenizer.tokenize(sentence)
            stokens = stokens[:MAX_LEN]
            stokens = ["[CLS]"] + stokens + ["[SEP]"]
            ids = get_ids(stokens, self.tokenizer, self.MAX_SEQ_LEN)
            masks = get_masks(stokens, self.MAX_SEQ_LEN)
            segments = get_segments(stokens, self.MAX_SEQ_LEN)
 
            return ids,masks,segments
        
        input_ids, input_masks, input_segments = [], [], []
        for txt in tqdm(texts,position=0, leave=True):
            if pd.isna(txt):
                log.error("I found a text which was empty and transformed into nan by pandas, check data. I just retransform into an empty string")
                txt = ""
            ids,masks,segments=create_single_input(txt,self.MAX_SEQ_LEN-2)
            input_ids.append(ids)
            input_masks.append(masks)
            input_segments.append(segments)
 
        return [np.asarray(input_ids, dtype=np.int32), np.asarray(input_masks, dtype=np.int32), np.asarray(input_segments, dtype=np.int32)]


    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        
        log.critical("A finir correctly!")
        
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")
        
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self.__generate_sentence_index(trainText)
        trainLabel=np.array(trainLabel)
        assert len(trainLabel)==len(trainIndices[0]), f"number of training examples {len(trainIndices[0])} and labels {len(trainLabel)} are not equal, check the preprocessing in Bert."
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self.__generate_sentence_index(valTexts)
        valLabels=np.array(valLabels)
        assert len(valLabels)==len(valIndices[0]), f"number of validation examples {len(valIndices[0])} and labels {len(valLabels)} are not equal, check the preprocessing in Bert."
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint])
            # the model fit and keep the best model (based on its performance on the evaluation set)
            log.critical("A REMETTRE")
            #self.model.load_weights(self.checkPointPath)
        else:
            epochNum = self.maxEpoch
            checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=True, verbose=1, callbacks = [checkpoint])
            self.model.load_weights(self.checkPointPath)
        self.model.save(self.checkPointPath+f'_ALiteration{pool.getLastIteration()}')

        self.histories = [history]
        
#         checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
#         epochNum = self.firstIterEpoch
#         history = self.model.fit(trainIndices,to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels, num_classes=2)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
#         self.model.load_weights(self.checkPointPath)
#         #self.model = tf.keras.models.load_model(self.checkPointPath, custom_objects={'KerasLayer':hub.KerasLayer})
#         return history
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty: a dictionary where each possible label is assigned a probability
        '''
        log.info(f"BertClassifier assigns labels and certainty to the {len(examples)} examples")
        predExamples = examples.copy()
        predText=predExamples['text']
        predIndices=self.__generate_sentence_index(predText)
        predLabel=self.model.predict(predIndices, batch_size=self.batchSize, verbose=1)
        predExamples['certainty_label_0'] = predLabel[:,0]
        predExamples['certainty_label_1'] = predLabel[:,1]        
        predLabel = np.argmax(predLabel, axis=1)
        predExamples['prediction']=predLabel

        return predExamples
        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> dict:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 score
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        log.fatal("Triple check the evaluation here.")
        return self.__getScores__(examplesTruth, examplesPredicted)
        
    def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        '''
        :param: examplesTruth a dataframe with the docIDs and label  
        :param: examplesPredicted a dataframe with the docIDs and prediction
        :return: list with the Precision, Recall, F1 scores computed
        '''        
        #df = DataFrame({'docIDTruth':examplesTruth['docID'], 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted['docID'], 'preds':examplesPredicted['label']})
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
        df.to_csv("/tmp/RandomClassifierEvaluationOutput.tsv", sep='\t')
        print(confusion_matrix(list(df['truths']), list(df['preds'])))
        #precision, recall, f1, support
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return {'precision':prec, 'recall':rec, 'f1':f1}
    
    
    @deprecated(version='0.0', reason="Too slow to iterrows, use __getScores__")
    def __getScoresByIterrows__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        #now I can compute the scores, I'll go by rows to be sure to do what I want to do...
        docIDsPred = []
        docIDsTruth = []
        preds = []
        truths = []
        
        for _, row in examplesTruth.iterrows():
            docIDsTruth.append(row['docID'])
            truths.append(row['label'])
            predRow = examplesPredicted.loc[examplesPredicted['docID']==row['docID'], ['docID','label']]
            assert len(predRow)==1, f"I retrieve multliple line in the prediction set for the docID {row['docID']}, whereas this should be unique, check the data."
            docIDsPred.append(predRow['docID'].values[0])
            preds.append(predRow['label'].values[0])
        df = DataFrame({'docIDsTruth':docIDsTruth, 'truths':truths, 'docIDsPred':docIDsPred, 'preds':preds})
        df.to_csv("/tmp/CNNClassifierEvaluationOutput.tsv", sep='\t', index=False)
        print(confusion_matrix(list(df['truths']), list(df['preds'])))
        #precision, recall, f1, support
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return {'precision':prec, 'recall':rec, 'f1':f1}

    def getPerformanceHistory(self) -> dict:
        '''
        :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
        '''
        return self.performance    
        
        