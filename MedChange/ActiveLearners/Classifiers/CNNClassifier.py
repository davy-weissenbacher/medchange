'''
a simple CNN classifier

In my original implement, I use text information along with discrete feature information (ease of use level, effectiveness level, satisfying level). Here, to match the "pool" dataframe, I omit all the discrete features. Certainly, they can be added to boost performances.
'''

'''
To be finished
'''

import logging as lg
log = lg.getLogger('CNNClassifier')
from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated
from datetime import datetime
from multiprocessing import Process

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Data.Pool import Pool
from Performance.Performance import Scores, Performance

import tensorflow as tf
import sklearn
import keras
import os
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras import backend as K
from keras.engine.topology import Layer, InputSpec
from keras import initializers
from keras.legacy import interfaces
from keras.utils import plot_model
from sklearn.metrics import *
from keras.layers import *
from keras.optimizers import Optimizer

class AttLayer(Layer):
    def __init__(self, **kwargs):
        # self.init = initializations.get('normal')#keras1.2.2
        self.init = initializers.get('normal')

        # self.input_spec = [InputSpec(ndim=3)]
        super(AttLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3
        self.W = K.variable(self.init((input_shape[-1], 1)))
        # self.W = self.init((input_shape[-1],))
        # self.input_spec = [InputSpec(shape=input_shape)]
        self.trainable_weights = [self.W]
        super(AttLayer, self).build(input_shape)  # be sure you call this somewhere!

    def call(self, x, mask=None):
        # eij = K.tanh(K.dot(x, self.W))
        #print(x.shape)
        #print(self.W.shape)
        eij = K.tanh(K.dot(x, self.W))

        ai = K.exp(eij)
        print(ai.shape)
        # weights = ai / K.sum(ai, axis=1).dimshuffle(0, 'x')
        weights = ai / K.expand_dims(K.sum(ai, axis=1), 1)
        #print('weights', weights.shape)
        # weighted_input = x * weights.dimshuffle(0, 1, 'x')
        weighted_input = x * weights

        # return weighted_input.sum(axis=1)
        return K.sum(weighted_input, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[-1]


class Adam(Optimizer):
    def __init__(self, lr=0.001, beta_1=0.9, beta_2=0.999,
                 epsilon=None, decay=0., amsgrad=False, **kwargs):
        super(Adam, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.lr = K.variable(lr, name='lr')
            self.beta_1 = K.variable(beta_1, name='beta_1')
            self.beta_2 = K.variable(beta_2, name='beta_2')
            self.decay = K.variable(decay, name='decay')
        if epsilon is None:
            epsilon = K.epsilon()
        self.epsilon = epsilon
        self.initial_decay = decay
        self.amsgrad = amsgrad

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        if self.initial_decay > 0:
            lr = lr * (1. / (1. + self.decay * K.cast(self.iterations,
                                                      K.dtype(self.decay))))

        t = K.cast(self.iterations, K.floatx()) + 1
        lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) /
                     (1. - K.pow(self.beta_1, t)))

        ms = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        vs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        if self.amsgrad:
            vhats = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        else:
            vhats = [K.zeros(1) for _ in params]
        self.weights = [self.iterations] + ms + vs + vhats

        for p, g, m, v, vhat in zip(params, grads, ms, vs, vhats):
            m_t = (self.beta_1 * m) + (1. - self.beta_1) * g
            v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square(g)
            if self.amsgrad:
                vhat_t = K.maximum(vhat, v_t)
                p_t = p - lr_t * m_t / (K.sqrt(vhat_t) + self.epsilon)
                self.updates.append(K.update(vhat, vhat_t))
            else:
                p_t = p - lr_t * m_t / (K.sqrt(v_t) + self.epsilon)

            self.updates.append(K.update(m, m_t))
            self.updates.append(K.update(v, v_t))
            new_p = p_t

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, new_p))
        return self.updates


class CNNClassifier(ClassifierInt):

    def __init__(self, name:str, preprocessWebMD: PreprocessWebMD, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        :param: preprocessWebMD an access to the word embeddings the classifier needs
        :param: initialModel if none, the CNNClassifier will compile a new network, if a path to a h5 describing a valid model is given, the model is loaded instead 
        :return: save the model compiled in /tmp/Initial_self.getName().h5
        '''
        super(CNNClassifier, self).__init__(name)
        log.info(f"A CNN classifier: {self.getName()} is used")
        
        self.batchSize = 64
        self.firstIterEpoch=20#3
        self.maxEpoch=20#5
        self._preprocessWebMD = preprocessWebMD
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'/tmp/{self.getName()}_bestModel.h5'
            self.initialModelPath = f'/tmp/Initial_{self.getName()}.h5'
            if InitialModelsPath is None:
                self.__buildCNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = load_model(self.initialModelPath)
        
        #the list of historie return after each training
        self.histories = []


    def __getCopyInitialModel(self):
        '''
        :return: a copy of the initial model given or build, just read the model from the disk
        '''
        return load_model(self.initialModelPath)


    def __buildCNN(self):
        '''
        Build and compile the architecture of the CNN
        '''
        #it crashes with my version of Keras-tensorflow, I'll change for the optimizer integrated in keras
        #self.myadam = Adam(lr=0.001,amsgrad=True)
        self.myadam = keras.optimizers.Adam(lr=0.001, amsgrad=True)

        self.text_input = Input(shape=(100,), dtype='int32') 
        # self.ease_input= Input(shape=(1,), dtype='int32')
        # self.effect_input= Input(shape=(1,), dtype='int32')
        # self.satis_input=Input(shape=(1,), dtype='int32')

        self.text_embedding_layer = Embedding(input_dim = self._preprocessWebMD.lister.shape[0], output_dim = 400, weights = [self._preprocessWebMD.lister], trainable = True)
        # self.ease_embedding_layer = Embedding(5,5,input_length=1,trainable=True)
        # self.effect_embedding_layer = Embedding(5,5,input_length=1,trainable=True)
        # self.satis_embedding_layer = Embedding(2,5,input_length=1,trainable=True)

        self.embedded_text = self.text_embedding_layer(self.text_input)
        # self.embedded_ease = self.ease_embedding_layer(self.ease_input)
        # self.embedded_effect = self.effect_embedding_layer(self.effect_input)
        # self.embedded_satis = self.satis_embedding_layer(self.satis_input)
        
        self.d_embedded_text=Dropout(0.2)(self.embedded_text)
        # self.d_embedded_ease= Dropout(0.2)(self.embedded_ease)
        # self.d_embedded_effect= Dropout(0.2)(self.embedded_effect)
        # self.d_embedded_satis= Dropout(0.2)(self.embedded_satis)

        self.cnn1=Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(self.d_embedded_text)
        self.d_cnn1= Dropout(0.2)(self.cnn1)
        self.p=GlobalMaxPooling1D()(self.d_cnn1)
        # self.cp=concatenate([self.p,Flatten()(self.d_embedded_ease),Flatten()(self.d_embedded_effect),Flatten()(self.d_embedded_satis)],axis=-1)

        self.dense= Dense(200, activation='relu')(self.p)
        self.final= Dense(2, activation='softmax')(self.dense)
        self.model = Model(self.text_input, self.final)
        # self.model = Model([self.text_input,self.ease_input,self.effect_input,self.satis_input], self.final)
        self.model.compile(loss='categorical_crossentropy', optimizer=self.myadam, metrics=['acc'])
        self.model.save(f'/tmp/Initial_{self.getName()}.h5')


    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        history = self.__trainCheckPoints(pool)
        #take too much memory, since the history keep the annotations
        #self.histories.append(history)
        self.histories = [history]


    def __trainCheckPoints(self, pool: Pool) -> History:
        '''
        Train the model and outputs a check point to rstart training the model between each iteration, use only the last examples annotated
        Load the best model found after the iteration 
        
        ==> There is a memory leak somewhere in Keras, loading a model do not free the memory. Adding reset default graph help but the usage of memory still increase in long term.
        ==> see if keeping all history is a good idea...
        
        '''
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)

        self.__cleanSession(trainIndices)

        
        #TODO: we need to resume the training with the new data coming, see to put the callback with the right parameters, https://towardsdatascience.com/resuming-a-training-process-with-keras-3e93152ee11a
        #log.fatal("TODO: there is a leak of memory somewhere in keras/tf, the usage increase and when running out of memory stop the run without any message or exception, to be corrected...")
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
            # the model fit and keep the best model (based on its performance on the evaluation set)
            log.fatal("TODO: handle properly the memory leak with TF...")            
            self.__cleanSession(trainIndices)
            self.model = load_model(self.checkPointPath)
        else:
            epochNum = self.maxEpoch
            checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
#             self.__cleanSession(trainIndices)
            self.model = load_model(self.checkPointPath)
        #history = []
        return history
    
    def __cleanSession(self, trainIndices):
#         tf.compat.v1.reset_default_graph
#         K.clear_session()
        from keras.models import load_model
        import tensorflow.compat.v1 as tf
        from tensorflow import Graph

#         graph1 = Graph()
#         with graph1.as_default():
#             session1 = tf.Session()
#             with session1.as_default():
#                 # load model
#                 model1 = load_model('/tmp/CNN1_bestModel.h5')
#                 output1 = model1.predict(trainIndices)
#         tf.reset_default_graph
#         graph2 = Graph()
#         with graph2.as_default():
# #             session2 = tf.Session()
# #             with session2.as_default():
#                 # load model
#                 model2 = load_model('/tmp/CNN2_bestModel.h5')
#                 output2 = model2.predict(trainIndices)
#         tf.compat.v1.reset_default_graph
#         graph3 = Graph()
#         with graph3.as_default():
# #             session3 = tf.Session()
# #             with session3.as_default():
#                 # load model
#                 model3 = load_model('/tmp/CNN2_bestModel.h5')
#                 output3 = model3.predict(trainIndices)
#         tf.compat.v1.reset_default_graph
    
    @deprecated(version='0.0', reason="Do not save the best model though the check point and validation set.__train should be used")
    def __trainFit(self, pool: Pool) -> History:
        '''
        Train the model with fit only, the state of the model is kept in memory, use only the last examples annotated
        Load the last model found after all iterations
        '''
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __TrainFit")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)

        #TODO: Not sure if I resume the training correctly with new data coming, see to put the callback with the right parameters, https://towardsdatascience.com/resuming-a-training-process-with-keras-3e93152ee11a
        if pool.getLastIteration()==0:
            epochNum=self.firstIterEpoch
        else:
            epochNum=self.maxEpoch
        history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1)
        
        return history
            

    def __trainOnAllAnnotatedExamplesAvailable(self, pool: Pool) -> History:
        '''
        Train the model from scratch with fit but use all examples annotated for each iteration
        Load the best model found after the iterations
        '''
        trainPool=pool.trainEx
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainOnAllAnnotatedExamplesAvailable")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        log.fatal("TODO: double-check the indices allocated to the words, there are too many 0s for words I would expect to find in the embeddings...")
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)

        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)

        log.fatal("TODO: we need to resume the training with the new data coming, see to put the callback with the right parameters, https://towardsdatascience.com/resuming-a-training-process-with-keras-3e93152ee11a")
        #in this case I just retrain with the a descent number of epochs and keep the best model on the validation set
        epochNum=self.firstIterEpoch
        self.model = self.__getCopyInitialModel()
        checkpoint = ModelCheckpoint(self.checkPointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
        history = self.model.fit(trainIndices, to_categorical(trainLabel, num_classes=2), validation_data = (valIndices, to_categorical(valLabels)), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint])
        # the model fit and keep the best model (based on its performance on the evaluation set)
        self.model = load_model(self.checkPointPath)
    
        return history

    
    def __trainByBatches(self, pool: Pool):
        '''
        Train the model by batch to ensure the continuity with internal parameters of Keras, use only the last examples available
        '''
                
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainByBatches")
        trainText=trainPool['text']
        trainLabel=trainPool['label']
        trainIndices=self._preprocessWebMD.generate_word_index(trainText)
        trainIndices=np.array(trainIndices)
        trainLabel=np.array(trainLabel)
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']
        valIndices=self._preprocessWebMD.generate_word_index(valTexts)
        valIndices=np.array(valIndices)
        valLabels=np.array(valLabels)
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
        else:
            epochNum = self.maxEpoch
            
        for iter in range(epochNum):
            log.debug(f"Iteration: {iter}")
            pointer = 0
            while pointer<len(trainIndices):
                if ((pointer + self.batchSize)>len(trainIndices)):#last batch with irregular size
                    batchX = trainIndices[pointer:]
                    batchY = to_categorical(trainLabel[pointer:], num_classes=2)
                    log.debug(f"Batch: {pointer}-{len(trainIndices)}")
                else:
                    #regular size batch
                    batchX = trainIndices[pointer:pointer+self.batchSize]
                    batchY = to_categorical(trainLabel[pointer:pointer+self.batchSize], num_classes=2)
                    log.debug(f"Batch: {pointer}-{pointer+self.batchSize}/{len(trainIndices)}")
                try:
                    self.model.train_on_batch(batchX, batchY)
                except Exception as exc:
                    log.error(f"A problem occurs when training on this batch, just ignore it: {exc}")
                    pointer = pointer + self.batchSize
                    continue
                pointer = pointer + self.batchSize
        
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        log.info(f"CNN assigns labels and certainty to the {len(examples)} examples")
        evalExamples = examples.copy()
        evalText=evalExamples['text']
        evalIndices=self._preprocessWebMD.generate_word_index(evalText)
        evalIndices=np.array(evalIndices)
        evalLabel=self.model.predict(evalIndices,batch_size=self.batchSize,verbose=1)
        evalExamples['certainty_label_0'] = evalLabel[:,0]
        evalExamples['certainty_label_1'] = evalLabel[:,1]        
        evalLabel = np.argmax(evalLabel, axis=1)
        evalExamples['prediction']=evalLabel

        return evalExamples