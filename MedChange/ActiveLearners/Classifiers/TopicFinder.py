import logging as lg
log = lg.getLogger('TopicModel')
from typing import List
from pandas.core.frame import DataFrame
import csv
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated
from nltk.tokenize import TweetTokenizer
from pprint import pprint

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
from Data.Pool import Pool
from Properties.PropertiesInt import PropertiesInt

import re
from itertools import combinations
import pandas as pd
import tensorflow as tf
import sklearn
import keras
import os
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.models import Model, load_model

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim import models as gModels
from gensim.test.utils import datapath

# spacy for lemmatization
import spacy

# Plotting tools
import pyLDAvis
import pyLDAvis.gensim 
import matplotlib.pyplot as plt
import random
import nltk

class TopicFinder(ClassifierInt):
    '''
    A classifier based on topic modeling to detect medication changes
    We follow https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/ to write the module
    '''

    def __init__(self, name:str, properties:PropertiesInt):
        """
        Implement a LDA topic modeling algorithm
        :param: name of the topic Finder
        :param: properties used to find the path to the full corpus format as tsv with a text columns
        """
        super(TopicFinder, self).__init__(name)
        log.info(f"A topic finder: {self.getName()} is used")
        # define the threshold on which we decide if a sentence associated with a topic of interest is assigned the label associated with the topic
        # meaning, if we know that topic 3 is associated with medication change and the sentence is associated at 0.89 with topic 3, given a threshold of 0.5 the sentence will be labeled Medication change
        # now if another sentence is associated with topic 3 but only at 0.49 then it will be labeled as other since it's under the threshold
        self.classificationThreshold = 0.5
        # we need to know from a human inspection of the topics which topics are associated with medication change and medication stop
        # should be a list 
        self.MCTopics = None
        # the threshold is used to determine when the difference of coherence between different lda models with different number of topics start to plateau
        # given a threshold of 2 if the model A has a coherence of 54, the model B should have a coherence > 56, otherwise model A is picked up has the difference between the coherence is not big enough and the system is probably starting to plateau
        self.plateauThreshold = 0.015
        #number of iterations to build the topics
        self.nbrIter = 100
        #to train the topic model we are trying multiple models, here are the parameters for the loop
        #limit is the maximum number of topics for a model
        self.limit=10
        self.start=2
        self.step=1        
        self._properties= properties
        self.model = None

        
    def __buildStopWordsList(self):
        '''
        Build and compile topic modeler
        '''
        self.stopwordslist=[]
        with open(self._properties.stopWordPath, encoding='ISO-8859-1') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                self.stopwordslist.append(row[0].strip())


    def __tokenize(self, texts):
        tokenizer= TweetTokenizer(strip_handles=True, reduce_len = True, preserve_case=False)
        tokenized_texts=[]

        for text in texts:  
            try:
                tokenized_text=tokenizer.tokenize(text)
            except:
                pass
            tokenized_texts.append(tokenized_text)

        for i in range(len(tokenized_texts)):
            tokenized_texts[i]=[word for word in tokenized_texts[i] if word not in self.stopwordslist]
        return tokenized_texts


    def __make_bigrams_and_trigrams(self,tokenized_text):
        bigram = gensim.models.Phrases(tokenized_text, min_count=5, threshold=50) # higher threshold fewer phrases.
        trigram = gensim.models.Phrases(bigram[tokenized_text], threshold=50)  
        bigram_mod = gensim.models.phrases.Phraser(bigram)
        trigram_mod = gensim.models.phrases.Phraser(trigram)
        return [trigram_mod[bigram_mod[doc]] for doc in tokenized_text]


    def __lemmatization(self,tokenized_text, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        texts_out = []
        try:
            nlp = spacy.load('en', disable=['parser', 'ner'])
        except IOError as io:
            log.error("If this is a E050 error complaining about can't find model en, we need to install the model for spacy: python -m spacy download en")
            raise io
        for sent in tokenized_text:
            doc = nlp(" ".join(sent)) 
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out


    def train(self, pool: Pool):
        """
        Check if an existing model has been train, if not it trains it otherwise it reads the model from the path indicated in properties
        :param: pool will be used to get all examples (train, dev, test, and unlabeled) to train the model since it's an unsupervised method this is not a problem.
        """
        if self.model is None:
            if os.path.exists(self._properties.classifierModelsPath+f"/{self.getName()}LDAModel.md"):
                self.__buildStopWordsList()
                log.info(f"Found an existing LDA model, reading @{self._properties.classifierModelsPath}/{self.getName()}LDAModel.md, I am using this model.")
                self.model = gModels.LdaModel.load(self._properties.classifierModelsPath+f"/{self.getName()}LDAModel.md")
            else:
                log.info(f"No LDA model is given, build it from scratch.")
                self.__buildStopWordsList()
                self.model = self.__builLDAModel(pool)        
                log.info(f"Best lda model computed and written @{self._properties.classifierModelsPath}/{self.getName()}LDAModel.md")
                self.model.save(self._properties.classifierModelsPath+f"/{self.getName()}LDAModel.md")
            #change if you want to speed up the computation by entering manually the topics of interests
            #self.MCTopics = self.__setMCTopicsByHand(self.model)
            self.MCTopics = self.__setMCTopicsAuto(self.model, pool, metric='prec')
        else:
            log.debug("An existing model has already been loaded, no more training.")
    
            
    def __builLDAModel(self, pool: Pool):
        # temporary I send all texts we have to the topic finder
        AllPool=pool.getAllTexts()
        log.info(f"I received {len(AllPool)} examples for finding topics.")
        rawText=AllPool['text']
        # Tokenize texts
        tokenized_text=self.__tokenize(rawText)
        # Find bigrams and trigrams
        grammed_text=self.__make_bigrams_and_trigrams(tokenized_text)
        # Lemmantize texts
        lemmatized_text=self.__lemmatization(grammed_text)
        # Create dictionary
        id2word = corpora.Dictionary(lemmatized_text)
        # Term document frequency
        corpus_text = [id2word.doc2bow(text) for text in lemmatized_text]
        
        # find the best number of topics based on coherence scores
        lda_model = self.__findBestNbTopics(dictionary = id2word, corpus = corpus_text, texts = lemmatized_text,  limit=self.limit, start=self.start, step=self.step)
        # pprint(lda_model.print_topics())
        # Compute perplexity: a measure of how good the model is. lower the better.
        #log.info('\nPerplexity: '+str(lda_model.log_perplexity(corpus_text)))

        vis = pyLDAvis.gensim.prepare(lda_model, corpus_text, id2word)
        html = pyLDAvis.prepared_data_to_html(vis, template_type="simple", visid="pyldavis-in-org-mode")
          
        log.info("I am writing the pyLDAvis.html in the tmp directory for visualisation.")
        with open("/tmp/pyLDAvis.html", 'w') as davisOut:
            davisOut.write(html)
            davisOut.close()
        return lda_model
    
    
    def __findBestNbTopics(self, dictionary, corpus, texts, limit:int, start:int, step:int) -> List:
        """
        Compute c_v coherence for various number of topics
    
        Parameters:
        ----------
        dictionary : Gensim dictionary
        corpus : Gensim corpus
        texts : List of input texts
        limit : Max num of topics
    
        Returns:
        -------
        Write all LDA models in the tmp directory
        return the 'best' LDA models found after checking for a plateau or a decrease of performances in the coherence of the topics 
        """
        assert (limit>start), f"To find a topic model I need to run at least one iteration, so the limit given: {limit}, should be higher than the start value: {start}"
        coherence_values = []
        model_list = []
        for num_topics in range(start, limit, step):
            lda_model = gensim.models.ldamodel.LdaModel(corpus = corpus, id2word = dictionary, num_topics=num_topics, random_state=100, update_every=1, chunksize=600, passes=self.nbrIter, alpha='auto', per_word_topics=True)
            model_list.append(lda_model)
            coherencemodel = CoherenceModel(model=lda_model, texts=texts, dictionary=dictionary, coherence='c_v')
            coherence_values.append(coherencemodel.get_coherence())

        x = range(start, limit, step)
        plt.plot(x, coherence_values)
        plt.xlabel("Num Topics")
        plt.ylabel("Coherence score")
        plt.legend(("coherence_values"), loc='best')
        plt.savefig("/tmp/topicModCoherences.jpeg")
        # write all models just in case I am not able to identify the best one automatically
        for ind, model in enumerate(model_list):
            model.save(f"/tmp/{self.getName()}LDAModel_{ind}.md")
        
        #return self.__findPlateauCoherence(coherence_values, model_list)    
        return self.__findMaxCoherence(coherence_values, model_list)
 
    def __findMaxCoherence(self, coherence_values, model_list):
        modelSelected = model_list[0]
        maxCoherence = coherence_values[0]
        for ind in range(len(coherence_values)):
            if maxCoherence <= coherence_values[ind]:#replace <= if you want to keep the smaller set of topics
                log.info(f"I found better performance {coherence_values[ind]}, I keep the model having #{model_list[ind].num_topics} topics.")
                maxCoherence = coherence_values[ind]
                modelSelected = model_list[ind]
        return modelSelected
    
    def __findPlateauCoherence(self, coherence_values, model_list):
        """
        Try to find the plateau of performance defined by coherence values of the model
        """
        #try to find the plateau automatically...
        modelSelected = model_list[0]
        for ind in range(len(coherence_values)):
            if ind+1 < len(coherence_values):
                diff = coherence_values[ind+1] - coherence_values[ind]  
                if diff < self.plateauThreshold:
                    log.info(f"I think the performance start to plateau at {coherence_values[ind]} with a model having #{model_list[ind].num_topics} topics, I keep this one.")
                    modelSelected = model_list[ind]
                    return modelSelected
            else:
                log.info("I didn't find any plateau when searching for the best topic model, maybe running with more topics would help... I just chose the last model.")
                modelSelected = model_list[-1]
                return modelSelected 
        raise Exception("The code should never come here, to debug...")

    def __setMCTopicsAuto(self, ldaModel, pool: Pool, metric:'f1') -> list:
        '''
        Use a training set (or validation set) to find the topics associated with medical change
        The algorithm is simple, get the combinations of the topic numbers (-1) and evaluate the F1-score of topicFinder on each comb, take the highest score 
        :param: ldaModel a valid model
        :param: pool an existing pool
        :param: metric the metric used to choose the set of topic associated with medication, f1, rec, prec
        '''
        assert ldaModel.num_topics>=2, "I was expecting at least 2 topics, I got less. Check the code"
        assert metric=='f1' or metric=='rec' or metric=='prec', f'Unknown metric to optimize, should be f1, prec or rec, I got:{metric}'
        print(ldaModel.num_topics)
        scores = {}
        #i set the length of the combinations
        #for i in range(1, ldaModel.num_topics):
        for i in range(1, 3):#I limit to 2 topics, if you want more uncomment the line above but the classifier may classify everything as 1 since they will fall under all topics-1 
            combs = combinations(list(range(0, ldaModel.num_topics)), i)
            for cb in list(combs):
                self.MCTopics = cb
                predictionsVal = self.classify(pool.valEx)
                prec, rec, f1 = self.__evaluate(predictionsVal, pool.valEx)
                log.info(f'The combination: {cb} gives {prec}P, {rec}R, {f1}F1.')
                if metric=='f1':
                    scores[cb] = f1
                elif metric=='prec':
                    scores[cb] = prec
                elif metric=='rec':
                    scores[cb] = rec
                else:
                    raise Exception(f'Unknown metric to optimize, should be f1, prec or rec, I got:{metric}')
        # I assume that the probabilities should never be equal here, otherwise I return the first one
        orderedScores = max(scores, key=scores.get)
        self.MCTopics = orderedScores
        predictionsVal = self.classify(pool.valEx)
        predictionsVal.to_csv('/tmp/topicFinderPredsEval.tsv', sep='\t')
        #no need anymore
        self.MCTopics = None
        log.info(f"I chose the combination {orderedScores} with a {metric}: {scores[orderedScores]}")
        
        return orderedScores


    def __setMCTopicsByHand(self, ldaModel) -> list:
        '''
        Ask the user to select the topics associated with medicine change
        :return: a valid list of topic numbers
        '''
        invalideInput = True
        valideTopicNb = []
        while invalideInput:
            print(ldaModel.print_topics(ldaModel.num_topics))
            MCtopics = input(f'===> DAVY: Please, enter the topics associated with medicine changes (integer, comma separated, no space), (type "quit" to quit). There are {ldaModel.num_topics} topics in the model loaded (start at 0): ')
            if MCtopics=='quit':
                raise Exception("I can't continue with knowing the topics associated with the medication changes, quit.")
            if(re.match(r'^[0-9]+(,[0-9]+)*$', MCtopics)):
                MCtopics = MCtopics.split(',')
                invalideInput = False
                for topic in MCtopics:
                    if int(topic)>(ldaModel.num_topics-1):
                        print(f"the topic {topic} is not a valid topic number...")
                        valideTopicNb = []
                        invalideInput = True
                        break
                    else:
                        valideTopicNb.append(int(topic))
        return valideTopicNb


    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - label: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        assert self.MCTopics is not None, "The topic numbers associated with Medication changes status should be given otherwise I can't make the classification..."
        log.info(f"TopicFinder assigns labels and certainty to the {len(examples)} examples")
        
        log.fatal("TODO: check that I call correctly the LDA model on new texts, otherwise it will crash or not compute the expected answer with tweets")
        
        #I preprocess the text
        rawText = examples['text']
        tokenized_text=self.__tokenize(rawText)
        # Find bigrams and trigrams
        grammed_text=self.__make_bigrams_and_trigrams(tokenized_text)
        # Lemmantize texts
        lemmatized_text=self.__lemmatization(grammed_text)
        
        #then map the text with the current dictionary
        corpus_text = [self.model.id2word.doc2bow(text) for text in lemmatized_text]
        # Init output
        sent_topics_df = pd.DataFrame()
     
        # If you want to write the classification of the system for the batch tested
        # Get main topic in each document
        docIDs = list(examples.index)
        for i, row in enumerate(self.model[corpus_text]):
            assert len(row)==3 and len(row[0])>0, f"I have an unexpected row returned by the LDA model when processing the sentence: {lemmatized_text[i]} -> {corpus_text[i]}"
            #row is a list composed of 3 elements: 
            #- list with (topic, probab) which give the participation of each topic forn this text,
            #- list of tuples (token id, [topics order by probability]) the topics assigned to each token in the text
            #- list of tuples (token id, [(topics, probability)]) the topic and its probability assigned to each token in the text
            row = sorted(row[0], key=lambda x: (x[1]), reverse=True)
            #Davy
            # Get the Dominant topic, Perc Contribution and Keywords for each document
            for j, (topic_num, prop_topic) in enumerate(row):
                if j == 0:  # => dominant topic
                    wp = self.model.show_topic(topic_num)
                    topic_keywords = ", ".join([word for word, prop in wp])
                    sent_topics_df = sent_topics_df.append(pd.Series([docIDs[i], int(topic_num), round(prop_topic,4), topic_keywords]), ignore_index=True)
                else:
                    break
        sent_topics_df.columns = ['docID', 'Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']
        # Add original text to the end of the output
        contents = pd.Series(lemmatized_text)
        sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
        df_dominant_topic = sent_topics_df.reset_index()
        df_dominant_topic.columns = ['docID', 'Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']
        # Show
        df_dominant_topic.to_csv(f'/tmp/{self.getName()}_dominantTopicPerDoc.tsv', sep='\t')
        
        evalExamples = examples.copy()
        certainty_label_0 = []
        certainty_label_1 = []
        for i, row in enumerate(self.model[corpus_text]):
            assert len(row)==3 and len(row[0])>0, f"I have an unexpected row returned by the LDA model when processing the sentence: {lemmatized_text[i]} -> {corpus_text[i]}"
            row = sorted(row[0], key=lambda x: (x[1]), reverse=True)
            for j, (topic_num, prop_topic) in enumerate(row):
                if j == 0:  # => dominant topic
                    if topic_num in self.MCTopics and prop_topic > self.classificationThreshold:
                        certainty_label_1.append(prop_topic)
                        certainty_label_0.append(1-prop_topic)
                    else:
                        certainty_label_0.append(prop_topic)
                        certainty_label_1.append(1-prop_topic)
                else:
                    break
        
        evalExamples['certainty_label_0'] = certainty_label_0
        evalExamples['certainty_label_1'] = certainty_label_1
        evalExamples['prediction']=evalExamples[['certainty_label_0', 'certainty_label_1']].idxmax(axis=1)
        evalExamples['prediction'].replace(to_replace='certainty_label_0', value=0, inplace=True)
        evalExamples['prediction'].replace(to_replace='certainty_label_1', value=1, inplace=True)
        
        return evalExamples
    

    def __evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> float:
        '''
        Internal function to evaluate the topicFinder given the topics thought to be associated with the class of interest
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
        prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')        
        f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return prec, rec, f1
    # def format_topics_sentences(ldamodel=lda_model, corpus=corpus, texts=data):
    #     # Init output
    #     sent_topics_df = pd.DataFrame()

    #     # Get main topic in each document
    #     for i, row in enumerate(ldamodel[corpus]):
    #         row = sorted(row, key=lambda x: (x[1]), reverse=True)
    #         # Get the Dominant topic, Perc Contribution and Keywords for each document
    #         for j, (topic_num, prop_topic) in enumerate(row):
    #             if j == 0:  # => dominant topic
    #                 wp = ldamodel.show_topic(topic_num)
    #                 topic_keywords = ", ".join([word for word, prop in wp])
    #                 sent_topics_df = sent_topics_df.append(pd.Series([int(topic_num), round(prop_topic,4), topic_keywords]), ignore_index=True)
    #             else:
    #                 break
    #     sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']

    #     # Add original text to the end of the output
    #     contents = pd.Series(texts)
    #     sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    #     return(sent_topics_df)


    # df_topic_sents_keywords = format_topics_sentences(ldamodel=optimal_model, corpus=corpus, texts=data)

    # # Format
    # df_dominant_topic = df_topic_sents_keywords.reset_index()
    # df_dominant_topic.columns = ['Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']

    # # Show
    # df_dominant_topic.head(10)