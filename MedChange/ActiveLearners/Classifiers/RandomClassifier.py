'''
Created on Oct 8, 2019

@author: davy
'''

import logging as lg
log = lg.getLogger('RandomClassifier')
from pandas.core.frame import DataFrame
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from deprecated import deprecated

from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Data.Pool import Pool


class RandomClassifier(ClassifierInt):
    '''
    A dummy classifier which returns a random decision
    The classifier is binary: 0 or 1; also indicate a random probability for its decisions
    '''

    def __init__(self, name:str):
        '''
        :param::param: name, the name of the classifier for further reference
        '''
        super(RandomClassifier, self).__init__(name)
        log.info(f"A random classifier: {self.getName()} is used, be sure that this is what you want to use...")
        #A map to record the performance of the classifier after each training iteration
        #iteration -> {precision,recall,f1}
        self.performance = {}
        

    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        log.info(f"I received {len(pool.getMostRecentTrainingExamples())} examples for training, but I am a random classifier, not much to train for...")
    
    
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty: a dictionary where each possible label is assigned a probability
        '''
        log.info(f"Random classifier randomly assigns labels and certainty to the {len(examples)} examples")
        examples = examples.copy()
        examples['prediction'] = np.random.randint(2, size=len(examples))
        rand0 = np.random.rand(1,len(examples))[0]
        examples['certainty_label_0'] = rand0
        examples['certainty_label_1'] = 1-rand0
        return examples
        
    
#     def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> dict:
#         '''
#         evaluate the performance of the classifier on the set of examples given
#         :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
#         :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
#         :return: P,R,F1 score
#         '''
#         assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
#         assert 'label' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
#         
#         log.fatal("Triple check the evaluation here.")
#         return self.__getScores__(examplesTruth, examplesPredicted)
#         
#     def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
#         '''
#         :param: examplesTruth a dataframe with the docIDs and label  
#         :param: examplesPredicted a dataframe with the docIDs and label
#         :return: list with the Precision, Recall, F1 scores computed
#         '''        
#         df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['label']})
#         assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."
#         df.to_csv(f"/tmp/{self.getName()}_EvaluationOutput.tsv", sep='\t')
#         log.info(f"{self.getName()}:\n{confusion_matrix(list(df['truths']), list(df['preds']))}")
#         #precision, recall, f1, support
#         prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
#         rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
#         f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
#         log.info(f"F1:{f1}; P:{prec}; R:{rec}")
#         
#         return {'precision':prec, 'recall':rec, 'f1':f1}
    
    
    # @deprecated(version='0.0', reason="Too slow to iterrows, use __getScores__")
    # def __getScoresByIterrows__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
    #     #now I can compute the scores, I'll go by rows to be sure to do what I want to do...
    #     docIDsPred = []
    #     docIDsTruth = []
    #     preds = []
    #     truths = []
    #
    #     for _, row in examplesTruth.iterrows():
    #         docIDsTruth.append(row['docID'])
    #         truths.append(row['label'])
    #         predRow = examplesPredicted.loc[examplesPredicted['docID']==row['docID'], ['docID','label']]
    #         assert len(predRow)==1, f"I retrieve multliple line in the prediction set for the docID {row['docID']}, whereas this should be unique, check the data."
    #         docIDsPred.append(predRow['docID'].values[0])
    #         preds.append(predRow['label'].values[0])
    #     df = DataFrame({'docIDsTruth':docIDsTruth, 'truths':truths, 'docIDsPred':docIDsPred, 'preds':preds})
    #     df.to_csv("/tmp/RandomClassifierEvaluationOutput.tsv", sep='\t', index=False)
    #     print(confusion_matrix(list(df['truths']), list(df['preds'])))
    #     #precision, recall, f1, support
    #     prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
    #     rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
    #     f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
    #
    #     return {'precision':prec, 'recall':rec, 'f1':f1}
    #
#     def getPerformanceHistory(self) -> dict:
#         '''
#         :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
#         '''
#         return self.performance
        
        