'''
Created on Jul 7, 2020

inspired from the blog
Implemented in https://colab.research.google.com/drive/1WQY_XxdiCVFzjMXnDdNfUjDFi0CN5hkT#scrollTo=oIjlbG8u9xis
https://www.kdnuggets.com/2020/02/intent-recognition-bert-keras-tensorflow.html

To fight 
For the imbalanced data see https://www.tensorflow.org/tutorials/structured_data/imbalanced_data
@author: dweissen
'''
import logging as lg
from pandas.core import series
log = lg.getLogger('BertTF2Classifier')

from Data.Pool import Pool
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt

import os
import re
from tqdm import tqdm
import pandas as pd
from pandas.core.frame import DataFrame
import numpy as np

import bert
import tensorflow as tf
from tensorflow import keras
from bert import BertModelLayer
from bert.loader import StockBertConfig, map_stock_config_to_params, load_stock_weights
from bert.tokenization.bert_tokenization import FullTokenizer
from tensorflow.keras.callbacks import ModelCheckpoint


class BertTF2Classifier(ClassifierInt):
    '''
    Instantiate a Bert layer for computing the embeddings and pass through a set of additinal layers to learn the task
    '''

    def __init__(self, name:str, InitialModelsPath=None):
        '''
        :param: name, the name of the classifier for further reference
        :param InitialModelsPath: the path to an initial Model (randomly initialized or pre-trained)
        '''
        super(BertTF2Classifier, self).__init__(name)
        log.info(f"A Bert classifier: {self.getName()} is used")

        self.performance = {}

        self.batchSize = 32
        self.firstIterEpoch=2
        self.maxEpoch=2
        self.MAX_SEQ_LEN=356
        self.TRAIN_EMBEDDINGS = True
        self.TRAIN_BERT_LAYERS =True
        self.WEIGHTED_CLASSES = False        
        self.METRICS = [
            keras.metrics.TruePositives(name='tp'),
            keras.metrics.FalsePositives(name='fp'),
            keras.metrics.TrueNegatives(name='tn'),
            keras.metrics.FalseNegatives(name='fn'), 
            keras.metrics.BinaryAccuracy(name='accuracy'),
            keras.metrics.Precision(name='precision'),
            keras.metrics.Recall(name='recall'),
#       #keras.metrics.AUC(name='auc'),
#       keras.metrics.SparseCategoricalAccuracy(name="acc")
        ] 
        
        self.bert_model_name="uncased_L-12_H-768_A-12" #basic BERT for production
        #self.bert_model_name="uncased_L-2_H-128_A-2" #tiny BERT to debug
        self.bert_ckpt_dir = os.path.join("/Users/dweissen/Downloads/dataK/", self.bert_model_name)
        self.bert_ckpt_file = os.path.join(self.bert_ckpt_dir, "bert_model.ckpt")
        self.bert_config_file = os.path.join(self.bert_ckpt_dir, "bert_config.json")
        
        self.tokenizer = FullTokenizer(vocab_file=os.path.join(self.bert_ckpt_dir, "vocab.txt"))
        
        if not os.path.exists('/home/gesy17/'):
            self.checkPointPath = f'/tmp/{self.getName()}_bestModel'
            self.initialModelPath = f'/tmp/Initial_{self.getName()}'
            self.currentModelPath = f'/tmp/Current_{self.getName()}'
            if InitialModelsPath is None:
                self.__buildNN()
            else:
                log.info(f"An existing model is given and will be loaded from {InitialModelsPath}.")
                self.initialModelPath = InitialModelsPath
                self.model = tf.keras.models.load_model(self.initialModelPath)
        
        #the list of historie return after each training
        self.histories = []
        
    def __buildNN(self):

        with tf.io.gfile.GFile(self.bert_config_file, "r") as reader:
            bc = StockBertConfig.from_json_string(reader.read())
            bert_params = map_stock_config_to_params(bc)
            bert_params.adapter_size = None
            #bert_params.adapter_init_scale = 1e-5
            bert = BertModelLayer.from_params(bert_params, name="bert")
            #If we want to train only the layer added and freeze the bert layer
            bert.trainable = self.TRAIN_BERT_LAYERS
              
        input_ids = keras.layers.Input(shape=(self.MAX_SEQ_LEN, ), dtype='int32', name="input_ids")
        bert_output = bert(input_ids)

        print("bert shape", bert_output.shape)

#         cls_out = tf.keras.layers.Lambda(lambda seq: seq[:, 0, :])(bert_output)
#         cls_out = tf.keras.layers.Dropout(0.5)(cls_out)
#         x1 = tf.keras.layers.Dense(units=768, activation="tanh")(cls_out)
#         x2 = tf.keras.layers.Dropout(0.5)(x1)
#         #logits = keras.layers.Dense(units=len(classes), activation="softmax")(logits)
#         x3 = tf.keras.layers.Dense(units=1, activation="sigmoid")(x2)

        #cls_out = tf.keras.layers.Lambda(lambda seq: seq[:, 0, :])(bert_output)
        #print(cls_out.shape)
        #d_embedded_text = keras.layers.Dropout(0.5)(cls_out)
        #print(d_embedded_text.shape)
        #d_embedded_text = tf.keras.layers.Dense(units=768, activation="tanh")(d_embedded_text)
        cnn1 = keras.layers.Conv1D(padding="same", activation="relu", strides=1, filters=400, kernel_size=3)(bert_output)
        d_cnn1 = keras.layers.Dropout(0.2)(cnn1)
        p = keras.layers.GlobalMaxPooling1D()(d_cnn1)
        x2 = keras.layers.Dense(200, activation='relu')(p)
        x3 = tf.keras.layers.Dense(units=1, activation="sigmoid")(x2)

        self.model = tf.keras.Model(inputs=input_ids, outputs=x3)
        self.model.build(input_shape=(None, self.MAX_SEQ_LEN))
        log.debug(self.model.summary())
  
        #see https://colab.research.google.com/github/kpe/bert-for-tf2/blob/master/examples/tpu_movie_reviews.ipynb
        #bert.apply_adapter_freeze() # if we want only some layer to be frozen if I understand correctly, goes with adapter_size
        bert.embeddings_layer.trainable=self.TRAIN_EMBEDDINGS
  
        load_stock_weights(bert, self.bert_ckpt_file)
        
        self.model.compile(
            optimizer=keras.optimizers.Adam(1e-5),
            #loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            loss=keras.losses.BinaryCrossentropy(),
            metrics=self.METRICS
        )

    def __preprocess_sentence(self, texts):
        """
        Simple preprocessing of the sentences: lowercase and remove twitter users and urls
        """
        USER_NAME = ' USERNAME '
        URL = ' URL '
        def __preprocess_text(text):
            text = text.strip().lower();
            # Mask user names
            text = re.sub(r'(?<!\w)@[a-z0-9_]+', USER_NAME, text)
            # URLs
            text = re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', URL, text)
            return text
        texts = texts.apply(__preprocess_text)
        return texts
    
    
    def __generate_sentence_index(self, texts: series):
        """
        Encode the text given for BERT 
        """
        def get_ids(tokens, tokenizer, max_seq_length):
            """Token ids from Tokenizer vocab"""
            token_ids = tokenizer.convert_tokens_to_ids(tokens,)
            input_ids = token_ids + [0] * (max_seq_length-len(token_ids))
            return input_ids
        def create_single_input(sentence,MAX_LEN):
            """"""
            stokens = self.tokenizer.tokenize(sentence)
            if len(stokens)>=self.MAX_SEQ_LEN:
                log.warn(f"One sentence tokenized is longer (len = {len(stokens)}) than max seq len {self.MAX_SEQ_LEN}")
            stokens = stokens[:MAX_LEN]
            stokens = ["[CLS]"] + stokens + ["[SEP]"]
            ids = get_ids(stokens, self.tokenizer, self.MAX_SEQ_LEN)
 
            return ids
        #first rpreprocess
        texts = self.__preprocess_sentence(texts)
        #then tokenize
        input_ids = []
        for txt in tqdm(texts,position=0, leave=True):
            if pd.isna(txt):
                log.error("I found a text which was empty and transformed into nan by pandas, check data. I just retransform into an empty string")
                txt = ""
            ids=create_single_input(txt,self.MAX_SEQ_LEN-2)
            input_ids.append(ids)
 
        return [np.asarray(input_ids, dtype=np.int32)]

    
    def train(self, pool: Pool):
        '''
        :param: use the most recent examples annotated from the pool to update the classifier model
        :param: evaluate, if True, the classifier will evaluate its score on the evaluation set and record its performance
                          if false, no evaluation is done
        :return: a model updated 
        '''
        
        log.critical("A finir correctly!")
        
        trainPool=pool.getMostRecentTrainingExamples()
        log.info(f"I received {len(trainPool)} examples for training CNN. Use __trainCheckPoints")
        
        trainText=trainPool['text']
        trainLabel=trainPool['label']
#         trainText = trainText[:200]
#         trainLabel = trainLabel[:200]
        
        trainIndices=self.__generate_sentence_index(trainText)
        trainLabel=np.array(trainLabel)
        assert len(trainLabel)==len(trainIndices[0]), f"number of training examples {len(trainIndices[0])} and labels {len(trainLabel)} are not equal, check the preprocessing in Bert."
        
        valTexts = pool.valEx['text']
        valLabels = pool.valEx['label']        
#         valTexts = valTexts[:200]
#         valLabels = valLabels[:200]
        
        valIndices=self.__generate_sentence_index(valTexts)
        valLabels=np.array(valLabels)
        assert len(valLabels)==len(valIndices[0]), f"number of validation examples {len(valIndices[0])} and labels {len(valLabels)} are not equal, check the preprocessing in Bert."
        
        if self.WEIGHTED_CLASSES:
            neg, pos = np.bincount(trainLabel)
            log.debug(f"Weighting classes --- positive count: {pos}, negative count:{neg}")
            weight_for_0 = (1 / neg)*(neg+pos)/2.0 
            weight_for_1 = (1 / pos)*(neg+pos)/2.0
            class_weight = {0: weight_for_0, 1: weight_for_1}
            print(f"classes are weighted: 0->{weight_for_0} and 1->{weight_for_1}")
        else:
            class_weight = {0: 1, 1: 1}
        
        if pool.getLastIteration()==0:
            epochNum = self.firstIterEpoch
            checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint], class_weight=class_weight)
            self.model = tf.keras.models.load_model(self.checkPointPath, custom_objects={"BertModelLayer": bert.model.BertModelLayer})
        else:
            epochNum = self.maxEpoch
            checkpoint = ModelCheckpoint(filepath=self.checkPointPath, monitor = 'val_loss', verbose = 1, save_best_only = True, mode = 'min')
            history = self.model.fit(trainIndices, trainLabel, validation_data = (valIndices, valLabels), batch_size=self.batchSize, epochs=epochNum, shuffle=False, verbose=1, callbacks = [checkpoint], class_weight=class_weight)
            self.model = tf.keras.models.load_model(self.checkPointPath, custom_objects={"BertModelLayer": bert.model.BertModelLayer})
        log.critical("a verifier comment sauvegarder le model")
        self.model.save(self.checkPointPath+f'_ALiteration{pool.getLastIteration()}')

        self.histories = [history]
    
        
    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty: a dictionary where each possible label is assigned a probability
        '''
        log.info(f"BertClassifier assigns labels and certainty to the {len(examples)} examples")
        predExamples = examples.copy()
        predText=predExamples['text']
        predIndices=self.__generate_sentence_index(predText)
        predLabel=self.model.predict(predIndices, batch_size=self.batchSize, verbose=1)
        
        predExamples['certainty_label_0'] = predLabel
        predExamples['certainty_label_1'] = 1-predLabel        
        predLabel = predExamples['certainty_label_0']>0.5
        predExamples['prediction']=predLabel

        return predExamples    
        
        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> dict:
        '''
        evaluate the performance of the classifier on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'label' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 score
        '''
        raise Exception("This function has not been implemented.")
        
        
    def __getScores__(self, examplesTruth: DataFrame, examplesPredicted: DataFrame) -> dict:
        '''
        :param: examplesTruth a dataframe with the docIDs and label  
        :param: examplesPredicted a dataframe with the docIDs and prediction
        :return: list with the Precision, Recall, F1 scores computed
        '''
        raise Exception("This function has not been implemented.")
        
        
    def getPerformanceHistory(self) -> dict:
        '''
        :return: a dictionary of all evaluations made on the evaluation set after training for each iteration (empty if no evaluation were ran after training)
        '''
        return self.performance    