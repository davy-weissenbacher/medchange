'''
Created on Nov 7, 2019

@author: davy
'''

import logging as lg
from Performance.Performance import Performance
log = lg.getLogger('UncertaintySampler')


from Data.Pool import Pool
import numpy as np
from pandas.core.frame import DataFrame
from enum import Enum
import scipy.stats as spy
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score
# lg.getLogger('matplotlib').setLevel(lg.WARNING)
# from matplotlib.backends.backend_pdf import PdfPages
# import matplotlib.pyplot as plt
# from typing import List
from keras.callbacks import History

from ActiveLearners.ActiveLearnerInt import ActiveLearnerInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Visualization.PerformanceDrawer import PerformanceDrawer
from Performance.Performance import Scores


class UncertaintyMeasure(Enum):
    '''
    A simple Enum class to enumerate the measure of uncertainty to use to return the examples
    '''
    LeastConfident = 1
    Margin = 2
    Entropy = 3
    
class USQueryingAlgorithm(object):
    def __init__(self, measure: UncertaintyMeasure):
        self.uncertaintyMeasure = measure 

    
    def getUncertaintyMeasure(self) -> UncertaintyMeasure:
        return self.uncertaintyMeasure

    
    def getExamplesToQuery(self, examples: DataFrame):
        log.info(f"I compute miExamples with {self.getUncertaintyMeasure()}")
        if self.getUncertaintyMeasure()==UncertaintyMeasure.LeastConfident:
            return self.__getUncertaintyRankedExamples(examples)
        elif self.getUncertaintyMeasure()==UncertaintyMeasure.Margin:
            return self.__getMarginRankedExamples(examples)
        elif self.getUncertaintyMeasure()==UncertaintyMeasure.Entropy:
            return self.__getEntropyRankedExamples(examples)

        
    def __getUncertaintyRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the max of uncertainty
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = 1-P(x^|x), x^ being the most likely class, we compute:
         1-0.85 = 0.15
         1-0.6 = 0.4
         1-0.61 = 0.39
         And the ex.2 should be returned as it is the less certain 
        '''
        raise Exception("Uncertainty does not seems to be computable for binary classification, just the margin.")
           
        
    def __getMarginRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the max margin between classes
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = P(x1|x)-P(x2|x) where x1 is the most likely examples and x2 the second most likely example
         0.85-0.1 = 0.75
         0.6-03 = 0.3
         0.61-0.39 = 0.22
         and the example 3 should be return since it exhibits the smallest margin that is the ma uncertainty
        '''
        if len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))==2:
            #binary classification
            predictions['margin'] = (predictions['certainty_label_0'] - predictions['certainty_label_1']).abs()
        elif len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))>2:
            #multiclass
            raise Exception("Not implemented for multi-class")
        else:
            #should not enter here but in case
            raise Exception("I did not find the expected number of classes, found 0 or 1 class, check the code and the dataframe input.")        
        # we want to minimize the difference since a prediction of 0.50 is the most uncertain one
        predictions.sort_values(by='margin', ascending=True, inplace=True)
        predictions.drop(columns='margin', inplace=True)
         
        return predictions
    
    
    def __getEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :return: the n examples ranked by the entropy between classes
        Assume 3 examples classified in 3 different classes, each probability assigned to one class: 
        [[0.1 , 0.85, 0.05],
         [0.6 , 0.3 , 0.1 ],
         [0.39, 0.61, 0.0 ]]
         U(x) = -SigmaK pk * log(pk) where pk is the probability assign to the example to belong to the class k
         entropy(0.1 , 0.85, 0.05) = 0.5181862130502128
         entropy(0.6 , 0.3 , 0.1) = 0.89794572
         entropy(0.39, 0.61, 0.0) = 0.66874809
         and the example 2 should be returned since it has the highest entropy, i.e. the distribution is closer to the uniform distribution
        '''
        if len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))==2:
            #binary classification
            predictions['entropy'] = spy.entropy([predictions['certainty_label_0'], 1-predictions['certainty_label_0']])
        elif len(set(predictions.filter(regex=("certainty_label_[0-9]+")).columns))>2:
            #multiclass classification
            predictions['entropy'] = predictions.filter(regex=("certainty_label_[0-9]+")).apply(spy.entropy, axis=1)
        else:
            #should not enter here but in case
            raise Exception("I did not find the expected number of classes, found 0 or 1 class, check the code and the dataframe input.")
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        predictions.drop(columns='entropy', inplace=True)
        
        return predictions
                

class UncertaintySampler(ActiveLearnerInt):
    '''
    This class implement the basic approach for active learning, the uncertainty sampling.
    I am following the implementation of modal: https://modal-python.readthedocs.io/en/latest/content/query_strategies/uncertainty_sampling.html 
    '''
    def __init__(self, classifier: ClassifierInt):
        '''
        Constructor
        '''
        self.classifier = classifier
        self._queryingAlgo: Optional[QueryingAlgorithm] = None

    
    def getClassifiers(self) -> ClassifierInt:
        return [self.classifier]


    def setQueryingAlgorithm(self, queryingAlgorithm: USQueryingAlgorithm):
        '''
        :param: queryingAlgorithm the querying algorithm to be used to select the most informative examples
        '''
        assert isinstance(queryingAlgorithm , USQueryingAlgorithm), f"I was expecting an instance of USQueryingAlgorithm, I got {queryingAlgorithm.__class__.__name__}, check the code." 
        self._queryingAlgo = queryingAlgorithm


    def getQueryingAlgorithm(self) -> USQueryingAlgorithm:
        '''
        :return: the querying algorithm set for the uncertainty sampler, 
        :raise exception: if the querying algorithm has not been set yet
        '''
        assert self._queryingAlgo is not None, "The code is calling for a querying algorithm which has not been instantiated in the uncertainty sampler."
        return self._queryingAlgo
        

    def train(self, pool: Pool):
        '''
        :param: a pool which contains the most recent annotated examples to run the next training iteration
        :return: a model updated
        '''
        log.info("Training the classifier...")
        self.getClassifiers()[0].train(pool)
        log.info("The classifier has finished its training...")

    
    def classify(self, examples: DataFrame):
        return self.getClassifiers()[0].classify(examples)

        
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 scores
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'].astype('int32'), 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction'].astype('int32')})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."        
        
        cf = confusion_matrix(list(df['truths']), list(df['preds']))        
        cr = classification_report(list(df['truths']), list(df['preds']),digits=4)
        acc=accuracy_score(list(df['truths']), list(df['preds']))
        if df['truths'].nunique()>2 or df['preds'].nunique()>2:
            #We are in a multinomial classification
            prec = precision_score(list(df['truths']), list(df['preds']), average='macro')
            rec = recall_score(list(df['truths']), list(df['preds']), average='macro')
            f1 = f1_score(list(df['truths']), list(df['preds']), average='macro')
        else:
            #binary
            prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return Scores(confusionMatrix=cf, classificationReport=cr, F1positive=f1, PrecisionPositive=prec, RecallPositive=rec, Accuracy=acc)

    
    def drawPerfOnEvaluation(self, path:str):
        drawer = PerformanceDrawer()
        assert len(self.getClassifiers()[0].histories)>0, "Try to draw performance on the evaluation set before computing it..."
        drawer.drawHistory(self.getClassifiers()[0].histories[-1], path)

        
    def drawPerformance(self, performance: Performance, path:str):
        drawer = PerformanceDrawer()
        drawer.drawPerformance(performance, path)


    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: pool with all unlabeled data
        :param: ExQueriedBatchSize the number of most informative examples returned to be annotated
        :return: a set of examples that should be the most informative to annotate
        '''
        cpUnlabelExs = pool.unlabEx.copy()
        predictions = self.classifier.classify(cpUnlabelExs)
        assert set(['certainty_label_0', 'certainty_label_1', 'label']).issubset(predictions.columns), f"The classifier {self.classifier.getName()} did not return a dtatframe with the expected columns: 'label', 'certainty_label_0', 'certainty_label_1', check the code"
        log.info(f"I run the querying algorithm {self.getQueryingAlgorithm().getUncertaintyMeasure()}")
        miExamples = self.getQueryingAlgorithm().getExamplesToQuery(predictions)
        miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])
        
        #delete the useless columns
        miExamples.drop(columns=list(miExamples.filter(regex=("certainty_label_[0-9]+")).columns), inplace=True)
        # and add the column empty for annotation
        miExamples['label'] = np.NaN
        return miExamples 


    def clear(self):
        #I have only one classifier nothing to do
        return
