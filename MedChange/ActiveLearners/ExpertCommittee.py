'''
Created on Oct 8, 2019

@author: davy
'''
import logging as lg
log = lg.getLogger('ExpertCommittee')
import os

from multiprocessing import Pipe, Process
import pandas as pd
from pandas.core.frame import DataFrame
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from TextPreprocess.WebMDPreprocess import PreprocessWebMD
lg.getLogger('matplotlib.font_manager').setLevel(lg.ERROR)
lg.getLogger('matplotlib.backends.backend_pdf').setLevel(lg.ERROR)
import numpy as np
from enum import Enum
import scipy.stats as spy
import joblib
from typing import List
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, classification_report, accuracy_score

from keras import backend as K

from Data.Pool import Pool
from ActiveLearners.ActiveLearnerInt import ActiveLearnerInt
from ActiveLearners.Classifiers.ClassifierInt import ClassifierInt
from Performance.Performance import Scores, Performance
from Visualization.PerformanceDrawer import PerformanceDrawer

class DisagreementAlgorithm(Enum):
    '''
    A simple Enum class to enumerate the disagreement algorithms implemented and ready to be used by the committee through their querying algorithm
    '''
    voteEntropy = 1
    consensusEntropy = 2
    maxDisagreement = 3

class ECQueryingAlgorithm(object):
    '''
    Since we will have to try multiple querying algorithms to get the best I just externalize the querying algorithm from the committee expert
    it instantiate only with one algorithm and always return the result of this algorithm when asked for the most informative examples
    
    For the algorithms I re-implement those given by Burr Settles "active learning"
    See for examples: https://modal-python.readthedocs.io/en/latest/content/query_strategies/Disagreement-sampling.html#disagreement-sampling-for-classifiers
    '''
    
    def __init__(self, algorithm: DisagreementAlgorithm, committee: ActiveLearnerInt):
        '''
        :param: instantiate the querying algorithm to be used, only disagreement algorithms and listed in the enum DisagreementAlgorithm are implemented
        '''
        self.algorithm = algorithm
        self.committee = committee
    
    def getAlgorithmName(self) -> DisagreementAlgorithm:
        return self.algorithm
    
    def getExamplesToQuery(self, examples: DataFrame):
        log.info(f"I compute miExamples with {self.algorithm}")
        if self.algorithm == DisagreementAlgorithm.voteEntropy:
            return self.__getVoteEntropyRankedExamples(examples)
        elif self.algorithm == DisagreementAlgorithm.consensusEntropy:
            return self.__getConsensusEntropyRankedExamples(examples)
        elif self.algorithm == DisagreementAlgorithm.maxDisagreement:
            return self.__getMaxDisagreementRankedExamples(examples)
        else:
            raise Exception(f"Unexpected querying algorithm name has been asked: {self.getAlgorithmName()}, check the code.")

    def __getVoteEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns, label_i and certainty_i, for each classifier i, where their respective predictions are given for each examples
        :return: the examples ranked according to the entropy of the vote distribution
        Assuming predictions for 5 examples by 3 classifiers [[0, 1, 0], [1, 1, 2], [0, 1, 2], [2, 2, 2], [1, 2, 2]]
        We have the probability for the vote of the first example 0: 0.6666, 1: 0.3333, 2: 0.0; for the vote of the second example 0: 0.0, 1: 0.6666, 2: 0.3333 etc.
        The entropy is then computed 
        for ex 1: - 0.6666*ln(0.6666) - 0.3333*ln(0.3333) - 0*ln(0.0) = 0.6365
        for ex 2: - 0*ln(0.0) - 0.6666*ln(0.6666) - 0.3333*ln(0.3333) = 0.6365
        If we make all computations for our example we have [0.6365, 0.6365, 1.0986, 0.0, 0.6365], so ex.3 should be returned, then 1,2,5 and last ex.4
        '''
        #I count the number of 0s 
        vote0 = predictions.filter(regex='^label_[0-9]+$').isin([0]).sum(1)
        # then divide by the number of classifiers
        probVote0 = vote0.divide(len(predictions.filter(regex='^label_[0-9]+$').columns))
        # and compute the entropy for each row
        predictions['entropy'] = spy.entropy([probVote0, (1-probVote0)])
        # I rank the examples by their entropy, higher first
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        #remove the now useless column entropy
        predictions.drop(columns='entropy', inplace=True)
        # and return our newly correctly ranked dataframe
        return predictions
    
    def __getConsensusEntropyRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns: label_i and certainty_i for each classifier i, where their respective predictions are given for each examples and the probabilities assigned in the certainty columns
        :return: the examples ranked according to the entropy of the vote distribution computed this time based on the probability assign for each possible label by each classifier
        Assuming predictions for each 3 possible labels of 5 examples by 3 classifiers 
        [[[0.9, 0.1, 0.0]    # \
        [0.3, 0.7, 0.0]    # |
        [1.0, 0.0, 0.0]    # |  <-- class probabilities for the first classifier
        [0.2, 0.2, 0.6]    # |
        [0.2, 0.7, 0.1]],  # /
        [[0.0, 1.0, 0.0]    # \
        [0.4, 0.6, 0.0]    # |
        [0.2, 0.7, 0.1]    # |  <-- class probabilities for the second classifier
        [0.3, 0.1, 0.6]    # |
        [0.0, 0.0, 1.0]],  # /
        [[0.7, 0.2, 0.1]    # \
        [0.4, 0.0, 0.6]    # |
        [0.3, 0.2, 0.5]    # |  <-- class probabilities for the third classifier
        [0.1, 0.0, 0.9]    # |
        [0.0, 0.1, 0.9]]]
        If we take the predictions of ex.2, we have C1: [0.3, 0.7, 0.0], C2: [0.4, 0.6, 0.0], C3: [0.4, 0.0, 0.6], if we take the mean for each possible label, 
        i.e. for label 1 (0.3+0.4+0.4)/3=0.366; for label 2 (0.7+0.6+0.0)/3=0.433, for label 3 (0.0+0.0+0.6)/3=0.2
        we can now compute the entropy for each prediction: -0.366*LN(0.366) -0.433*LN(0.433) -0.2*LN(0.2) = 1.0521908
        for all examples: 
        prob predictions: [[0.5   , 0.4333, 0.0333], [0.3666, 0.4333, 0.2   ], [0.5   , 0.3   , 0.2   ], [0.2   , 0.1   , 0.7   ], [0.0666, 0.2666, 0.6666]]
        entropy: [0.8167, 1.0521, 1.0296, 0.8018, 0.8033]
        the ranks returned are then ex 2,3,1,5,4
        '''
        # First I get the mean of the certainty of all classifiers for an example to be label 0
        meanL0 = predictions.filter(regex='^certainty_label_0_C[0-9]+$').mean(axis=1)
        # I compute the entropy for these mean
        predictions['entropy'] = spy.entropy([meanL0, (1-meanL0)])
        # I rank the examples by their entropy, higher first
        predictions.sort_values(by='entropy', ascending=False, inplace=True)
        #remove the now useless column entropy
        predictions.drop(columns='entropy', inplace=True)
        # and return our newly correctly ranked dataframe     
        return predictions
    
    def __getMaxDisagreementRankedExamples(self, predictions: DataFrame):
        '''
        :param: expect a dataframe with columns: label_i and certainty_i for each classifier i, where their respective predictions are given for each examples and the probabilities assigned in the certainty columns
        :return: the examples ranked according to the Kullback-Leibler divergence of each learner to the consensus prediction (we choose the example with the highest divergence computed)
        Resource for KL divergence: https://towardsdatascience.com/kl-divergence-python-example-b87069e4b810
        Assuming the same example and prediction used for the consensus entropy (self.__getConsensusEntropyRankedExamples), we can compute the probability of each prediction and get again: 
        [[0.5   , 0.4333, 0.0333]
         [0.3666, 0.4333, 0.2   ]
         [0.5   , 0.3   , 0.2   ]
         [0.2   , 0.1   , 0.7   ]
         [0.0666, 0.2666, 0.6666]]
         
        For ex.2 we have [0.3666, 0.4333, 0.2   ] computed from the votes: C1: [0.3(a), 0.7, 0.0], C2: [0.4, 0.6(b), 0.0], C3: [0.4, 0.0, 0.6(c)]
        The distance is compute by comparing each individual vote with the means of all votes, i.e.:
        print(spy.entropy([0.3, 0.7, 0.0], qk=[0.3666, 0.4333, 0.2])) = 0.2755083411831241  (C1)
        print(spy.entropy([0.4, 0.6, 0.0], qk=[0.3666, 0.4333, 0.2])) = 0.23007687556278716 (C2)
        print(spy.entropy([0.4, 0.0, 0.6], qk=[0.3666, 0.4333, 0.2])) = 0.6939446528814834  (C3)

        This should give [0.27549995,  0.23005799,  0.69397192] for ex.2, for all examples:
             C1            C2           C3
        [[0.32631363,  0.80234647,  0.15685227],
         [0.27549995,  0.23005799,  0.69397192], <-- ex.2
         [0.69314718,  0.34053564,  0.22380466],
         [0.04613903,  0.02914912,  0.15686827],
         [0.70556709,  0.40546511,  0.17201121]]
        Then we just take the max of each list to reorder the examples:
        [0.80234647, 0.69397192, 0.69314718, 0.15686827, 0.70556709], giving examples: 1,5,2,3,4
        '''
        # First I get the mean of the certainty of all classifiers for an example to be label 0
        meanL0 = predictions.filter(regex='^certainty_label_0_C[0-9]+$').mean(axis=1)
        # then I use the certainty for the labels assigned by each classifier to compute the KL distance to the consensus vote
        # and add it in the respective column
        for ind, _ in enumerate(self.committee.getClassifiers()):
            predictions['KLDistance_C'+str(ind)] = spy.entropy([predictions['certainty_label_0_C'+str(ind)], predictions['certainty_label_1_C'+str(ind)]], qk=[meanL0, (1-meanL0)])
        # then select the max KLDistance for each example
        predictions['maxKLDistance'] = predictions.filter(regex='^KLDistance_C[0-9]+$').max(axis=1)
        # and reorder the examples based on the max
        predictions.sort_values(by='maxKLDistance', ascending=False, inplace=True)
        predictions.drop(columns=predictions.filter(regex='^KLDistance_C[0-9]+$'), inplace=True)
        predictions.drop(columns='maxKLDistance', inplace=True)
                
        return predictions

class ExpertCommittee(ActiveLearnerInt):
    '''
    Create a committee of experts to run query on unlabel examples
    This committee only handle binary classification
    
    !!!! If the experts are NNs, the code will not work with tensorflow backend, a leak memory will crash the system after some time, switch to theano, but theano is 15 times slower...
    
    '''

    def __init__(self, experts: List[ClassifierInt]):
        '''
        Create the committee with all classifiers given, classifiers do not have to be all supervised but they should implement the service of their interface
        :param: expect a list of fully instantiated list of classifiers implementing the classifierInt services
        '''
        self.__setExperts__(experts)
        #a list of weights representing the trust in each expert
        trustExperts: List[float] = []
        #I compute the colors for each classifier to have the same colors over the iterations
        self.colors = {}
        for expert in self.getClassifiers():
            self.colors[expert.getName()] = np.random.rand(3,)
        
        self._queryingAlgo: Optional[QueryingAlgorithm] = None
    
    def __setExperts__(self, experts: List[ClassifierInt]):
        expertsNames = []
        #check the experts have unique names
        for expert in experts:
            expertsNames.append(expert.getName())
        for expertName in expertsNames:
            assert (expertsNames.count(expertName) == 1), f'Two experts have the same name, they should be uniquely identify, check your code' 
        for expert in experts:
            assert isinstance(expert, ClassifierInt), f"I was expecting an instance of ClassifierInt in the expert committee, got {type(expert)}, an incompatible object, check the code."
            assert len(experts)>=2, f"A committee of experts need more than one expert... Instantiate the object with more than one expert in the list or use an uncertainty sampler if only one classifier is available."
            
        self._experts = experts

    
    def getClassifiers(self) -> List[ClassifierInt]:
        return self._experts


    def setQueryingAlgorithm(self, queryingAlgorithm: ECQueryingAlgorithm):
        '''
        :param: queryingAlgorithm the querying algorithm to be used to select the most informative examples, for an expert committee a disagreement based algorithm
        '''
        assert isinstance(queryingAlgorithm , ECQueryingAlgorithm), f"I was expecting an instance of QueryingAlgorithm, I got {queryingAlgorithm.__class__.__name__}, check the code."
        self._queryingAlgo = queryingAlgorithm
        
        
    def getQueryingAlgorithm(self) -> ECQueryingAlgorithm:
        '''
        :return: the querying algorithm set for the committee, 
        :raise exception: if the querying algorithm has not been set yet
        '''
        assert self._queryingAlgo is not None, "The code is calling for a querying algorithm which has not been instantiated in the expert committee yet."
        return self._queryingAlgo
    
    def clear(self):
        """
        There is probably a cleaner way to implement that, but it's working for now...
        """
        #K.clear_session() to be commented if Theano is running in backend
        K.clear_session()
        #checking if the expert needs to reload any resources
        for expert in self.getClassifiers():
            load = getattr(expert, 'load', None)  
            if load is not None:
                expert.load()
            #else: do nothing
             
    def train(self, pool: Pool):
        '''
        :param: pool a pool which contains the most recent annotated examples to run the next training iteration
        :return: a model updated
        '''
        assert pool.trainEx['label'].nunique()==2, f"Seems that you are trying to run the expert committee on a multi-class problem, it is not supported yet, use uncertainty sampling."
        self.__runExpertsInSequence(pool)
#         self.__runExpertsInParallel(pool, evaluate)
        log.info("All experts have finished their training on the data.")
    
        
    def __runExpertsInSequence(self, pool: Pool):
        #run the classifiers in sequence
        log.info("Training the experts in sequence...")
        for expert in self.getClassifiers(): 
            expert.train(pool)


    def __runExpertsInParallel(self, pool: Pool):
        log.info("Training the experts in parallel...")
        path_dump = '/tmp/{}.joblib'
        
        def run_proc(clf: ClassifierInt, p_l: Pool, evl: bool) -> None:
            clf.train(p_l, evl)
            dst_path = path_dump.format(clf.getName())
            joblib.dump(clf, dst_path)
            return
  
        processes: List[Process] = []
        for expert in self.getClassifiers():
            proc = Process(target=run_proc, args=(expert, pool))
            proc.daemon = True
            proc.start()
            processes.append(proc)
            
        for proc in processes:
            proc.join()

        for idx_clf, expert in enumerate(self._experts):
            src_path = path_dump.format(expert.getName())
            self._experts[idx_clf] = joblib.load(src_path)
            
        
    def getMostInformativeExamples(self, pool: Pool, ExQueriedBatchSize: int) -> DataFrame:
        '''
        :param: a pool with all unlabeled data
        :return: a set of examples that should be the most informative to annotate,
                 the examples have a label and cetainty columns set to NaN
        '''
        for ind, exp in enumerate(self.getClassifiers()):
            #take a copy since all experts will make their predictions on the same set of examples
            cpUnlabelExs = pool.unlabEx.copy()
            cpUnlabelExs = exp.classify(cpUnlabelExs)
            assert set(['certainty_label_0', 'certainty_label_1', 'prediction']).issubset(cpUnlabelExs.columns), f"The classifier {exp.getName()} did not return a dtatframe with the expected columns: 'prediction', 'certainty_label_0', 'certainty_label_1', check the code"  
            cpUnlabelExs.rename(columns={"prediction":"prediction_"+str(ind), "certainty_label_0":"certainty_label_0_C"+str(ind), "certainty_label_1":"certainty_label_1_C"+str(ind)}, inplace=True)
            if ind==0:
                predictions = cpUnlabelExs                
            else:
                predictions = predictions.join(cpUnlabelExs[["prediction_"+str(ind), "certainty_label_0_C"+str(ind), "certainty_label_1_C"+str(ind)]], on='docID')
        
        #we have the predictions for each expert, now we select the most informative based on the querying algorithm
        log.info(f"I run the querying algorithm {self.getQueryingAlgorithm().getAlgorithmName()}")
        miExamples = self.getQueryingAlgorithm().getExamplesToQuery(predictions)
        miExamples = DataFrame(miExamples[0:ExQueriedBatchSize])

        #now I can remove the useless columns
        miExamples.drop(columns=miExamples.filter(regex='^prediction_[0-9]+$'), inplace=True)
        miExamples.drop(columns=miExamples.filter(regex='^certainty_label_[0-9]+_C[0-9]+$'), inplace=True)
        # and add the column empty for annotation
        miExamples['label'] = np.NaN
        
        return miExamples


    def classify(self, examples: DataFrame) -> DataFrame:
        '''
        :param: classify the examples given
        :return: a copy of the examples updated with 2 columns label and certainty:
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        return self.__MeanAgregate(examples.copy(deep=True))
    
    def __MeanAgregate(self, examples: DataFrame) -> DataFrame:
        '''
        :param: predictions made by the different experts
        :return: the final decision taken as the mean of each probability assigned by the experts (no weights)
        the dataframe returned multile columns 
          - prediction: the best prediction made by the classifier, ie, the label with the highest certainty 
          - certainty_label_n: a column where the classifier assign the probability for the label n for each example
        '''
        predictions = DataFrame() 
        for expert in self.getClassifiers():
            pred = expert.classify(examples)
            assert 'prediction' in pred.columns, "One classifier did not return a label column has it should, change the code."
            #TODO: I'm coding for binary classification, (in rush as usual...), extend to multinomial 
            assert "certainty_label_0" in pred.columns,  "One classifier did not return a certainty_label_0 column has it should, change the code." 
            predictions[expert.getName()+"_certainty_label_0"] = pred["certainty_label_0"]
        #now I can compute the mean for the example to be labeled 0
        examples['certainty_label_0'] = predictions.mean(axis=1)
        examples['certainty_label_1'] = 1-examples['certainty_label_0']
        assert len(set(examples.filter(regex=("certainty_label_[0-9]+")).columns))==2, "The expert commitee is used to compute a multi-class classification whereas it is not yet implemented to do so, just binary classification"
        def __predictClass__(mean):
            if mean>=0.5: return 0
            return 1
        examples['prediction'] = examples['certainty_label_0'].apply(lambda mean0: __predictClass__(mean0))
        return examples
    
    def evaluate(self, examplesPredicted: DataFrame, examplesTruth: DataFrame) -> Scores:
        '''
        evaluate the performance of the learner on the set of examples given
        :param: examplesPredicted, the predictions made by the classifier, the df should have a column 'prediction' where the predictions are store
        :param: examplesTruth, the true values of the examples also store in a column label, both set of examples should have the same docIDs
        :return: P,R,F1 scores
        '''
        assert len(examplesPredicted.index.intersection(examplesTruth.index))==len(examplesPredicted.index), f"There is no index mapping between the predicted and the truth set of examples, check the code."
        assert 'prediction' in examplesPredicted.columns and 'label' in examplesTruth.columns, "The examples predicted or gold truth do not have the expected label columns, check code."
        
        df = DataFrame({'docIDTruth':examplesTruth.index, 'truths':examplesTruth['label'], 'docIDPredicted':examplesPredicted.index, 'preds':examplesPredicted['prediction']})
        assert(np.array_equal(df['docIDTruth'], df['docIDPredicted'])), "I have differences between the lists of docIDs from the truth and the predicted which should be identical lists, check the code..."        
        
        cf = confusion_matrix(list(df['truths']), list(df['preds']))        
        cr = classification_report(list(df['truths']), list(df['preds']),digits=4)
        acc=accuracy_score(list(df['truths']), list(df['preds']))
        if df['truths'].nunique()>2 or df['preds'].nunique()>2:
            #We are in a multinomial classification
            prec = precision_score(list(df['truths']), list(df['preds']), average='macro')
            rec = recall_score(list(df['truths']), list(df['preds']), average='macro')
            f1 = f1_score(list(df['truths']), list(df['preds']), average='macro')
        else:
            #binary
            prec = precision_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            rec = recall_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
            f1 = f1_score(list(df['truths']), list(df['preds']), pos_label=1, average='binary')
        
        return Scores(confusionMatrix=cf, classificationReport=cr, F1positive=f1, PrecisionPositive=prec, RecallPositive=rec, Accuracy=acc)

    def drawPerfOnEvaluation(self, path:str):
        log.error(f"TODO: I'm not drawing the performance of the committee on the evaluation set, I need to think about the histories...")
    def drawPerformance(self, performance: Performance, path:str):
        drawer = PerformanceDrawer()
        drawer.drawPerformance(performance, path)
        
#     def drawImprovement(self):
#         '''
#         draw the improvement observed iteration after iteration of active learning
#         :param: iteration for which the improvement will be drawn
#         '''
#         df = DataFrame()
#         iterationsX = sorted(self.getClassfiers()[0].getPerformanceHistory().keys())
#         assert len(iterationsX)>0, "I am asked to draw an history which is empty, check the code."
#         
#         df['x'] = iterationsX 
#         for expert in self.getClassfiers():
#             f1s = []
#             for iterationx in iterationsX:
#                 f1s.append(expert.getPerformanceHistory()[iterationx]['f1'])
#             df[expert.getName()] = f1s
#                     
#         pp = PdfPages(f'/tmp/classifiersImprovement_{str(iterationsX[-1])}.pdf')
#         
#         fig = plt.figure()    
#         for expert in self.getClassfiers():
#             plt.plot('x', expert.getName(), data=df, marker='', color=self.colors[expert.getName()], linewidth=2)
#             plt.legend()
#         pp.savefig(fig)        
#         pp.close()

#I moved the drawing to the drawer

#     def drawImprovement(self,properties):
#         '''
#         draw the improvement observed iteration after iteration of active learning
#         :param: iteration for which the improvement will be drawn
#         '''
#         df = DataFrame()
#         iterationsX = sorted(self.getClassfiers()[0].getPerformanceHistory().keys())
#         assert len(iterationsX)>0, "I am asked to draw an history which is empty, check the code."
#         
#         df['x'] = iterationsX 
#         for expert in self.getClassfiers():
#             f1s = []
#             for iterationx in iterationsX:
#                 f1s.append(expert.getPerformanceHistory()[iterationx]['f1'])
#             df[expert.getName()] = f1s
#                     
#         if properties.name=='Davy':
#             pp = PdfPages(f'/tmp/classifiersImprovement_{str(iterationsX[-1])}.pdf')
#             fig = plt.figure()    
#             for expert in self.getClassfiers():
#                 plt.plot('x', expert.getName(), data=df, marker='', color=self.colors[expert.getName()], linewidth=2)
#                 plt.legend()
#             pp.savefig(fig)        
#             pp.close()
#         else:
#             # pp = PdfPages(f'/home/gesy/MNA/logs/classifiersImprovement_{str(iterationsX[-1])}.pdf')
#             pass

