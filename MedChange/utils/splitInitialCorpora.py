'''
Created on Dec 12, 2019

@author: dweissen
'''

import pandas as pd
import re


def splitWebMDReviews():
    annoReviews = pd.read_csv("/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSCClassification_12972_1211019_temp.csv", sep='\t', dtype=str)
    assert({'SOURCE_FILE', 'TEXT', 'Medication Status Change \n1=yes\n0=no', 'DRUG'}.issubset(set(annoReviews.columns))), "Apparently one column is missing in the file loaded, check the data."
    assert len(annoReviews)==12972, f"I was expecting 12972 examples, got {len(annoReviews)}, check the data"
   
    #remove all new lines in text...
    annoReviews['TEXT'] = annoReviews['TEXT'].apply(lambda tweetTxt: re.sub(r'\n', '   ', str(tweetTxt)))
    
    #split into train and test
    test_val = annoReviews.sample(frac=0.2, random_state=33)
    train = annoReviews.drop(test_val.index, inplace = False)
    test = test_val.sample(frac=0.5, random_state=33)
    val = test_val.drop(test.index, inplace = False)
    assert ((len(train)+len(test)+len(val))==len(annoReviews)), "Error in the code, train {}+test {} should equal the input df {}.".format(len(train), len(test), len(annoReviews))
    test.to_csv('/tmp/MSCClassification_12972_1211019_temp_TEST.csv', sep='\t', index=False)
    train.to_csv('/tmp/MSCClassification_12972_1211019_temp_TRAIN.csv', sep='\t', index=False)
    val.to_csv('/tmp/MSCClassification_12972_1211019_temp_VAL.csv', sep='\t', index=False)
        
def splitMSCTweets():
    annoTW = pd.read_csv('/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSC_Twitter_9835_011720.csv', sep='\t', dtype=str)
    assert({'tweet_id', 'text', 'MSC: 0 = no MSC, 1= MSC', 'Drug Name Extracted'}.issubset(set(annoTW.columns))), "Apparently one column is missing in the file loaded, check the data."
    assert len(annoTW)==9835, f"I was expecting 9835 examples, got {len(annoTW)}, check the data"
    annoTW.rename(columns={'MSC: 0 = no MSC, 1= MSC':'label'}, inplace=True)
    annoTW.drop_duplicates('tweet_id', keep='first', inplace=True)
    assert len(annoTW)==9831, f"I was expecting 9831 examples after removing the 4 duplicates statins that were in Kusuri corpus, got {len(annoTW)}, check the data"    
    
    #remove all new lines in text...
    annoTW['text'] = annoTW['text'].apply(lambda tweetTxt: re.sub(r'\n', '   ', str(tweetTxt)))
    annoTW.set_index(['tweet_id'], verify_integrity=True)
    #split into train test val and unlabeled
    # first split pos/neg
    posTwts =  annoTW[annoTW['label']=='1']
    negTwts =  annoTW[annoTW['label']=='0']
    
    print(f'nbr of positive examples: {len(posTwts)}')
    print(f'nbr of negative examples: {len(negTwts)}')
    
    posTwtsValTest = posTwts.sample(frac=0.4, random_state=33)
    negTwtsValTest = negTwts.sample(frac=0.4, random_state=33)
    
    posTwtsTrain = posTwts.drop(posTwtsValTest.index, inplace=False)
    negTwtsTrain = negTwts.drop(negTwtsValTest.index, inplace=False)
    
    print(f'nbr of positive training examples: {len(posTwtsTrain)}')
    print(f'nbr of negative training examples: {len(negTwtsTrain)}')
    
    posTwtsVal = posTwtsValTest.sample(frac=0.4, random_state=33)
    negTwtsVal = negTwtsValTest.sample(frac=0.4, random_state=33)
    
    print(f'nbr of positive validation examples: {len(posTwtsVal)}')
    print(f'nbr of negative validation examples: {len(negTwtsVal)}')
    
    posTwtsTest = posTwtsValTest.drop(posTwtsVal.index, inplace=False)
    negTwtsTest = negTwtsValTest.drop(negTwtsVal.index, inplace=False)
    
    print(f'nbr of positive test examples: {len(posTwtsTest)}')
    print(f'nbr of negative test examples: {len(negTwtsTest)}')
    
    assert (len(posTwts) == (len(posTwtsTrain)+len(posTwtsVal)+len(posTwtsTest))), 'I did not find the same number of positive tweets before and after the split, check the code'
    assert (len(negTwts) == (len(negTwtsTrain)+len(negTwtsVal)+len(negTwtsTest))), 'I did not find the same number of negative tweets before and after the split, check the code'

    train = pd.concat([posTwtsTrain, negTwtsTrain], ignore_index=False)
    train = train.sample(frac=1.0)
    val = pd.concat([posTwtsVal, negTwtsVal], ignore_index=False)
    val = val.sample(frac=1.0)
    test = pd.concat([posTwtsTest, negTwtsTest], ignore_index=False)
    test = test.sample(frac=1.0)
    
    assert len(train)==(518+5380), 'Did not found the number of training examples expected after the split, check the code'
    assert len(val)==(138+1434), 'Did not found the number of validation examples expected after the split, check the code'
    assert len(test)==(208+2152), 'Did not found the number of test examples expected after the split, check the code'
    
    train.to_csv('/tmp/MSC_Twitter_9835_011720_TRAIN.tsv', sep='\t', index=False)
    val.to_csv('/tmp/MSC_Twitter_9835_011720_VAL.tsv', sep='\t', index=False)
    test.to_csv('/tmp/MSC_Twitter_9835_011720_TEST.tsv', sep='\t', index=False)

if __name__ == '__main__':
    splitMSCTweets()

    