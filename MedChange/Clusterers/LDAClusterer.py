'''
Created on Aug 4, 2020
Example code
https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/

@author: dweissen
'''

import logging as lg
log = lg.getLogger('LDAClusterer')
from Clusterers.ClustererInt import ClustererInt
from Properties.PropertiesInt import PropertiesInt

from typing import List
from pandas.core.frame import DataFrame
from nltk.tokenize import TweetTokenizer

import os
import csv
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim import models as gModels
from gensim.test.utils import datapath

# spacy for lemmatization
import spacy
# Plotting tools
import pyLDAvis
import pyLDAvis.gensim 
import matplotlib.pyplot as plt

class LDAClusterer(ClustererInt):
    '''
    A basic LDA clustering algorithm to test if we can find reasons in texts where users are mentioning their changes in medication
    '''

    def __init__(self, properties:PropertiesInt):
        '''
        Constructor
        '''
        self._properties= properties
        
        # we need to know from a human inspection of the topics which topics are associated with medication change and medication stop
        # should be a list 
        self.MCTopics = None
        # the threshold is used to determine when the difference of coherence between different lda models with different number of topics start to plateau
        # given a threshold of 2 if the model A has a coherence of 54, the model B should have a coherence > 56, otherwise model A is picked up has the difference between the coherence is not big enough and the system is probably starting to plateau
        self.plateauThreshold = 0.015
        #number of iterations to build the topics
        self.nbrIter = 100
        #to train the topic model we are trying multiple models, here are the parameters for the loop
        #limit is the maximum number of topics for a model
        self.limit=15
        self.start=5
        self.step=1
        # an existing model
        self.model = None
        
    def __buildStopWordsList(self):
        '''
        Build and compile topic modeler
        '''
        self.stopwordslist=[]
        with open(self._properties.stopWordPath, encoding='ISO-8859-1') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                self.stopwordslist.append(row[0].strip())


    def __tokenize(self, texts):
        tokenizer= TweetTokenizer(strip_handles=True, reduce_len = True, preserve_case=False)
        tokenized_texts=[]

        for text in texts:  
            try:
                tokenized_text=tokenizer.tokenize(text)
            except:
                pass
            tokenized_texts.append(tokenized_text)

        for i in range(len(tokenized_texts)):
            tokenized_texts[i]=[word for word in tokenized_texts[i] if word not in self.stopwordslist]
        return tokenized_texts


    def __make_bigrams_and_trigrams(self,tokenized_text):
        bigram = gensim.models.Phrases(tokenized_text, min_count=5, threshold=50) # higher threshold fewer phrases.
        trigram = gensim.models.Phrases(bigram[tokenized_text], threshold=50)  
        bigram_mod = gensim.models.phrases.Phraser(bigram)
        trigram_mod = gensim.models.phrases.Phraser(trigram)
        return [trigram_mod[bigram_mod[doc]] for doc in tokenized_text]


    def __lemmatization(self,tokenized_text, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        texts_out = []
        try:
            nlp = spacy.load('en', disable=['parser', 'ner'])
        except IOError as io:
            log.error("If this is a E050 error complaining about can't find model en, we need to install the model for spacy: python -m spacy download en")
            raise io
        for sent in tokenized_text:
            doc = nlp(" ".join(sent)) 
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out
    
    def __train(self, docs: DataFrame):
        """
        Check if an existing model has been train, if not it trains it otherwise it reads the model from the path indicated in properties
        :param docs: dataframe containing one column 'text' which contains the raw texts of the corpus
        """
        if self.model is None:
            if os.path.exists(self._properties.classifierModelsPath+f"/LDAClusterer.md"):
                self.__buildStopWordsList()
                log.info(f"Found an existing LDA model, reading @{self._properties.classifierModelsPath}/LDAClusterer.md, I am using this model.")
                self.model = gModels.LdaModel.load(self._properties.classifierModelsPath+f"/LDAClusterer.md")
            else:
                log.info(f"No LDA model is given, build it from scratch.")
                self.__buildStopWordsList()
                self.model = self.__builLDAModel(docs)        
                log.info(f"Best lda model computed and written @{self._properties.classifierModelsPath}/LDAClusterer.md")
                self.model.save(self._properties.classifierModelsPath+f"/LDAClusterer.md")
        else:
            log.debug("An existing model has already been loaded, no more training.")
            
    def __builLDAModel(self, docs: DataFrame):
        """
        Build a model from the texts given
        :param docs: dataframe containing one column 'text' which contains the raw texts of the corpus
        """
        log.info(f"I received {len(docs)} examples for finding topics.")
        rawText=docs['text']
        # Tokenize texts
        tokenized_text=self.__tokenize(rawText)
        # Find bigrams and trigrams
        grammed_text=self.__make_bigrams_and_trigrams(tokenized_text)
        # Lemmantize texts
        lemmatized_text=self.__lemmatization(grammed_text)
        # Create dictionary
        id2word = corpora.Dictionary(lemmatized_text)
        # Term document frequency
        corpus_text = [id2word.doc2bow(text) for text in lemmatized_text]    
        # find the best number of topics based on coherence scores
        lda_model = self.__findBestNbTopics(dictionary = id2word, corpus = corpus_text, texts = lemmatized_text,  limit=self.limit, start=self.start, step=self.step)
        # pprint(lda_model.print_topics())
        # Compute perplexity: a measure of how good the model is. lower the better.
        #log.info('\nPerplexity: '+str(lda_model.log_perplexity(corpus_text)))

        vis = pyLDAvis.gensim.prepare(lda_model, corpus_text, id2word)
        html = pyLDAvis.prepared_data_to_html(vis, template_type="simple", visid="pyldavis-in-org-mode")
          
        log.info("I am writing the pyLDAvis.html in the tmp directory for visualisation.")
        with open("/tmp/pyLDAvis.html", 'w') as davisOut:
            davisOut.write(html)
            davisOut.close()
        return lda_model
    
    def __findBestNbTopics(self, dictionary, corpus, texts, limit:int, start:int, step:int) -> List:
        """
        Compute c_v coherence for various number of topics
    
        Parameters:
        ----------
        dictionary : Gensim dictionary
        corpus : Gensim corpus
        texts : List of input texts
        limit : Max num of topics
    
        Returns:
        -------
        Write all LDA models in the tmp directory
        return the 'best' LDA models found after checking for a plateau or a decrease of performances in the coherence of the topics 
        """
        assert (limit>start), f"To find a topic model I need to run at least one iteration, so the limit given: {limit}, should be higher than the start value: {start}"
        coherence_values = []
        model_list = []
        for num_topics in range(start, limit, step):
            lda_model = gensim.models.ldamodel.LdaModel(corpus = corpus, id2word = dictionary, num_topics=num_topics, random_state=100, update_every=1, chunksize=600, passes=self.nbrIter, alpha='auto', per_word_topics=True)
            model_list.append(lda_model)
            coherencemodel = CoherenceModel(model=lda_model, texts=texts, dictionary=dictionary, coherence='c_v')
            coherence_values.append(coherencemodel.get_coherence())

        x = range(start, limit, step)
        plt.plot(x, coherence_values)
        plt.xlabel("Num Topics")
        plt.ylabel("Coherence score")
        plt.legend(("coherence_values"), loc='best')
        plt.savefig("/tmp/topicModCoherences.jpeg")
        # write all models just in case I am not able to identify the best one automatically
        for ind, model in enumerate(model_list):
            model.save(f"/tmp/LDAClusterer_{ind}.md")
        
        #return self.__findPlateauCoherence(coherence_values, model_list)    
        return self.__findMaxCoherence(coherence_values, model_list)
 
    def __findMaxCoherence(self, coherence_values, model_list):
        modelSelected = model_list[0]
        maxCoherence = coherence_values[0]
        for ind in range(len(coherence_values)):
            if maxCoherence <= coherence_values[ind]:#replace <= if you want to keep the smaller set of topics
                log.info(f"I found better performance {coherence_values[ind]}, I keep the model having #{model_list[ind].num_topics} topics.")
                maxCoherence = coherence_values[ind]
                modelSelected = model_list[ind]
        return modelSelected
    
    def cluster(self, examples: DataFrame)->DataFrame:
        """
        Find the best clusters for the examples given using LDA topic modeling
        """
        self.__train(examples)
        return None