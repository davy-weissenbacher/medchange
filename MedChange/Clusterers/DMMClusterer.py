'''
Created on Aug 6, 2020

@author: dweissen
'''

 
import logging as lg
log = lg.getLogger('DMMClusterer')
from pandas.core.series import Series
from Clusterers.ClustererInt import ClustererInt
from Properties.PropertiesInt import PropertiesInt
from Clusterers.MovieGroupProcess import MovieGroupProcess

import re
import numpy as np
# spacy for lemmatization
import spacy
#from topic_allocation import top_words, topic_attribution
from nltk.tokenize import TweetTokenizer
import pickle
import os
import csv
import gensim
import gensim.corpora as corpora
import pyLDAvis.gensim 
from pandas.core.frame import DataFrame
from typing import List

class DMMClusterer(ClustererInt):
    '''
    Use he Gibbs sampling algorithm for a Dirichlet Mixture Model of Yin and Wang 2014 to find topics in short texts
    Re-use the implementation of https://github.com/rwalk/gsdmm
    '''

    def __init__(self, properties:PropertiesInt):
        '''
        Constructor
        '''
        self._properties= properties
        
        self.K=20
        self.alpha=0.1
        self.beta=0.1
        self.n_iters=30
        
    
    def __buildStopWordsList(self):
        '''
        Build and compile topic modeler
        '''
        self.stopwordslist=[]
        with open(self._properties.stopWordPath, encoding='ISO-8859-1') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                self.stopwordslist.append(row[0].strip())
                
    
    def __preprocessTweets(self, texts: Series) -> Series:
        """
        Preprocess the tweets before tokenizing them:
        lowercased, removing the urls, remove numbers, remove hashtags
        usernames and elongated will be handle by the tokenizer
        """
        #lowercase the text of the tweets
        texts =  texts.apply(str.lower)
        #remove the urls
        texts = texts.apply(lambda tweetTxt: re.sub(r'https?://([A-Za-z.0-9/])*', '', tweetTxt))
        #delete the digits
        def replaceNumbers(text, replacementString):
            normalizeTwTxt = []
            for wd in text.split(' '):
                if not wd.startswith('#'):#we ignore the hashtags, numbers can be important
                    wd = re.sub(r'[-+]?[.\d]*[\d]+[:,.\d]*', replacementString, wd)
                    normalizeTwTxt.append(' ')
                    normalizeTwTxt.append(wd)
                else:
                    normalizeTwTxt.append(' ')
                    normalizeTwTxt.append(wd)
            return ''.join(normalizeTwTxt)
        
        texts = texts.apply(lambda tweetTxt: replaceNumbers(tweetTxt, ''))
        #delete the hashtags
        texts = texts.apply(lambda tweetTxt: re.sub(r'#(\S+)', '', tweetTxt))
        #user names will be handle by the tokenizer
        #the elongated too
        return texts
    
    
    def __tokenize(self, texts):
        tokenizer= TweetTokenizer(strip_handles=True, reduce_len = True, preserve_case=False)
        tokenized_texts=[]

        for text in texts:  
            try:
                tokenized_text=tokenizer.tokenize(text)
            except:
                pass
            tokenized_texts.append(tokenized_text)

        return tokenized_texts
    def __lemmatization(self,tokenized_text, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        texts_out = []
        try:
            nlp = spacy.load('en', disable=['parser', 'ner'])
        except IOError as io:
            log.error("If this is a E050 error complaining about can't find model en, we need to install the model for spacy: python -m spacy download en")
            raise io
        for sent in tokenized_text:
            doc = nlp(" ".join(sent)) 
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out
    def __stopwords(self,tokenized_text):
        for i in range(len(tokenized_text)):
            tokenized_text[i]=[word for word in tokenized_text[i] if word not in self.stopwordslist]
        return tokenized_text
        
    
    def __top_words(self, cluster_word_distributions:List, top_index:List, number_words=5):
        """
        display the top words for each cluster discovered
        :param cluster_word_distributions: the words distribution per topics, computed by the model mgp
        :param top_index: the n top topics indexes to display the top words
        :param number_words: number of top words to display for these topics
        """
        # Show the top 5 words by cluster, it helps to make the topic_dict below
        #self.__top_words(mgp.cluster_word_distribution, top_index, 5)
        for index in top_index:
            top_words = [(k,v) for k, v in sorted(cluster_word_distributions[index].items(), key=lambda item: item[1], reverse=True)]
            log.info(f"Cluster n`{index} top words: {top_words[:number_words]}")
    
    
    def __builDMMModel(self, docs: DataFrame):
        """
        Build a model from the texts given
        :param docs: dataframe containing one column 'text' which contains the raw texts of the corpus
        """
        log.info(f"I received {len(docs)} examples for finding topics.")
        log.info(f"preprocess tweets...")
        docs['text'] = self.__preprocessTweets(docs['text'])
        rawText=docs['text']
        # Tokenize texts
        log.info("tokenizing...")
        tokenized_text=self.__tokenize(rawText)
        # Lemmantize texts
        log.info("lemmatizing...")
        tokenized_text=self.__lemmatization(tokenized_text)
        #apply stopwords
        tokenized_text=self.__stopwords(tokenized_text)
        #to display with pyLDAvis, need to create a LDA model first, I don't have the time right now...
#         # Create dictionary
#         log.info("term doc frequency...")
#         id2word = corpora.Dictionary(tokenized_text)
#         # Term document frequency
#         corpus_text = [id2word.doc2bow(text) for text in tokenized_text]

        vocab = set(x for tkDoc in tokenized_text for x in tkDoc)
        n_terms = len(vocab)
        log.debug(f"Voc size: {n_terms}")
        log.debug(f"Number of documents: {len(tokenized_text)}")
        
        # Init of the Gibbs Sampling Dirichlet Mixture Model algorithm
        mgp = MovieGroupProcess(self.K, self.alpha, self.beta, self.n_iters)
        log.info("Start computing the model...")
        clusterPerDoc = mgp.fit(tokenized_text, n_terms)
        
        log.info(clusterPerDoc)
        
        doc_count = np.array(mgp.cluster_doc_count)
        log.info(f'Number of documents per topics :{doc_count}')
        # Topics sorted by document inside
        top_index = doc_count.argsort()[-20:][::-1]
        log.info(f'Most important clusters (by number of docs inside):{top_index}')

        self.__top_words(mgp.cluster_word_distribution, top_index, number_words=50)
        
#         vis = pyLDAvis.gensim.prepare(clusterPerDoc, corpus_text, id2word)
#         html = pyLDAvis.prepared_data_to_html(vis, template_type="simple", visid="pyldavis-in-org-mode")
#         log.info("I am writing the pyLDAvis.html in the tmp directory for visualisation.")
#         with open("/tmp/pyLDAvis.html", 'w') as davisOut:
#             davisOut.write(html)
#             davisOut.close()
        
        return mgp, clusterPerDoc
        
        
    def cluster(self, examples: DataFrame)->DataFrame:
        """
        Find the best clusters for the examples given using LDA topic modeling
        :return: the df with the doc associated with a topic ID in column 'TopicPredicted'
        """
        log.info(f"No DMM model is given, build it from scratch.")
        self.__buildStopWordsList()
        self.model, clusterLabelsPerDocs = self.__builDMMModel(examples)
        examples['TopicPredicted'] = clusterLabelsPerDocs
        examples.to_csv(f"{self._properties.tmpPath}/DMMClustererOutput.tsv", sep='\t')       
        log.info(f"Best DMM model computed and written @{self._properties.classifierModelsPath}/DMMClusterer.md")
        with open(self._properties.classifierModelsPath+f"/DMMClusterer.md", "wb") as f:
            pickle.dump(self.model, f)
            f.close()
        return clusterLabelsPerDocs