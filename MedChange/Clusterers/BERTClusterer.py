'''
Created on Aug 7, 2020

@author: dweissen
'''
import logging as lg
log = lg.getLogger('BERTClusterer')
from pandas.core.series import Series

from Clusterers.ClustererInt import ClustererInt
from Properties.PropertiesInt import PropertiesInt

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans, AgglomerativeClustering
from flair.data import Sentence
from flair.embeddings import TransformerDocumentEmbeddings
from flair.embeddings import SentenceTransformerDocumentEmbeddings

import matplotlib.pyplot as plt
plt.style.use('ggplot')
import plotly.graph_objects as go

from typing import List
import re
from os import path
import numpy as np
import pickle as pkl
import pandas as pd
from pandas.core.frame import DataFrame


class BERTClusterer(ClustererInt):
    '''
    Use the embedding of BERT and a KNN clustering algorithms to discover the clusters of reason
    '''


    def __init__(self, properties:PropertiesInt, clusteringAlgorithm='KMeans'):
        '''
        Constructor
        '''
        self._properties= properties
        # init embedding
        self.embedding = SentenceTransformerDocumentEmbeddings('bert-base-nli-mean-tokens')
        algorithmsSupported = ['KMeans', 'AggloClust']
        if clusteringAlgorithm in algorithmsSupported:
            self.clustAlg = clusteringAlgorithm
        else:
            raise Exception(f"Unknown clustering algorithm: {clusteringAlgorithm}, are supported: {algorithmsSupported}")
        
        
    def __preprocessTweets(self, texts: Series) -> Series:
        """
        Preprocess the tweets before tokenizing them:
        lowercased, removing the urls, remove hashtags
        usernames and elongated will be handle by the tokenizer
        """
        #lowercase the text of the tweets
        texts =  texts.apply(str.lower)
        #delete user names
        texts = texts.apply(lambda tweetTxt: re.sub(r'@(\w)+', '', tweetTxt))
        #remove the urls
        texts = texts.apply(lambda tweetTxt: re.sub(r'https?://([A-Za-z.0-9/])*', '', tweetTxt))
        #delete the hashtags
        texts = texts.apply(lambda tweetTxt: re.sub(r'#(\S+)', '', tweetTxt))
        #log.debug("start removing elonged words, takes time...")
        def getElongsIndexes(wd):            
            elongIndexes = {}
            index = 0
            while index<len(wd):
                if (index+2)<len(wd):
                    #elong are should be with letters only
                    if wd[index].isalpha() and (wd[index]==wd[index+1]) and (wd[index+1]==wd[index+2]):
                        #we have an elonged word
                        startElongIndex = index
                        car = wd[index]
                        endIndexElong = index
                        while index < len(wd) and (car==wd[index]):
                            endIndexElong = index
                            index += 1
                        if index >= len(wd):
                            endIndexElong = len(wd)-1
                        assert startElongIndex < endIndexElong, "start index elonged word should be lower than end index of the elong word, check the code..."
                        #else: endIndexElong is at the right position
                        elongIndexes[startElongIndex] = endIndexElong
                        index = endIndexElong
                    else:#nothing to worried about (despite misspellings...)
                        index += 1
                else:
                    break        
            return elongIndexes
        def removeElongSequences(wd, elongIndexes):
            """
            :TODO: check in a dictionary if the word exist
            As a shortcut (I'm short in time) I cut all elong to the first letter
            """          
            wdNormalized = []
            index = 0  
            while index < len(wd):
                if index in elongIndexes: # I found an elong sequence
                    wdNormalized.append(wd[index])
                    index = elongIndexes[index]+1# I move to the end of the sequence ignoring the repetition
                else:
                    wdNormalized.append(wd[index])
                    index += 1
            wdNormalized = ''.join(wdNormalized)
            #log.debug("Word [{}] has been shorten to [{}]".format(wd, wdNormalized))
            return wdNormalized
        def deleteCharacterRepetitions(text):
            """
            Short the elonged words deletes the additional letters 
            - from more than 3 to 1 (most cases): loooool -> lol
            - from more than 3 to 2 if the word can be found in a dictionary, even if an ambiguity remains ex gooood -> good (could have been god)
            :TODO: check in a dictionary if the word exist
            As a shortcut (I'm short in time) I cut all elongs to the first letter
            """
            if len(text)<=2:
                return text
            normalizeTwTxt = []
            for wd in text.split(' '):
                if not wd.startswith('#'):#we ignore the hashtags has they concatenate words they have elong by default...
                    elongIndexes = getElongsIndexes(wd)
                    if elongIndexes:
                        wd = removeElongSequences(wd, elongIndexes)
                        normalizeTwTxt.append(' ')
                        normalizeTwTxt.append(wd)
                    else: 
                        normalizeTwTxt.append(' ')
                        normalizeTwTxt.append(wd)
                else:
                    normalizeTwTxt.append(' ')
                    normalizeTwTxt.append(wd)
            return ''.join(normalizeTwTxt)
        texts = texts.apply(lambda tweetTxt: deleteCharacterRepetitions(tweetTxt))
        return texts
    
    
    def __embed(self, texts:DataFrame):
        """
        Embed the texts and return the result in a new column 'embedding'
        The process is long, I write it in the tmp the first processed, and read from it after
        """
        pathPickle = '/Users/dweissen/Documents/AvirerBig/textsEmbedded.pkl'
        if path.exists(pathPickle):
            log.warning(f"An existing tsv with the texts embedded has been found @{pathPickle}, will be used...")
            textsEmb = pd.read_pickle(pathPickle)
            return textsEmb
        else:
            def embedText(text, cmp):
                # create a sentence
                sentence = Sentence(text)
                # embed the sentence
                self.embedding.embed(sentence)
                cmp[0] = cmp[0]+1
                print(cmp[0])
                return sentence.get_embedding().numpy()
            cmp = [0]
            texts['embedding'] = texts['text'].apply(lambda tweetTxt: embedText(tweetTxt, cmp))
            texts.to_pickle(pathPickle)
            return texts
    
    
    def __cluster(self, texts:DataFrame):
        """
        Cluster the texts already embedded
        """
        if self.clustAlg=='KMeans':
            return self.__KMeans(texts)
        elif self.clustAlg=='AggloClust':
            return self.__AggluClust(texts)
        
    
    def __KMeans(self, texts:DataFrame):
        
        log.info("Start clustering with KMean...")
        md = KMeans(n_clusters=5, random_state=170)
        clusterPerDoc = md.fit_predict(list(texts['embedding']))
        return md, clusterPerDoc
    
    def __AggluClust(self, texts:DataFrame):
        def plot_dendrogram(model, **kwargs):
            from matplotlib import pyplot as plt
            from scipy.cluster.hierarchy import dendrogram
            # Create linkage matrix and then plot the dendrogram
        
            # create the counts of samples under each node
            counts = np.zeros(model.children_.shape[0])
            n_samples = len(model.labels_)
            for i, merge in enumerate(model.children_):
                current_count = 0
                for child_idx in merge:
                    if child_idx < n_samples:
                        current_count += 1  # leaf node
                    else:
                        current_count += counts[child_idx - n_samples]
                counts[i] = current_count
        
            linkage_matrix = np.column_stack([model.children_, model.distances_,
                                              counts]).astype(float)
        
            # Plot the corresponding dendrogram
            dendrogram(linkage_matrix, **kwargs)
            plt.title('Hierarchical Clustering Dendrogram')
            plt.xlabel("Number of points in node (or index of point if no parenthesis).")
            plt.show()
        log.info("Start clustering with AggloCluster...")
        md = AgglomerativeClustering(distance_threshold=0, n_clusters=None)
        clusterPerDoc = md.fit_predict(list(texts['embedding']))
        plot_dendrogram(md, truncate_mode='level', p=3)
        return md, clusterPerDoc

    def __draw(self, texts:DataFrame, md, predictions:List):
            pca = PCA(n_components=2)
            
            print(len(texts))
            X_pca2D = pca.fit_transform(list(texts['embedding']))
            
            pcaDF = pd.DataFrame(data = X_pca2D, columns = ['pc1', 'pc2'])
            
            import plotly.graph_objects as go
            colorsIdx = {0: 'rgb(215,48,39)', 1: 'rgb(215,148,39)', 2: 'rgb(115,100,100)', 3:'blue', 4:'pink'}
            cols = texts['prediction'].map(colorsIdx)
            fig = go.Figure(data=go.Scattergl(x = pcaDF['pc1'], y = pcaDF['pc2'], text = texts['text'], mode='markers', marker=dict(line_width=1, size=5, color=cols), hoverinfo='text'))
            fig.update_layout(title_text = 'Plotly')
            fig.write_html('first_figure.html', auto_open=True)
            
#             plt.figure()
#             plt.figure(figsize=(10,10))
#             plt.xticks(fontsize=12)
#             plt.yticks(fontsize=14)
#             plt.xlabel('Principal Component - 1',fontsize=20)
#             plt.ylabel('Principal Component - 2',fontsize=20)
#             plt.title("Principal Component Analysis of text embeddings",fontsize=20)
#             targets = ['embeddings']
            #targets = ['Benign', 'Malignant']
            #colors = ['r', 'g']
#             for target, color in zip(targets,colors):
#                 indicesToKeep = breast_dataset['label'] == target
#                 plt.scatter(pcaDF.loc[indicesToKeep, 'pc1']
#                            , pcaDF.loc[indicesToKeep, 'pc2'], c = color, s = 50)
#             
#             plt.legend(targets,prop={'size': 15})
#             plt.scatter(pcaDF['pc1'], pcaDF['pc2'], c = ['b'], s = 50)
#             plt.legend(targets,prop={'size': 15})
#             plt.show()
            
    
    def __buildBERTKNNModel(self, docs: DataFrame):
        """
        Build a model from the texts given
        :param docs: dataframe containing one column 'text' which contains the raw texts of the corpus
        """
        log.info(f"I received {len(docs)} examples for finding topics.")
        log.info(f"preprocess tweets...")
        docs['text'] = self.__preprocessTweets(docs['text'])
        rawText=docs['text']
        log.info("embedding...")
        docsEmb=self.__embed(docs)
        log.info("clustering...")
        md, clusterPerDoc = self.__cluster(docsEmb)
        docsEmb['prediction'] = clusterPerDoc
        
        self.__draw(docsEmb, md, clusterPerDoc)
        #clean
        docsEmb.drop(columns=['embedding', 'prediction'], inplace=True)
        return md, clusterPerDoc 
    
    
    def cluster(self, examples: DataFrame)->DataFrame:
        """
        Find the best clusters for the examples given using LDA topic modeling
        :return: the df with the doc associated with a topic ID in column 'TopicPredicted'
        """
        log.info(f"No BERT-KNN model is given, build it from scratch.")
        self.model, clusterLabelsPerDocs = self.__buildBERTKNNModel(examples)
        examples['TopicPredicted'] = clusterLabelsPerDocs
        examples.to_csv(f"{self._properties.tmpPath}/BERTClustererOutput.tsv", sep='\t')       
        log.info(f"Best BERT-KNN model computed and written @{self._properties.classifierModelsPath}/BERTClusterer.md")
        with open(self._properties.classifierModelsPath+f"/BERTClusterer.md", "wb") as f:
            pkl.dump(self.model, f)
            f.close()
        return clusterLabelsPerDocs