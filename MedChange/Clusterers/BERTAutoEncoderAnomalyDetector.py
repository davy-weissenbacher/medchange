'''
Created on Oct 26, 2020

@author: dweissen
'''

import logging as lg
log = lg.getLogger('BERTAutoEncoderAnomalyDetector')
from Properties.PropertiesInt import PropertiesInt
from os import path

from sklearn.neural_network import MLPRegressor
from scipy.spatial.distance import cosine

import pandas as pd
from pandas import DataFrame, Series
import pickle as pkl 
from flair.data import Sentence
from flair.embeddings import TransformerDocumentEmbeddings
from flair.embeddings import SentenceTransformerDocumentEmbeddings

class BERTAutoEncoderAnomalyDetector():
    '''
    Not properly a clusterer, this object detect the reviews that are different from the others 
    by computing a distance from the autoencoding of the bert representation of the review
    inspired by https://medium.com/datadriveninvestor/unsupervised-outlier-detection-in-text-corpus-using-deep-learning-41d4284a04c8
    '''
    
    def __init__(self, properties:PropertiesInt):
        # init embedding
        self.embedding = SentenceTransformerDocumentEmbeddings('bert-base-nli-mean-tokens')
        
    def __preprocessDocs(self, texts: Series) -> Series:
        """
        Preprocess the docs before tokenizing them:
        lowercased
        """
        #lowercase the text of the tweets
        texts =  texts.apply(str.lower)
        return texts
        
    def __embed(self, texts:DataFrame):
        """
        Embed the texts and return the result in a new column 'embedding' (size 768)
        The process is long, I write it in the tmp the first processed, and read from it after
        """
        log.info("Start embedding the docs...")
        pathPickle = '/Users/dweissen/Documents/AvirerBig/textsEmbedded.pkl'
        if path.exists(pathPickle):
            log.warning(f"An existing tsv with the texts embedded has been found @{pathPickle}, will be used...")
            textsEmb = pd.read_pickle(pathPickle)
            return textsEmb
        else:
            def embedText(text, cmp):
                # create a sentence
                sentence = Sentence(text)
                # embed the sentence
                self.embedding.embed(sentence)
                cmp[0] = cmp[0]+1
                print(cmp[0])
                return sentence.get_embedding().numpy()
            cmp = [0]
            texts['embedding'] = texts['text'].apply(lambda tweetTxt: embedText(tweetTxt, cmp))
            texts.to_pickle(pathPickle)
            return texts
        
    def __encode(self, docs):
        """
        Auto-encode the vector representing the reviews
        """
        log.info("Start training the auto-encoder...")
        auto_encoder = MLPRegressor(hidden_layer_sizes=(1200, 300, 1200,))
        auto_encoder.fit(list(docs['embedding']), list(docs['embedding']))
        predicted_vectors = auto_encoder.predict(list(docs['embedding']))
        log.info(f"performance of the autoencoder: {auto_encoder.score(predicted_vectors, list(docs['embedding']))}")
#         print(pd.DataFrame(auto_encoder.loss_curve_).plot())
#         print(predicted_vectors)
        docs["input_autoencoded"] = pd.Series(list(predicted_vectors), index = docs.index)
        return docs
    
    
    def __buildBERTAEModel(self, docs: DataFrame):
        log.info(f"I received {len(docs)} examples to ranked.")
        log.info(f"preprocess docs...")
        docs['text'] = self.__preprocessDocs(docs['text'])
        rawText=docs['text']
        docsEmb=self.__embed(docs)
        docsEncoded = self.__encode(docsEmb)
        def score(doc):
            return (1-cosine(doc['embedding'], doc["input_autoencoded"]))
        docsEncoded["cosinScore"] = docsEncoded.apply(lambda doc:score(doc), axis=1)
        docsEncoded.sort_values(by=['cosinScore'], axis=0, ascending=True, inplace=True)
        docsEncoded.drop(['embedding', 'input_autoencoded'], axis='columns', inplace=True)
        docsEncoded.to_csv('/tmp/out.tsv', sep='\t')
    
    
    def key_consine_similarity(self, tupple):
        return tupple[1]

    
#     def get_computed_similarities(self, vectors, predicted_vectors, reverse=False):
#         data_size = len(vectors)
#         cosine_similarities = []
#         for i in range(data_size):
#             cosine_sim_val = (1 - cosine(vectors[i], predicted_vectors[i]))
#             cosine_similarities.append((i, cosine_sim_val))
#     
#         return sorted(cosine_similarities, key=self.key_consine_similarity, reverse=reverse)
    
    def display_top_n(sorted_cosine_similarities, n=5):
        for i in range(n):
            index, consine_sim_val = sorted_cosine_similarities[i]
            print('Movie Title: ', title_plot_df.iloc[index, 0])  
            print('Cosine Sim Val :', consine_sim_val)
            print('---------------------------------')
        
    def detect(self, examples: DataFrame)->DataFrame:
        """
        try to compute the anomaly in the semantic of the reviews
        :return: the df with a score for each review representing the degradation of the autoencoder
        """
#         log.fatal("A virer!!!!!!!!!!!!!!!!!!!!!!!!!!")
#         examples = examples[:100]
        
        log.info("start computing the embedding")
        self.__buildBERTAEModel(examples)
        