'''
Created on Aug 4, 2020

@author: dweissen
'''

import abc
from pandas.core.frame import DataFrame

class ClustererInt(object):
    '''
    Interface describing the services needed for the unsupervised algorithm used to detect clusters of reasons in texts mentioning medication changes
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
    
    @abc.abstractmethod
    def cluster(self, examples: DataFrame) -> DataFrame:
        pass