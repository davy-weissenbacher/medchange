'''
Created on Nov 18, 2019

@author: dweissen
'''
import logging as lg
log = lg.getLogger('PerformanceDrawer')
lg.getLogger('matplotlib').setLevel(lg.WARNING)
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from keras.callbacks import History
from pandas.core.frame import DataFrame
from typing import List


from Performance.Performance import Performance, Scores

class PerformanceDrawer(object):
    '''
    The drawer plot the perfmance of an active learner or a classifier
    '''

    def __init__(self):
        '''
        Constructor
        '''
    
    def drawHistory(self, history: History, path: str):
        '''
        Draw the performance of a classifier on the evaluation set from the history computed by Keras
        '''
        #         # Plot training & validation accuracy values
#         plt.plot(history.history['acc'])
#         plt.plot(history.history['val_acc'])
#         plt.title('Model performance')
#         plt.ylabel('Metrics')
#         plt.xlabel('Epoch')
#         plt.legend(['Train', 'Validation'], loc='upper left')
#         plt.show()
        
        # Plot training & validation loss values
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Metrics')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Validation'], loc='upper right')
        plt.draw()
        plt.savefig(path)
        plt.close()
#     def drawHistories(self, histories: List[History], path: str):
        
    def drawPerformance(self, perf: Performance, path:str):
        '''
        Draw the performance of a classifier on the test set
        '''
        df = DataFrame()
        iterationsX = sorted(perf.getPerformance().keys())
        assert len(iterationsX)>0, "I am asked to draw an history which is empty, check the code."
        
        df['x'] = iterationsX 
        f1s = []
        for iterationx in iterationsX:
            f1s.append(perf.getPerformance()[iterationx].getDetails()['F1positive'])
        df[perf.classifier] = f1s
                    
        pp = PdfPages(path)
        
        fig = plt.figure()
        plt.plot('x', perf.classifier, data=df, marker='', color='blue', linewidth=2)
        for row in df.itertuples():
            assert row[2]<=1.0, "Probably an error when retrieving the f1-score from the df for drawing the curve, check the code..."
            if len(str(row[2]))>=5:
                plt.text(row[1],row[2], str(row[2])[2:5], fontsize=4)
            else:
                plt.text(row[1],row[2], str(row[2])[2:], fontsize=4)
        plt.legend()
        pp.savefig(fig)        
        pp.close()
        
#     def drawPerformances(self, performances: List[Performance], path:str):    
        
        