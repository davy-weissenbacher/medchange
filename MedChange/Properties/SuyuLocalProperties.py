from Properties.PropertiesInt import PropertiesInt

class SuyuLocalProperties(PropertiesInt):
    '''
    Just a convenient class to store the properties
    '''

    def __init__(self):
        self.name='suyu'
        self.wordEmbeddingPath = '/data/wuch/glove.840B.300d.txt'

        self.logFilePath = '../../../logs/MedChangeLogs.log'
        self.picklePath = "../../../pickle/session.pkl"
        
        self.WebMDTrainingReviewsPath = '/data/gesy/MNA/NonAdherenceClassification_8688_woNL_Train.csv'
        self.WebMDValidationReviewsPath = '/data/gesy/MNA/NonAdherenceClassification_8688_woNL_Val.csv'
        self.WebMDUnlabeledReviewsPath = '/data/gesy/MNA/All_WebMD_two_stars_and_less_satisfy.tsv'
        
        self.drugTweetsAnnotatedPath = '/data/gesy/MNA/AnnotationDRUGSInTweets_EMNLPChallenge18_TrainingSetClean.csv'
        self.drugTweetsUnlabeledPath = '/data/gesy/MNA/AnnotationDRUGSInTweets_ExtraValidation.tsv'
        self.drugTweetsTestPath = '/data/gesy/MNA/AnnotationDRUGSInTweets_EMNLPChallenge18_TestSetClean.csv'
        
        # the path of the csv file used to communicate with a human annotator
        self.annotatedMostInformativeExamplesPath = '/data/gesy/MNA/MIExamples/'
        
        # the path to the folder where all classifiers are requested to write their models
        self.classifierModelsPath = '/data/gesy/MNA/models/'
        self.pathToEmbeddingsDic='/data/gesy/MNA/renewed_lister.pkl'
        self.w2vVocabPath="/data/gesy/MNA/w2vVocab.npy"
        self.w2vVectorPath="/data/gesy/MNA/w2vVector.npy"
        self.stopWordPath='/data/gesy/MNA/stopwords.csv'
        
        self.ExQueriedBatchSize = 600