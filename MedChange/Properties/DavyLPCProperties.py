'''
Created on Oct 24, 2019

@author: davy
'''
from Properties.PropertiesInt import PropertiesInt

class DavyLPCProperties(PropertiesInt):
    '''
    Just a convenient class to store the properties on the LPC environment
    '''

    def __init__(self):
        self.name='Davy'
        self.tmpPath = '/home/dweissen/tal/projects/MedChange/tmpOutputs/'
        self.logFilePath = '/home/dweissen/tal/projects/MedChange/outputs/MedChangeLogs.log'
        self.picklePath =  '/home/dweissen/tal/projects/MedChange/outputs/session.pkl'
         
        
        # the set of webMD review annotated for Medical Change Status
        self.WebMDTrainingReviewsPath = '/home/dweissen/tal/projects/MedChange/data/MSCClassification_12972_1211019_temp_TRAIN.csv'
        self.WebMDValReviewsPath = '/home/dweissen/tal/projects/MedChange/data/MSCClassification_12972_1211019_temp_VAL.csv'
        self.WebMDTestReviewsPath = '/home/dweissen/tal/projects/MedChange/data/MSCClassification_12972_1211019_temp_TEST.csv'
        self.WebMDUnlabeledReviewsPath = '/home/dweissen/tal/projects/MedChange/data/WebMD-one-two-stars-satisfy.tsv'
                                                                                                
        #the set of tweets from Kusuri + statins annotated for Medical Change Status
        self.MCSTweetsTrainingPath = '/home/dweissen/tal/projects/MedChange/data/MSC_Twitter_9835_011720_TRAIN.tsv'
        self.MCSTweetsValPath = '/home/dweissen/tal/projects/MedChange/data/MSC_Twitter_9835_011720_VAL.tsv'
        self.MCSTweetsTestPath = '/home/dweissen/tal/projects/MedChange/data/MSC_Twitter_9835_011720_TEST.tsv'
         
        self.drugTweetsAnnotatedPath = '/home/dweissen/tal/projects/MedChange/data/AnnotationDRUGSInTweets_EMNLPChallenge18_TrainingSetClean.csv'
        self.drugTweetsUnlabeledPath = '/home/dweissen/tal/projects/MedChange/data/AnnotationDRUGSInTweets_ExtraValidation.tsv'
        self.drugTweetsTestPath = '/home/dweissen/tal/projects/MedChange/data/AnnotationDRUGSInTweets_EMNLPChallenge18_TestSetClean.csv'
         
        # the path of the csv file used to communicate with a human annotator
        self.annotatedMostInformativeExamplesPath = '/home/dweissen/tal/projects/MedChange/data/MIExamples/'
         
        # the path to the folder where all classifiers are requested to write their models
        self.classifierModelsPath = '/home/dweissen/tal/projects/MedChange/models/'
        
        # the path to word embeddings
        self.wordEmbeddingPath = '/home/dweissen/tal/projects/sharedResources/glove.840B.300d.txt'

        #path to BERT embeddings
        self.BERTEmbedding = '/home/dweissen/tal/projects/sharedResources/'
        
                # paths to word2vecEmbeddings
        self.w2vVocabPath = '/home/dweissen/tal/projects/sharedResources/w2vVocab.npy'
        self.w2vVectorPath = '/home/dweissen/tal/projects/sharedResources/w2vVector.npy'

        self.pathToEmbeddingsDic = self.wordEmbeddingPath+".pkl"
        #self.stopWordPath='/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Resources/stopwords1.tsv'
        self.stopWordPath='/Users/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Resources/smallStopWords.tsv'

        self.ExQueriedBatchSize = 200
