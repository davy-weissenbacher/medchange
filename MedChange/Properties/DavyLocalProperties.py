'''
Created on Oct 7, 2019

@author: davy
'''

import os

from Properties.PropertiesInt import PropertiesInt

class DavyLocalProperties(PropertiesInt):
    '''
    Just a convenient class to store the properties on my local environment
    '''

    def __init__(self):
        
#---------- on new desktop
        self.name='Davy'
        self.tmpPath = '/tmp/'
        self.logFilePath = '/tmp/MedChangeLogs.log'
        self.picklePath = "/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Pickle/session.pkl"
        
        #the set of tweets annoted for Coronavirus presence
        self.covidTweetsExamplesPath = ''
         
        # the set of webMD review annotated for Medical Change Status
        self.WebMDTrainingReviewsPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSCClassification_12972_1211019_temp_TRAIN.csv'
        self.WebMDValReviewsPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSCClassification_12972_1211019_temp_VAL.csv'
        self.WebMDTestReviewsPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSCClassification_12972_1211019_temp_TEST.csv'
        self.WebMDUnlabeledReviewsPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/WebMD-one-two-stars-satisfy.tsv'
        
        #the set of tweets from Kusuri + statins annotated for Medical Change Status
        self.MCSTweetsTrainingPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSC_Twitter_9835_011720_TRAIN.tsv'
        self.MCSTweetsValPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSC_Twitter_9835_011720_VAL.tsv'
        self.MCSTweetsTestPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Corpus/MSC_Twitter_9835_011720_TEST.tsv'
        
        #the set of tweets from Kusuri annotated for drugs vs not drugs 
        self.drugTweetsAnnotatedPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/AnnotationDRUGSInTweets_EMNLPChallenge18_TrainingSetClean.csv'
        self.drugTweetsUnlabeledPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/AnnotationDRUGSInTweets_ExtraValidation.tsv'
        self.drugTweetsTestPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/AnnotationDRUGSInTweets_EMNLPChallenge18_TestSetClean.csv'
                
        self.drugTweetsUnbalancedAnnotatedPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/trainStratTimelines.csv'
        self.drugTweetsUnbalancedUnlabeledPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/trainStratTimelines.csv' 
        self.drugTweetsUnbalancedTestPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/DrugTweetDetector/Kusuri/EMNLPExperiments/testStratTimelines.csv'
         
        # the path of the csv file used to communicate with a human annotator
        self.annotatedMostInformativeExamplesPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/MIExamples/'
         
        # the path to the folder where all classifiers are requested to write their models
        self.classifierModelsPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Models/'
         
        # the path to word embeddings
        self.wordEmbeddingPath = '/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Resources/glove.840B.300d.txt'

        #path to BERT embeddings
        self.BERTEmbedding = '/home/dweissen/tal/Soft_Developement/Ressources/SharedResources/BERT/uncasedL12H768A12/'
        
        # paths to word2vecEmbeddings
        self.w2vVocabPath = '/home/dweissen/tal/Soft_Developement/Ressources/SharedResources/Embeddings/w2vVocab.npy'
        self.w2vVectorPath = '/home/dweissen/tal/Soft_Developement/Ressources/SharedResources/Embeddings/w2vVector.npy'

        self.pathToEmbeddingsDic = self.wordEmbeddingPath+".pkl"
        #self.stopWordPath='/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Resources/stopwords1.tsv'
        self.stopWordPath='/home/dweissen/tal/Soft_Developement/Ressources/Dev_SM4Pharmacovigilance/NonAdherence/MedicalStatusChange/Resources/smallStopWords.tsv'
        
        # define the size of the batch of the most informative examples returned by the active learner for annotation by the oracle
        self.ExQueriedBatchSize = 200
        
#---------- on old destop         
#         self.logFilePath = '/tmp/MedChangeLogs.log'
#         self.picklePath = "/media/davy/storage/Data/MedChange/Pickle/session.pkl"
#         
#         self.WebMDTrainingReviewsPath = '/media/davy/storage/Data/MedChange/NonAdherenceClassification_8688_woNL_Train.csv'
#         self.WebMDValidationReviewsPath = '/media/davy/storage/Data/MedChange/NonAdherenceClassification_8688_woNL_Val.csv'
#         self.WebMDUnlabeledReviewsPath = '/media/davy/storage/Data/MedChange/WebMD-one-two-stars-satisfy.tsv'
#         
#         self.drugTweetsAnnotatedPath = '/media/davy/storage/Data/MedChange/AnnotationDRUGSInTweets_EMNLPChallenge18_TrainingSetClean.csv'
#         self.drugTweetsUnlabeledPath = '/media/davy/storage/Data/MedChange/AnnotationDRUGSInTweets_ExtraValidation.tsv'
#         self.drugTweetsTestPath = '/media/davy/storage/Data/MedChange/AnnotationDRUGSInTweets_EMNLPChallenge18_TestSetClean.csv'
#         
#         # the path of the csv file used to communicate with a human annotator
#         self.annotatedMostInformativeExamplesPath = '/media/davy/storage/Data/MedChange/MIExamples/'
#         
#         # the path to the folder where all classifiers are requested to write their models
#         self.classifierModelsPath = '/media/davy/storage/Data/MedChange/Models/'
#         
#         # the path to word embeddings
#         self.wordEmbeddingPath = '/media/davy/storage/Data/MedChange/Resources/glove.840B.300d.txt'

#         DIR_DATA = os.path.join(os.path.expanduser('~'), 'Downloads', 'MedChangeData')
#         self.picklePath = os.path.join(DIR_DATA, 'Pickle', 'session.pkl')
# 
#         self.WebMDTrainingReviewsPath = os.path.join(DIR_DATA, 'NonAdherenceClassification_8688_woNL_Train.csv')
#         self.WebMDValidationReviewsPath = os.path.join(DIR_DATA, 'NonAdherenceClassification_8688_woNL_Val.csv')
#         self.WebMDUnlabeledReviewsPath = os.path.join(DIR_DATA, 'WebMD-one-two-stars-satisfy.tsv')
# 
#         self.drugTweetsAnnotatedPath = os.path.join(DIR_DATA, 'AnnotationDRUGSInTweets_EMNLPChallenge18_TrainingSetClean.csv')
#         self.drugTweetsUnlabeledPath = os.path.join(DIR_DATA, 'AnnotationDRUGSInTweets_ExtraValidation.tsv')
#         self.drugTweetsTestPath = os.path.join(DIR_DATA, 'AnnotationDRUGSInTweets_EMNLPChallenge18_TestSetClean.csv')
# 
#         # the path of the csv file used to communicate with a human annotator
#         self.annotatedMostInformativeExamplesPath = os.path.join(DIR_DATA, 'MIExamples')
#         
#         # the path to the folder where all classifiers are requested to write their models
#         # self.classifierModelsPath = '/media/davy/storage/Data/MedChange/Models/'
#         self.classifierModelsPath = os.path.join(DIR_DATA, 'Models')
