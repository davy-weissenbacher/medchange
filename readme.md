# MedChange, a classifier to detect posts mentioning changes in medication treatments #

MedChange is a classifier optimized to detect posts mentioning a change in medication treatment, the first step to detect medication nonadherence. MedChange interface is a RESTful service. MedChange performs its classification using either a CNN or Regular Expressions or a BERT classifier. MedChange has been optimized to work on 2 types of corpus Twitter and WebMD drug reviews (forums type). This is the first version of the classifier, the CNN and REs are described in [Weissenbacher et al., 2020](https://doi.org/10.1101/2020.12.04.20244210). The BERT has been added after and trained on WebMD, there is currently no model for Twitter (in progress). 

---

##*How to install*

Assuming you have FLASK installed and working locally, just run medchangeAPI.py, this will start MedChange which will listen at http://127.0.0.1:5000 by default.

##*How to use*
MedChange expects the inputs formatted as a valid JSON object and will return its outputs as a JSON. MedChange will return the JSON when receiving a post request, by default `http://127.0.0.1:5000//medchange/v0.1/predict` see testMedChangeAPI.py for a concrete example.

###*Input*
MedChange provides one service *predict* through a POST request. The parameters of the prediction should be posted as a JSON with the following format: 
`{"classifier":"BERT", "genre":"twitter", "posts": [{"postID":"10", "text":"I stopped adderall cold turkey", "Drug Name Extracted":'adderall'}, {"postID":"11", "text":"My son changed his medication against my advice", "Drug Name Extracted":''}, {"postID":"12", "text":"My third tweet", "Drug Name Extracted":'third'}, {"postID":"13", "text":"My fourth tweet", "Drug Name Extracted":''}]}`

- posts: the keys 'postID' and 'text' are expected. Other keys/values can be included, they will be ignored by the API and returned unchanged in the JSON output
- classifier: classifier_available are *REs*, *CNN*, *BERT*. *REs* will apply a baseline Regular Expression classifier on the input posts. All posts with a phrase matching a pattern will be labeled 1. *CNN* will apply an ensemble of 5 CNNs trained to detect changes in a corpus of tweets/WebMD reviews. *BERT* will apply a BERT classifier trained to detect changes in a corpus of WebMD reviews. A version of the classifier for tweets will be uploaded later    
- genre: *[twitter|forum]*, according to the option chosen, a different model will be ran on the input. They are the same classifier but with different rules or trained on different corpora, therefore they will achieve different performances.

###*Output*
If the prediction is successful, MedChange will return a JSON with the following format:
1. If the REs classifier is used:
`{'posts': [{'postID': 1, 'text': 'the first tweet', 'certainty_label_1':0, 'certainty_label_0':1 'prediction':0}, {'postID': '2', 'text': 'the second tweet'}, 'certainty_label_1':1, 'certainty_label_0':0, 'prediction':1]}`
2. If the CNN or BERT classifier are used: 
`{'posts': [{'tweetID': 1, 'text': 'the first tweet', 'certainty_label_0':0.99, 'certainty_label_1':0.01, 'prediction':0}, {'tweetID': '2', 'text': 'the second tweet'}, , 'certainty_label_0':0.48, 'certainty_label_1':0.52, 'prediction':1]}`

###Calling MedChange
The script testMedChangeAPI.py shows how to call MedChange from a python client. It also provides an example of using the service to evaluate the performance of the classifiers on public benchmark for medication change detection.

---

#*MedChange performance*
We trained the models of the CNN an BERT classifiers using different corpora created by the HLP center. The corpus of tweets annotated with changes in medication was released during SMM4H'21 Task 3. The methods used for training the CNNs and their performances are described in [Weissenbacher et al., 2020](https://doi.org/10.1101/2020.12.04.20244210). The models selected for the REST API are for: 
- CNN-twitter: the expert committees pretrained on WebMD Corpus with Active Learning and fine-tuned on the Change Twitter Corpus. Performance on the Twitter Corpus test set are P:0.4723, R:0.5337, F1:0.5011 
- CNN-forum: the expert committees trained on WebMD Corpus with Active Learning. Performance on the WebMD Corpus test set are P:0.8166, R:0.8544, F1:0.8351
- BERT-forum: a basic BERT classifier trained on WebMD Corpus without Active Learning (no additional layer, 3 epochs, we took the last model). Performance on the WebMD Corpus test set = P:0.8714, R:0.8762, F1:0.8738
- BERT-twitter: not available yet.

---

#*How to retrain the models*
The models can be retrained on any existing fully annotated corpus using the Trainer object in TransferTestPipe.py. This will require modifying the code according to the input corpora and if transfer/AL are used together or separately. If the corpora are not fully annotated, the classifiers can still be trained using the MedChangePipe.py. Again this will require modifying the code and involve human annotators if AL is used. This is still a prototype and could require code corrections to work properly on the user's environment. 

---