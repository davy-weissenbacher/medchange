'''
Created on Oct 15, 2020

@author: dweissen
'''

import requests
import pandas as pd
import json
from sklearn.metrics import confusion_matrix, classification_report
import logging as lg
import sys

from Handlers.WebMDReviewsHandler import WebMDReviewsHandler
from Handlers.TwitterHandler import TwitterHandler

from Properties.PropertiesInt import PropertiesInt
from Properties.DavyMacProperties import DavyMacProperties

if __name__ == '__main__':
    properties: PropertiesInt = DavyMacProperties()
    assert hasattr(properties, 'entryURL'), "I can't find the URL in the properties file."
    
    lg.basicConfig(level=lg.DEBUG,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    handlers=[lg.StreamHandler(sys.stdout), lg.FileHandler('/tmp/testMedChangeAPI.log')])
    lg.info("==========================================START==========================================")
    log = lg.getLogger(__name__)

#     #For testing the service if needed:
    exJSON = {
        "classifier":"BERT",
        "genre":"twitter",
        "posts": [
                {"postID":"10", "text":"I stopped adderall cold turkey", "label":"1", "Drug Name Extracted":'adderall'},
                {"postID":"11", "text":"My son changed his medication against my advice", "label":"2", "Drug Name Extracted":''},
                {"postID":"12", "text":"My third tweet", "label":"1", "Drug Name Extracted":'third'},
                {"postID":"13", "text":"My fourth tweet", "label":"3", "Drug Name Extracted":''}]
    }

#     wrh = WebMDReviewsHandler()
#     examples = wrh.getUnlabeledExamples(properties.WebMDUnlabeledReviewsPath)
#     examples = examples.rename(columns={'TEXT':'text'})
#     examples['postID'] = examples.index

#     wrh = WebMDReviewsHandler()
#     examples = wrh.getTestExamples(properties.WebMDTestReviewsPath)
#     examples = examples.rename(columns={'TEXT':'text'})
#     examples['postID'] = examples.index
    
    twth = TwitterHandler()
    examples = twth.getTestExamples(properties.MCSTweetsTestPath)
    examples['postID'] = examples.index
    
    exJSON = {}
    exJSON['posts'] = json.loads(examples.to_json(orient="records"))
    exJSON['classifier'] = "BERT"
    exJSON['genre'] = "forum"
    
    #POST
    url = 'http://127.0.0.1:5000'+properties.entryURL
    #url = 'http://hlp.ibi.upenn.edu/'+properties.entryURL
    resp = requests.post(url, json=exJSON)
    if resp.status_code != 200:
        raise Exception(f'POST /predict/ ERROR: {resp.status_code}')
    else:
        exJSON = resp.json()
        if 'errors' in exJSON:
            raise Exception(f'POST /predict/ ERROR: {resp.json()}')
        elif 'posts' in exJSON:
            log.info('ok!')
            #TODO: to see what is the format sent and normalize that in a dataframe
            df = pd.json_normalize(exJSON['posts'])
                        
            lg.info(f'Prediction done for {len(df)} posts.')
            lg.info(f'First 10 posts:\n{df.head(10)}')
            df.to_csv('/tmp/outDrug.tsv', sep='\t')
            
            #Evaluation of the prediction if needed
            log.info(f"\n{confusion_matrix(list(df['label']), list(df['prediction']))}")        
            log.info(f"\n{classification_report(list(df['label']), list(df['prediction']),digits=4)}")
        else:
            raise Exception(f'POST /predict/ ERROR: unexpected json received from the rest: {exJSON}')