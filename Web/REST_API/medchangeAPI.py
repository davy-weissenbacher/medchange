'''
Created on Oct 15, 2020

To run the application
cd /my/path/to/testFlask.py
export FLASK_ENV=development #to enable the debugger
export FLASK_DEBUG=1
export FLASK_APP=testFlask.py
python -m flask run 
logging output are in the console running the Flask

(or use the build flask server and run in debug mode as usual with the main function...)

Can create the request using postman

@author: dweissen
'''

from flask import Flask
from flask_restful import Api, Resource, reqparse
import json
import os
import pickle as pkl
import pandas as pd

from ActiveLearners.Classifiers.CNNTF2Classifier import CNNTF2Classifier
from ActiveLearners.Classifiers.BertNSPClassifier import BertNSPClassifier
from ActiveLearners.Classifiers.RegExFinder import RegExFinder
from ActiveLearners.RandomSampler import RandomSampler
from ActiveLearners.ExpertCommittee import ExpertCommittee
from Properties.PropertiesInt import PropertiesInt
from Properties.DavyMacProperties import DavyMacProperties

from logging.config import dictConfig
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__, static_url_path="")
api = Api(app)
properties: PropertiesInt = DavyMacProperties()
assert hasattr(properties, 'entryURL'), "I can't find the URL in the properties file."


class predictAPI(Resource):
    """
    main service implemented in the POST
    """
    
    def __init__(self, **kwargs):
        self.properties = kwargs['properties']
        self.CLASSIFIERS_AVAILABLE = ['REs', 'CNN', 'BERT']
        self.GENRES_HANDLED = ['forum', 'twitter']
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('posts', type=list, required=True,
                                   help='No posts were provided.',
                                   location='json')
        self.reqparse.add_argument('classifier', type=str, required=True,
                                   help=f'No classifier was provided. Classifier available: {self.CLASSIFIERS_AVAILABLE}',
                                   location='json')
        self.reqparse.add_argument('genre', type=str, required=True,
                           help=f'No genre of corpus were provided. Genre processed: {self.GENRES_HANDLED}',
                           location='json')
        super(predictAPI, self).__init__()
        self.USAGE = f"\
                POST - predict: The parameters should be posted as a JSON with the following format: "
                
                
    def post(self):
        """
        :input: the input are expected as a JSON, see USAGE for the format:
        :output: return a JSON of the form [{'postID': 1, 'text': 'the first tweet', 'Predicted_label': 0, 'Certainty':0.54}, {'postID': '2', 'text': 'the second tweet', 'Predicted_label': 2, 'Certainty':0.78}]}.
        If other columns were present in the input JSON, they will be added in the output JSON
        This format can be obtained by df.to_json(orient="records"), where df is a pandas.DataFrame 
        """
        #chech the request args
        try:
            args = self.reqparse.parse_args()
        except:
            errormsg = "Problem when parsing the request. Usage: "+self.USAGE
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        #valid the classifier received
        if args['classifier'] not in self.CLASSIFIERS_AVAILABLE:
            errormsg = f"Received a classifier not supported, valid classifiers are {self.CLASSIFIERS_AVAILABLE}"
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        if args['genre'] not in self.GENRES_HANDLED:
            errormsg = f"Received a genre not supported, valid genre are {self.GENRES_HANDLED}"
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        #valid the set of posts received
        if len(args['posts'])==0:
            errormsg = "Received an empty set of posts, nothing to predict."
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        app.logger.debug(f"Received {len(args['posts'])} posts to process...")
        try:
            posts = pd.json_normalize(args['posts'])
        except Exception as e:
            errormsg = f"Error when converting the JSON representing the posts into a pandas DataFrame: {e}"
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        if not {'postID','text'}.issubset(posts.columns):
            errormsg = "Received a JSON representing the posts with an invalid format, the required attributs are 'postID':str and 'text':str"
            app.logger.error(errormsg)
            return {'errors':[{'code':400, 'message':errormsg}]}
        
        app.logger.debug('Start prediction...')
        try:
            #create the classifier
            if args['classifier']=='REs':
                if args['genre']=='forum':
                    actLearner: RandomSampler = RandomSampler(RegExFinder('RE1', RegExCorpus='WebMD', verbose=False))
                elif args['genre']=='twitter':
                    actLearner: RandomSampler = RandomSampler(RegExFinder('RE1', RegExCorpus='Twitter', verbose=False))
                else:
                    raise Exception(f"Unsupported genre of corpus was given : {args['genre']}")
            elif args['classifier']=='CNN':
                embeddingsPath = self.properties.wordEmbeddingPicklePath
                if os.path.exists(embeddingsPath):
                    app.logger.warning(f"Im reading an existing process_WebMD pickled in temp.")
                    process_WebMD = pkl.load(open(embeddingsPath, 'rb'))
                else:
                    raise Exception(f"I was expecting a pickle of the embeddings already available @{embeddingsPath}")
                if args['genre']=='forum':
                    #best model obtained for the JAMIA paper?, the CNN trained on webmd with AL run 2
                    actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath=self.properties.CNN_WebMD1), CNNTF2Classifier('CNN2',process_WebMD, InitialModelsPath=self.properties.CNN_WebMD2), CNNTF2Classifier('CNN3',process_WebMD, InitialModelsPath=self.properties.CNN_WebMD3), CNNTF2Classifier('CNN4',process_WebMD, InitialModelsPath=self.properties.CNN_WebMD4), CNNTF2Classifier('CNN5',process_WebMD, InitialModelsPath=self.properties.CNN_WebMD5)])
                elif args['genre']=='twitter':
                    #best model obtained for the JAMIA paper?, the CNN trained with transfer and AL run 2.
                    actLearner: ExpertCommittee = ExpertCommittee([CNNTF2Classifier('CNN1',process_WebMD, InitialModelsPath=self.properties.CNN_Twitter1), CNNTF2Classifier('CNN2',process_WebMD, InitialModelsPath=self.properties.CNN_Twitter2), CNNTF2Classifier('CNN3',process_WebMD, InitialModelsPath=self.properties.CNN_Twitter3), CNNTF2Classifier('CNN4',process_WebMD, InitialModelsPath=self.properties.CNN_Twitter4), CNNTF2Classifier('CNN5',process_WebMD, InitialModelsPath=self.properties.CNN_Twitter5)])
                else: #should not be here, but in case...
                    raise Exception(f"Unsupported genre of corpus was given : {args['genre']}")
            elif args['classifier']=='BERT':
                if args['genre']=='forum':
                    #not published, a BERT trained without AL on WebMD (seq.256, batch 16, max epoch 3 take the last one)
                    actLearner: RandomSampler = RandomSampler(BertNSPClassifier('bert1', self.properties, InitialModelsPath=self.properties.BERT_WebMD))
                elif args['genre']=='twitter':
                    #not done
                    raise Exception(f"I don't have the model for the tweets trained yet... todo, sorry.")
                else: #should not be here, but in case...
                    raise Exception(f"Unsupported genre of corpus was given : {args['genre']}")
                
            else: #should not be here, but in case...
                raise Exception(f"Unsupported classifier was given : {args['classifier']}")
        except Exception as e:
            errormsg = f"An error occured when instantiating the classifier: {e}"
            app.logger.error(errormsg)
            return {'errors':[{'code':500, 'message':errormsg}]}

        predictions = actLearner.classify(posts)
        
        return {"posts":json.loads(predictions.to_json(orient="records"))} 

#api.add_resource(predictAPI, config['urls']['entryURL'], endpoint='predict', resource_class_kwargs={'classifier': classifier})
api.add_resource(predictAPI, properties.entryURL, endpoint='predict', resource_class_kwargs={'properties':properties})

if __name__ == '__main__':
    app.run(debug=False, threaded=False)

